import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os, sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

from matplotlib.lines import Line2D
from matplotlib.patches import Patch 

def sort4qualifier(shot_list):
    powers = ['PNBI_TOT', 'PICR_TOT','PECR_TOT', 'P_TOT']
    #DCN, DCK
    #H/L-facs
    new_shot_list = []
    L_list = []
    H_list = []

    NBI_list = []
    ICR_list = []
    ECR_list = []

    DCN_list = []
    DCK_list = []
    
    #extract shotnumber and iterate over all shotnumbers
    shot_numbers = [shot_info[0] for shot_info in shot_list] 
    for i, shot_number in enumerate(shot_numbers):
        
        print("current shot ", shot_number)
        shot = shot_data[shot_number]

        #print(shot['IDE']['H/L-facs'])
        #check if H-mode or L-mode
        try:
            if shot['IDE']['H/L-facs'] is not None:
                if np.any(shot['IDE']['H/L-facs']) < 0.6:
                    L_list.append(shot_number)
                else:
                    H_list.append(shot_number)
            else:
                print(f"H/L-facs is None for {shot_number}")
        except Exception as e:
            print(f"H/L-facs for shot {shot_number} not found. Error {e}")

        #check powers and add to lists
        if shot['IDE']['PNBI_TOT'] is not None:
            if np.amax(shot['IDE']['PNBI_TOT']) > 0:
                NBI_list.append(shot_number)
        
        if shot['IDE']['PICR_TOT'] is not None:
            if np.amax(shot['IDE']['PICR_TOT']) > 0:
                ICR_list.append(shot_number)

        if shot['IDE']['PECR_TOT'] is not None:
            if np.amax(shot['IDE']['PECR_TOT']) > 0:
                ECR_list.append(shot_number)
        

        if shot['IDE']['DCK_status'] is True:
            DCK_list.append(shot_number)
        elif shot['IDE']['DCK_status'] is False:
            DCN_list.append(shot_number)
        else:
            print("Neither DCk nor DCN available")

    print("L-modes", L_list)
    print("H-modes", H_list)
    print("PNBI_TOT", NBI_list)
    print("PICR_TOT", ICR_list)
    print("PECR_TOT", ECR_list)

    list_dict = {
        'L_list' : L_list,
        'H_list' : H_list,
        'NBI_list' : NBI_list,
        'ICR_list' : ICR_list,
        'ECR_list' : ECR_list,
    } 

    return list_dict

def plot_scalar_diff(shot_data, shot_list, scalar, t_res = 1.0, plotQual = False):
    """
    Plots the scalar values for the specified shots on separate subplots.

    Args:
    - shot_data (dict): Dictionary containing shot data.
    - shot_list (list): List of shot information (shotnumber, tcIps, tcIpe).
    - scalar (str): The scalar name to plot.
    """
    shot_list = sorted(shot_list, key=lambda x: x[0])       #sorts the shots asccending


    shot_numbers = [shot_info[0] for shot_info in shot_list]  # Extract shot numbers
    num_shots = len(shot_numbers)
    spacing = 1  # Adjust this value to control the spacing between shot numbers on the plot
    plt.figure(1, figsize=(8, 5))

    #plots the coloring bars for the qualifier stuff
    if plotQual:
        list_dict = sort4qualifier(shot_list)
        for i, shot_number in enumerate(shot_numbers):

            if shot_number in list_dict['NBI_list']:
                plot_shading(shot_number, spacing, i, colour = 'blueviolet')
            if shot_number in list_dict['ICR_list']:
                plot_shading(shot_number, spacing, i, colour = 'cyan')
            """ if shot_number in list_dict['ECR_list']:
                plot_shading(shot_number, spacing, i, colour = 'olive') """

    for i, shot_number in enumerate(shot_numbers):
        try:
            #set data
            shot = shot_data[shot_number]

            #create start adn end times of constant plasma current
            ipc_obj = utils.get_signal_objects(sf.SFREAD(shot_number, 'FPC'), 'IpiFP', TB=['IpiFP'])
            start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
            times, eqh_time_indexes, ide_time_indexes = utils.create_timestamps(shot['EQH']['time'], shot['IDE']['time'], start_time, end_time, t_res)
            
            #define time indexes for varaible resolution timescales of IDA and EQH
            times, eqh_time_indexes, ide_time_indexes = utils.create_timestamps(shot['EQH']['time'], shot['IDE']['time'], shot['EQH']['tcIps'], shot['EQH']['tcIpe'], t_res)

            #check if both arrays are the same size
            if not len(eqh_time_indexes) == len(ide_time_indexes):
                print("!!!!!NOT THE SAME NUMBER OF TIME INDICIES!!!!!!!!!!!!")
                return
            
            plt.axvline(x=i*spacing, c='k', ls = 'dotted', alpha=0.25)
            print(f'shot: {shot_number} with {len(eqh_time_indexes)} time indices')
            ratio_list = []
            for j, time_index in enumerate(eqh_time_indexes):
                #define scalars
                scalar_value_eqh = shot_data[shot_number]['EQH'][scalar][eqh_time_indexes[j]]
                scalar_value_ide = shot_data[shot_number]['IDE'][scalar][ide_time_indexes[j]]
                
                #calc diff for plot
                ratio = (scalar_value_eqh/scalar_value_ide) * 100
                ratio_list.append(ratio)


            ratio_list = np.array(ratio_list)
            x_values = i * spacing + np.arange(len(ratio_list)) * 0.02 - len(ratio_list)/2 * 0.02
            color = 'b' if i % 2 == 0 else 'r'  # Alternate colors between blue and red
            plt.plot(x_values, ratio_list, 'x', color = color)
                                
        except KeyError:
            print(f"Shot data for shot {shot_number} couldnt be displayed.")

    plt.xlabel('Shot Number', fontsize = 14)
    plt.ylabel(f"Ratio in % of IDE [%]", fontsize = 14)
    plt.title(f"{scalar} every {t_res}s during constant " + r"$I_p$" + " regime", fontsize = 14)
    plt.xticks(range(num_shots), shot_numbers)  # Set shot numbers as x-axis ticks

    # Add dotted line at y=0 along the x-axis
    plt.axhline(y=100, color='black', linestyle='--', linewidth=1)

    legend_elements =  [Line2D([], [], marker='x', color='k', linestyle='None', label='Ratio (EQH/IDE)*100%')] 

    """ legend_elements =  [Patch(facecolor='blueviolet', label='NBI-Heating', alpha =0.1),
                        Patch(facecolor='cyan', label='ICR-Heating', alpha =0.1),
                        Line2D([], [], marker='x', color='r', linestyle='None', label='absolute Difference (IDE-EQH)')]  """

    plt.legend(handles=legend_elements, loc='best')
    plt.tight_layout()
    plt.savefig(f'figs/scalarDiffs/sca_diff_{scalar}_ttrim')
    plt.show()
    plt.close()

def plot_shading(shotnumber, spacing, space_int, colour= 'blueviolet'):
    #plot shading
    plt.axvspan(space_int * spacing - 0.5 , space_int * spacing + 0.5 , ymin =-100, ymax = 100, facecolor = colour, alpha = .1)
    return


if __name__ == "__main__":
    pickle_file = './shot_data.pickle'
    shotlist_file = './shotlist.txt'
    #scalars = ('Zgeri', 'Zgera', 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    #scalars = ( 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    scalars = ('Wmhd',)
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)

    #plot_single_scalar(shot_data, shot_list, 'Raus')
    
    for scalar in scalars:
        plot_scalar_diff(shot_data, shot_list, scalar, 0.3)
        #plot_scalar_diff_norm(shot_data, shot_list, scalar)
        #plot_scalar_diff_boxplot(shot_data, shot_list, scalar, 0.3)

    #plot_scalar_values(shot_data, shot_list, scalars)  # Provide the shotnumber and timepoint
