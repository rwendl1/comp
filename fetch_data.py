import pickle
import os
import aug_sfutils as sf
import numpy as np


def save_not_found_shots(shot_numbers, filename):
    with open(filename, "w") as file:
        for shot_number in shot_numbers:
            file.write(str(shot_number) + "\n")


def get_signal_objects(Geq, *signal_names, TB=[]):
    signal_objects = {}
    for signal_name in signal_names:
        try:
            #print('getting ', signal_name)
            signal_objects[signal_name] = Geq.getobject(signal_name)
            if signal_name in TB:
                signal_objects[str(signal_name+'_TB')] = Geq.gettimebase(signal_name)
        except Exception as e:
            print(f"Error getting object for signal '{signal_name}': {e}")
            signal_objects[signal_name] = None
    return signal_objects

#gets the fringe jumping signal thingy
def get_DCN_objects(shotnumber):
    """Get objects for specified signals from Geq"""
    signal_objects = {}
    DCN = sf.SFREAD(shotnumber, 'DCN')
    DCK = sf.SFREAD(shotnumber, 'DCK')
    if DCK.status:
        signal_objects['DCN_H0'] = DCN.getobject('H-0')
        signal_objects['DCN_H1'] = DCN.getobject('H-1')
        signal_objects['DCN_H5'] = DCN.getobject('H-5')
        signal_objects['DCN_TB'] = DCN.gettimebase('H-1')

        signal_objects['DCK_H0'] = DCK.getobject('H-0')
        signal_objects['DCK_H1'] = DCK.getobject('H-1')
        signal_objects['DCK_H5'] = DCK.getobject('H-5')
        signal_objects['DCK_TB'] = DCK.gettimebase('H-1')
        signal_objects['DCK_status'] = True
    else:
        signal_objects['DCN_H0'] = DCN.getobject('H-0')
        signal_objects['DCN_H1'] = DCN.getobject('H-1')
        signal_objects['DCN_H5'] = DCN.getobject('H-5')
        signal_objects['DCN_TB'] = DCN.gettimebase('H-1')
        signal_objects['DCK_H0'] = None
        signal_objects['DCK_H1'] = None
        signal_objects['DCK_TB'] = None
        signal_objects['DCK_status'] = False
        print("DCK not found proceeded with DCN")
    return signal_objects

#iterates through list of experiments to find one which has a shotfile with IDE
def try_exp_list(shotnumber, exps):
    for exp in exps:
        eval = sf.SFREAD(shotnumber, 'IDE', experiment = exp)
        if eval.status:
            return exp
    return None


def fetch_diag_data(shotnumber, diagnostic, exps, **kwargs):
    """
    Pickles shot data for the given shot number and diagnostic.

    Args:
    - shotnumber (int): The shot number for which data will be pickled.
    - diagnostic (str): The diagnostic identifier.
    - **kwargs: Additional keyword arguments representing shot data.
    """
    print("---Fetching data for ", shotnumber, ", diag: ", diagnostic, " ---")
    # Initialize shot data dictionary
    shot_data = {}

    try:
        if diagnostic.upper() == 'IDE':
            sfr = sf.SFREAD(shotnumber, 'IDE')  # Check if IDE shotfile exists
            if sfr.status:
                equ = sf.EQU(shotnumber, diag='IDE')  # Shotfile data class of the equilibrium
                Geq = sf.SFREAD(shotnumber, 'IDG')  # Shotfile data class of the scalar values
            else:
                exp = try_exp_list(shotnumber, exps)    #try to find the shotfile in the experiment list (Lrado and micdu)
                if exp is not None:
                    equ = sf.EQU(shotnumber, diag='IDE', experiment = exp)  # Shotfile data class of the equilibrium
                    Geq = sf.SFREAD(shotnumber, 'IDG', experiment = exp) 
                else:
                    #save_not_found_shots(shotnumber, "IDE_not_found.txt")
                    with open("IDE_not_found.txt", "a") as file:
                        file.write(str(shotnumber) + "\n")
                    return shot_data
        elif diagnostic.upper() == 'EQH':
            sfr = sf.SFREAD(shotnumber, 'EQH')  # Check if EQI shotfile exists
            if sfr.status:
                equ = sf.EQU(shotnumber, diag='EQH')  # Shotfile data class of the equilibrium
                Geq = sf.SFREAD(shotnumber, 'GQH')  # Shotfile data class of the scalar values
            else:
                return shot_data
        else:
            # Custom error here
            raise ValueError(f"Diagnostic '{diagnostic}' not known to datastructure")
    except Exception as e:
        print(f"Error: {e}")
        return shot_data

    #read scalar values for Geq (IDG and GQH)
    signal_objects = get_signal_objects(Geq, 'Zgeri', 'Zgera', 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95', 'k')
    #read power singals
    TOT_objects = get_signal_objects(sf.SFREAD(shotnumber, 'TOT'), 'PNBI_TOT', 'PICR_TOT','PECR_TOT', 'P_TOT', TB=['P_TOT'])
    #read plamsa current
    FPC_objects = get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    #read Toroidal Field (Ro=1.65m)
    MAI_objects = get_signal_objects(sf.SFREAD(shotnumber, 'MAI'), 'BTF', TB=['BTF'])
    #read H/L mode qualifier, L-mode when <0,6
    TTH_objects = get_signal_objects(sf.SFREAD(shotnumber, 'TTH'), 'H/L-facs')
    #density readout corrected available when DCK_status = True otherwise DCN uncorrected for fringe jumps
    DCN_objects = get_DCN_objects(shotnumber)

    rhop_tor = sf.mapeq.rho2rho(equ, equ.rho_tor_n, coord_in = "rho_tor", coord_out = "rho_pol")
    rhop_psi = np.sqrt(equ.psiN)
    # Create the shot data dictionary with nested structure
    shot_data = {
        'pres': equ.pres,
        'dpres': equ.dpres,
        'psiN': equ.psiN,
        'rhot': equ.rho_tor_n,
        'rhop_tor': rhop_tor,
        'rhop_psi': rhop_psi,
        'time': equ.time,
        **signal_objects,
        **TOT_objects,
        **FPC_objects,
        **MAI_objects,
        **TTH_objects,
        **DCN_objects,
        'tcIps': kwargs.get('tcIps', equ.time[0] ),
        'tcIpe': kwargs.get('tcIpe', equ.time[-1] ),
    }
    #print(shot_data)
    return shot_data