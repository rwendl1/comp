import matplotlib.pyplot as plt
import numpy as np
import os
import pickleboi
import utils


# Example usage
point1 = (0, 0)
point2 = (-3, -4)
angles = [0, np.pi / 6, np.pi / 4, np.pi / 3, np.pi]  # Angles in radians

angles = np.linspace(0, 2*np.pi, 9)  # Angles in radians


dis_points = np.sqrt((point2[0]-point1[0]) **2 + (point2[1]-point1[1])**2)

angle2horz = utils.angle_to_horizontal(point1, point2)
gamma = np.pi - angle2horz
for angle in angles:
    distance = dis_points * np.cos(gamma + angle)
    print(F' winkel: {angle:.2f} has distance:  {distance:.2f} ')