import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np

#Fetching magnetic surface contours

equ = sf.EQU(40128, diag='eqh')
rhop = [0.1,0.2,0.3, 1]
r2, z2 = sf.rho2rz(equ, rhop, t_in=3, coord_in='rho_pol')
print(r2.shape)
print(z2.shape)
print(len(r2[0]))
print(len(r2[0][0]))
n_theta = len(r2[0][-1])
#print(r2[0][-1])
print(n_theta)
theta = np.linspace(0, 2*np.pi, 2*n_theta)
#print(theta)
r1, z1 = sf.rhoTheta2rz(equ, rhop, theta, t_in=3, coord_in='rho_pol')
print(r1.shape)
print(z1.shape)
plt.figure(1, figsize=(10, 7))
for jrho, rho in enumerate(rhop):
    plt.subplot(1, len(rhop), jrho + 1, aspect='equal')
    plt.title('Rho = %8.4f' %rho)
    plt.plot(r2[0][jrho], z2[0][jrho],  label='EQH_rho2rz')
    plt.plot(r1[0, :, jrho], z1[0, :, jrho],  label='EQH_rhoTheta2rz')


equ = sf.EQU(40128, diag='ide')
r2, z2 = sf.rho2rz(equ, rhop, t_in=3, coord_in='rho_pol')
n_theta = len(r2[0][-1])
theta = np.linspace(0, 2*np.pi, 2*n_theta)
r1, z1 = sf.rhoTheta2rz(equ, rhop, theta, t_in=3, coord_in='rho_pol')
print(r1.shape)
plt.figure(1, figsize=(10, 7))
for jrho, rho in enumerate(rhop):
    plt.subplot(1, len(rhop), jrho + 1, aspect='equal')
    plt.title('Rho = %8.4f' %rho)
    plt.plot(r2[0][jrho], z2[0][jrho], label='IDE_rho2rz')
    plt.plot(r1[0, :, jrho], z1[0, :, jrho], label='IDE_rhoTheta2rz')
    
plt.legend(loc='center left',)
plt.show()

