import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Create some sample data (e.g., a sine wave)
t = np.linspace(0, 2*np.pi, 100)
signal = np.sin(t)

# Create a figure and an axis
fig, ax = plt.subplots()
line, = ax.plot(t, signal)

# Update function for animation
def update(frame):
    # Shift the signal by a small amount in each frame
    shifted_signal = np.sin(t + frame * 0.1)
    line.set_ydata(shifted_signal)
    return line,

# Create the animation
ani = FuncAnimation(fig, update, frames=range(100), interval=50)

# Show the plot
plt.xlabel('Time')
plt.ylabel('Signal Amplitude')
plt.title('Dynamic Signal')
plt.show()