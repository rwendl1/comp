import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os
#import pickle
import pickleboi

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

# Populate the shot data
shotlist_file = 'shotlist.txt'
pickle_file = 'shot_data.pickle'
diagnostics = ['IDE', 'EQH'] 
#populate_data_from_list(shotlist_file, pickle_file)
shotnumber = 40128
diag = 'IDE'

loaded_shot_data = pickleboi.load_shot_data(pickle_file)


print(loaded_shot_data.keys())
data = loaded_shot_data[shotnumber][diag]


equ = sf.EQU(shotnumber)
psiN = data['psiN'] 
rhop = data['rhop_tor']
print(rhop.shape)
print(rhop[:,40].shape)
rhop_psi = data['rhop_psi']
r2, z2 = sf.rho2rz(equ, rhop[:,40], t_in=3, coord_in='rho_pol')
print(r2.shape)
n_theta = len(r2[0][-1])
theta = np.linspace(0, 2*np.pi, 2*n_theta)
r1, z1 = sf.rhoTheta2rz(equ, rhop, theta, t_in=3, coord_in='rho_pol')
print(r1.shape)
plt.figure(1, figsize=(10, 7))
for jrho, rho in enumerate(rhop):
    plt.subplot(1, len(rhop), jrho + 1, aspect='equal')
    plt.title('Rho')
    plt.plot(r2[0][jrho], z2[0][jrho], label='rho2rz')
    plt.plot(r1[0, :, jrho], z1[0, :, jrho], label='rhoTheta2rz')
    plt.legend()

plt.show()