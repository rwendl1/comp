import aug_sfutils as sf
import matplotlib.pyplot as plt


gc_d = sf.getgc()

plt.figure(1, figsize=(9, 7))
plt.subplot(1, 1, 1, aspect='equal')
for gc in gc_d.values():
    plt.plot(gc.r, gc.z, 'b-')

plt.show()