import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np


#calcs magnetic fields at any {R,z} 


equ = sf.EQU(28053, diag='EQI', ed=1)
# r_in and z_in must have the same length, they are paired
Rin = [1.35, 1.65, 1.95]
zin = [0., -0.6, 0.6]
br, bz, bt = sf.rz2brzt(equ, r_in=Rin, z_in=zin, t_in=None)
bp = np.hypot(br, bz)
plt.figure(1, figsize=(10, 7))
n_rz = len(Rin)
for j_rz in range(n_rz):
    plt.subplot(1, n_rz, j_rz + 1)
    plt.plot(equ.time, br[:, j_rz], label='Br')
    plt.plot(equ.time, bz[:, j_rz], label='Bz')
    plt.plot(equ.time, bp[:, j_rz], label='Bpol')
    plt.plot(equ.time, bt[:, j_rz], label='Btor')
    plt.legend()

plt.show()