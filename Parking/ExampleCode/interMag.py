import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np

#Intersections line / magnetic surface

equ = sf.EQU(28053)
R, z = sf.cross_surf(equ, rho=0.6, t_in=None, r_in=2., z_in=.2, theta_in=np.radians(20))
plt.figure(1, figsize=(10, 7))

plt.subplot(1, 2, 1)
plt.plot(equ.time, R[:, 0], 'r-', label='R of sec1')
plt.plot(equ.time, R[:, 1], 'g-', label='R of sec1')
plt.plot(equ.time, equ.Rmag  , 'm-', label='Rmag')
plt.plot(equ.time, equ.Rzunt , 'b-', label='Rzunt')
plt.plot(equ.time, equ.Rzoben, 'k-', label='Rzoben')
plt.legend()

plt.subplot(1, 2, 2)
plt.plot(equ.time, z[:, 0], 'r-', label='z of sec1')
plt.plot(equ.time, z[:, 1], 'g-', label='z of sec1')
plt.plot(equ.time, equ.Zmag  , 'm-', label='Zmag')
plt.plot(equ.time, equ.Zunt  , 'b-', label='Zunt')
plt.plot(equ.time, equ.Zoben , 'k-', label='Zoben')
plt.legend()

plt.show()