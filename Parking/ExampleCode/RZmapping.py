import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np

#plots ti over rho_tor oder so

equ = sf.EQU(28053)
cez = sf.SFREAD(equ.shot, 'CEZ')
if cez.status:
    tim_cez = cez.gettimebase('Ti_c')
    rmaj = cez('R_time').T
    zin  = cez('z_time').T
    Ti   = cez('Ti_c')
    rho_t = sf.rz2rho(equ, rmaj, zin, t_in=tim_cez, coord_out='rho_tor', extrapolate=False)
    jt_plot = len(tim_cez)//2
    plt.figure(1)
    plt.xlabel(r'$\rho_{tor}$')
    plt.ylabel('Ti [keV]')
    plt.figtext(0.5, 0.95, '#%s  at t = %9.4f' %(equ.shot, tim_cez[jt_plot]))
    plt.plot(rho_t[jt_plot, :], Ti[jt_plot, :], 'go')
    plt.show()