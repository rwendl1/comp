import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np

#Getting the (outer) magnetic surface of a given q value

equ = sf.EQU(28053, diag='EQH', ed=1)
tin = [2.2, 4., 3., 4.0001]
qval = 2
#tin=eq.time
qs = sf.get_q_surf(equ, qval, t_in=tin)
print(tin)
print(qs)
plt.figure('qsurf')
if len(np.atleast_1d(tin)) < 8:
    plt.plot(tin, qs, 'ro')
else:
    plt.plot(tin, qs)

plt.show()