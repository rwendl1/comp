import aug_sfutils as sf
import matplotlib.pyplot as plt

equ = sf.EQU(37388,diag='eqi')
sfequ = sf.SFREAD(37388, 'eqi')

print(sfequ.time)   #prints the creation time of the shotfile

#plt.plot(equ.psiN.transpose(), equ.pres.transpose())
#plt.show()
#plt.savefig("dummyname.png")

sfidg = sf.SFREAD(40128, 'idg')

print(sfidg.time)

idg = sf.EQU(40128, diag = 'idE')

print(idg.rho_tor_n)