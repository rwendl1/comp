#!/usr/bin/env python
#Imports
import dd
import numpy as np
import matplotlib.pyplot as plt
#Kruecken
#np.set_printoptions(threshold='nan')
#Programm
def co_ar(shot,ed1):
	#Aufruf von Daten
	idf = dd.shotfile('IDF',shot,experiment = 'JILLER',edition = ed1)
	t_sep = idf('sepR').time
	r_sep = idf('sepR').data
	z_sep = idf('sepZ').data
	
	
	#Ausschluss von X-PKT & Z ausserhalb
	#X-Pkt Koordinaten
	ide = dd.shotfile('IDE',shot,experiment = 'JILLER',edition = ed1)
	Rxpu = ide('RPFx').data[:,1]
	Zxpu = ide('zPFx').data[:,1]
	t_indices = np.arange(np.shape(t_sep)[0])
	for t_in in t_indices:
		ind_under_x = np.where(z_sep[t_in]<Zxpu[t_in])
		r_sep[t_in,ind_under_x]=0
		z_sep[t_in,ind_under_x]=0
	
	#Indizierung
	index_half = np.shape(r_sep)[1]

	#Magnetic Center
	r_mag_a = np.empty((0,index_half))
	z_mag_a = np.empty((0,index_half))
	for time in t_sep:
		r_mag_a = np.append(r_mag_a,[np.ones(index_half)*1.65],axis=0)
		z_mag_a = np.append(z_mag_a,[np.ones(index_half)*0],axis=0)

	r_sep1 = r_sep-r_mag_a
	z_sep1 = z_sep-z_mag_a

	
	#make phi array
	phi_sep = np.arctan2(z_sep1,r_sep1)
	
	return t_sep, r_sep, z_sep, phi_sep
