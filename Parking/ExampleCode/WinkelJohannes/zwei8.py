'''jetzt mit neuer Zeitspur'''

#usr/bin/env python
import eins_nox1 as eins
import numpy as np
import matplotlib.pyplot as plt

import warnings
import dd
#Kruecken
np.set_printoptions(threshold='nan')
shot = 33178
ed1 = 25
ed2 = 99

                                                                                                        
#programm
#Datenimport
t_sep1, b, c, c = eins.co_ar(shot,ed1)
if ed2==99:
	import map_equ
	t_sep2 = t_sep1
	rhopol = 1.00
	
	eqm = map_equ.equ_map()
	eqm.Open(shot,'EQH')
	r_eqh = eqm.rho2rz(rhopol,t_sep2)[0]
	z_eqh = eqm.rho2rz(rhopol,t_sep2)[1]
	t_ind_eqh = np.arange(np.shape(t_sep2)[0])
	#leere arrays um Ergebnisse zu speichern
	r_sep2 = np.zeros((t_ind_eqh[-1]+1,400))
	z_sep2 = np.zeros((t_ind_eqh[-1]+1,400))
	phi_sep2 = np.zeros((t_ind_eqh[-1]+1,400))
	#Werte fuer X-Pkt
	eqh = dd.shotfile('EQH',shot)
	txpu_eqh = eqh('time').data
	Rxpu_eqh = eqh('RPFx').data
	#data[:,1]
	Zxpu_eqh = eqh('zPFx').data[:,1]
	for t_index in t_ind_eqh:
		#r indexing
		r_inds = np.arange(np.shape(r_eqh[t_index])[1])
		for r_ind in r_inds:
			#Eintraege der Leeren Arrays bevoelkern
			r_t_ind = r_eqh[t_index][0][r_ind]
			z_t_ind = z_eqh[t_index][0][r_ind]
			r_sep2[t_index][r_ind] = r_t_ind
			z_sep2[t_index][r_ind] = z_t_ind
					
			#Winkelberechnung
			r_mag = r_t_ind-1.65
			z_mag = z_t_ind
			#phi_t_ind = np.arctan2(z_mag,r_mag)
			#print phi_t_ind
			#phi_sep2[t_index,r_ind] = z_t_ind
		#for EQH find time equivalent for X-Pt
		idx = (np.abs(txpu_eqh-t_sep2[t_index])).argmin()
		#cut out X-point
		ind_under_x2 = np.where(z_sep2[t_index]<Zxpu_eqh[idx])
		r_sep2[t_index][ind_under_x2]=0
		z_sep2[t_index][ind_under_x2]=0
		
	phi_sep2 = np.arctan2(z_sep2,r_sep2-1.65)
	
	#IDE same
	eqm1 = map_equ.equ_map()
	ide = dd.shotfile('IDE',shot,experiment = 'JILLER',edition = ed1)

	eqm1.Open(shot,'IDE',exp = 'JILLER',ed = ed1)
	Zxpu_ide = ide('zPFx').data[:,1]
	print (Zxpu_ide)

	
	t_sep = np.linspace(t_sep1[0],t_sep1[-1],np.shape(Zxpu_ide)[0])
	r_eqh1 = eqm1.rho2rz(rhopol,t_sep1)[0]
	z_eqh1 = eqm1.rho2rz(rhopol,t_sep1)[1]
	t_ind_eqh1 = np.arange(np.shape(t_sep1)[0])
	#leere arrays um Ergebnisse zu speichern
	r_sep1 = np.zeros((t_ind_eqh1[-1]+1,400))
	z_sep1 = np.zeros((t_ind_eqh1[-1]+1,400))
	phi_sep1 = np.zeros((t_ind_eqh1[-1]+1,400))
	#Werte fuer X-Pkt
	Rxpu_ide = ide('RPFx').data[:,1]
	Zxpu_ide = ide('zPFx').data[:,1]
	
	
	for t_index1 in t_ind_eqh1:
		
		#r indexing
		r_inds1 = np.arange(np.shape(r_eqh1[t_index1])[1]) 
		for r_ind1 in r_inds1:
			#Eintraege der Leeren Arrays bevoelkern
			r_t_ind1 = r_eqh1[t_index1][0][r_ind1]
			z_t_ind1 = z_eqh1[t_index1][0][r_ind1]
			r_sep1[t_index1][r_ind1] = r_t_ind1
			z_sep1[t_index1][r_ind1] = z_t_ind1
					
		#cut out X-point
		ind_under_x1 = np.where(z_sep1[t_index1]<Zxpu_ide[t_index1])
		r_sep1[t_index1][ind_under_x1]=0
		z_sep1[t_index1][ind_under_x1]=0
	#Winkelarray
	phi_sep1 = np.arctan2(z_sep1,r_sep1-1.65)
else:
	t_sep1, r_sep1, z_sep1, phi_sep1 = eins.co_ar(shot,ed1)
	t_sep2, r_sep2, z_sep2, phi_sep2 = eins.co_ar(shot,ed2)

#ELM Aussschluss
#import elm times
exceptions = np.array([34304,34218,33857,34532,33864,33692,33421,34218,34219,34658,34660,34664,34671,34630])
if not exceptions.__contains__(shot):
	elm = dd.shotfile('ELM',shot)
	t_endELM = elm('t_endELM')
	a_elm1 = np.column_stack((t_endELM.time,t_endELM.data))
	a_elm2 = np.column_stack((t_endELM.time,t_endELM.data))
	#Cut t_range to range of elm shotfile
	elm_t_c_ind1 = np.where((t_sep1>a_elm1[0,0])&(t_sep1<a_elm1[-1,0]))
	elm_t_c_ind2 = np.where((t_sep2>a_elm1[0,0])&(t_sep2<a_elm1[-1,0]))
	t_sep1 = t_sep1[elm_t_c_ind1]
	r_sep1 = r_sep1[elm_t_c_ind1]
	z_sep1 = z_sep1[elm_t_c_ind1]
	phi_sep1 = phi_sep1[elm_t_c_ind1]
	
	t_sep2 = t_sep2[elm_t_c_ind2]
	r_sep2 = r_sep2[elm_t_c_ind2]
	z_sep2 = z_sep2[elm_t_c_ind2]
	phi_sep2 = phi_sep2[elm_t_c_ind2]
	#Cut out elm times
	ausschuss1 = np.empty((0,1))
	for row1 in a_elm1:
		for element1 in t_sep1:
			if element1 < row1[1]+(2e-3) and element1 > row1[0]-(0.7e-3):
				ausschuss1 = np.append(ausschuss1,[[element1]],axis=0)
	cut_ind1 =  np.where(np.in1d(t_sep1,ausschuss1[:,0])==False)
	t_sep1 = t_sep1[cut_ind1]
	r_sep1 = r_sep1[cut_ind1]
	z_sep1 = z_sep1[cut_ind1]
	phi_sep1 = phi_sep1[cut_ind1]
	# sep2 data
	ausschuss2 = np.empty((0,1))
	for row2 in a_elm2:
		for element2 in t_sep2:
			if element2 < row2[1]+(2e-3) and element2 > row2[0]-(0.7e-3):
				ausschuss2 = np.append(ausschuss2,element2)
	cut_ind2 =  np.where(np.in1d(t_sep2,ausschuss2)==False)
	t_sep2 = t_sep2[cut_ind2]
	r_sep2 = r_sep2[cut_ind2]
	z_sep2 = z_sep2[cut_ind2]
	phi_sep2 = phi_sep2[cut_ind2]


#only use same timepoints
oneintwo = np.where(np.in1d(t_sep1,t_sep2)==True)
twoinone = np.where(np.in1d(t_sep2,t_sep1)==True)
r_sep1 = r_sep1[oneintwo]
t_sep1 = t_sep1[oneintwo]
z_sep1 = z_sep1[oneintwo]
phi_sep1 = phi_sep1[oneintwo]
r_sep2 = r_sep2[twoinone]
z_sep2 = z_sep2[twoinone]
t_sep2 = t_sep2[twoinone]
phi_sep2 = phi_sep2[twoinone]
#empty arrays for all timepoints
a_t_sep1 = np.empty((0))
a_dist1 = np.empty((0,400))
a_r_sep1 = np.empty((0,400))
a_z_sep1 = np.empty((0,400))
a_phi_sep1 = np.empty((0,400))

 
#first indexing
#t_indices = np.array([779])
t_indices = np.arange(np.shape(r_sep1)[0])
#Zeitindizierung der Daten
for t_index in t_indices:
	t_sep1_t = t_sep1[t_index]
	r_sep1_t = r_sep1[t_index]
	z_sep1_t = z_sep1[t_index]
	phi_sep1_t = phi_sep1[t_index]
	t_sep2_t = t_sep2[t_index]
	r_sep2_t = r_sep2[t_index]
	z_sep2_t = z_sep2[t_index]
	phi_sep2_t = phi_sep2[t_index]
	
	
		
	#arrays
	a_dist = np.zeros(400,)
	a_r1 = np.zeros(400,)
	a_z1 = np.zeros(400,)
	a_phi = np.zeros(400,)
	
	r_indices = np.arange(np.shape(z_sep1_t)[0])
	for r_index in r_indices:
		phi_sep1_t2 = phi_sep1_t[r_index]
		# Finde die naechsten zwei pkte
		cl_ind = np.argsort(np.abs(phi_sep2_t-phi_sep1_t2))
		if phi_sep2_t[cl_ind[0]]<phi_sep2_t[cl_ind[1]]:
			r1 = r_sep2_t[cl_ind[0]]
			z1 = z_sep2_t[cl_ind[0]]
			r2 = r_sep2_t[cl_ind[1]]
			z2 = z_sep2_t[cl_ind[1]]
		else:
			r2 = r_sep2_t[cl_ind[0]]
			z2 = z_sep2_t[cl_ind[0]]
			r1 = r_sep2_t[cl_ind[1]]
			z1 = z_sep2_t[cl_ind[1]]
		
		# Ausgangspunkt
		
		r_med = r_sep1_t[r_index]
		z_med = z_sep1_t[r_index]
		
		#Neuer Abstand ueber lindalg
		p1 = np.array([r1,z1])
		p2 = np.array([r2,z2])
		#P3 fuer abstand senkrecht zur separatrix
		p3 = np.array([r_med,z_med])
		
		dist1 = (np.cross(p2-p1,p3-p1))/np.linalg.norm(p2-p1)
		
		#in r umrechnen
		#dist1= dist1*np.cos(np.arctan2((r2-r1),abs(z2-z1)))
		
		#ALT
		#a_dist[r_index] = np.linalg.norm(np.cross(p2-p1,p3-p1))/np.linalg.norm(p2-p1)
		
		#Eintraege machen
		a_dist[r_index] = dist1
		
		#Aussortieren von Nasenpkten
		if ((r1+z1)*(r2+z2)*(r_med+z_med))==0:
			a_dist[r_index] = np.nan
			
		
			
		a_r1[r_index] = r_med
		a_z1[r_index] = z_med
		a_phi[r_index] = phi_sep1_t2
		
		
		#Ausreisser plotten
		'''if t_sep1[t_index]>0:
			if abs(a_dist[r_index])>0.05 :
				plt.plot(r1,z1,linestyle='_',marker='+',color='red',markersize=13)
				plt.plot(r_med,z_med,linestyle='_',marker='x',color='blue',markersize=13)
				plt.plot(r2,z2,linestyle='_',marker='x',color='red',markersize=13)
				#ganze seps
				plt.plot(r_sep2_t,z_sep2_t,color='r',linestyle='_',marker='.',markersize=2,label='ed:'+str(ed2))
				plt.plot(r_sep1_t,z_sep1_t,color='b',linestyle='_',marker='.',markersize=2,label='ed:'+str(ed1))
				#plot settings
				plt.grid('on')
				plt.legend(loc=3)
				plt.title('delta:'+str(a_dist[r_index])+'m Zeit:'+str(t_sep1_t)+'s Winkel:'+str(phi_sep1_t2/np.pi*180)+'grad')
				plt.show()'''
		
		
		
	a_t_sep1 = np.append(a_t_sep1,[t_sep1_t],axis=0)
	a_dist1 = np.append(a_dist1,[a_dist],axis=0)
	a_r_sep1 = np.append(a_r_sep1,[a_r1],axis=0)
	a_z_sep1 = np.append(a_z_sep1,[a_z1],axis=0)
	a_phi_sep1 = np.append(a_phi_sep1,[a_phi],axis=0)
	
nasenin = np.where((a_r_sep1==0)&(a_z_sep1==0))
a_dist1[nasenin]=np.nan
a_r_sep1[nasenin]=np.nan
a_z_sep1[nasenin]=np.nan
a_phi_sep1[nasenin]=np.nan


		
f_a =  np.column_stack((a_t_sep1, a_dist1, a_r_sep1, a_z_sep1, a_phi_sep1))
print (np.shape(f_a))

np.save('ergebnisse3'+str(shot)+'_'+str(ed1)+'vs'+str(ed2)+'2.npy',f_a)
		
