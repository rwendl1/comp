#import vier1
import numpy as np
import matplotlib.pyplot as plt
#Kruecken
np.set_printoptions(threshold='nan')
color1 = 'm'
color2 = 'green'
color3 = 'crimson'
color4 = 'darkorange'
color_a = np.array(['cornflowerblue','forestgreen','orangered','magenta','yellow','cyan'])

nr_pts = 80
#UI
shot = int(raw_input('shot:'))
t0 = float(raw_input('t0:'))
t1 = float(raw_input('t1:'))
ed_ls = np.empty((0,1),dtype=int)
edi1 = int(raw_input('edition 0:'))
ed = raw_input('edition:')
plt.figure(figsize=(24,12))
while not ed=='':
	ed_ls = np.append(ed_ls,[int(ed)])
	ed = raw_input('edition:')


ind_c=0
for edi2 in ed_ls:
	#Programm
	if edi2 > edi1:
		g = np.load('ergebnisse3'+str(shot)+'_'+str(edi1)+'vs'+str(edi2)+'.npy')
		
		i = 1
	else:
		g = np.load('ergebnisse3'+str(shot)+'_'+str(edi2)+'vs'+str(edi1)+'.npy')
		# vzwechsel falls umdrehen
		i = -1

	#Indexing fuer ganze arrays
	in_max = (np.shape(g)[1]-1)/4
	a_t_sep1 = g[:,0]
	a_dist1 = i*g[:,1:in_max+1]
	a_r_sep1 = g[:,(in_max+1):((2*in_max)+1)]
	a_z_sep1 = g[:,((2*in_max)+1):((3*in_max)+1)]
	a_phi_sep1 = g[:,((3*in_max)+1):((4*in_max)+1)]

	time_inds = np.where((a_t_sep1>=t0)&(a_t_sep1<t1))[0]
	phi_indices = np.linspace((-np.pi),np.pi,nr_pts+1)
	phi_indices1 = np.arange(np.shape(phi_indices)[0]-1)
	num_cuts = np.shape(phi_indices1)[0]
	f_a_dist = np.empty((0,num_cuts))
	for t_ind in time_inds:
		#print a_t_sep1[t_ind]
		a_r_sep1_t = (a_r_sep1[t_ind])
		a_z_sep1_t = (a_z_sep1[t_ind])
		a_dist1_t = a_dist1[t_ind]
		a_phi_sep1_t = a_phi_sep1[t_ind]
		a_res_dist_t = np.empty((0))
		for index in phi_indices1:
			zehn = np.where((phi_indices[index]<a_phi_sep1_t)&(phi_indices[index+1]>=a_phi_sep1_t))
			a_res_dist_t = np.append(a_res_dist_t,np.nanmean((a_dist1_t)[zehn]))
		f_a_dist = np.append(f_a_dist,[a_res_dist_t],axis=0)
	dist_mean = np.nanmean(f_a_dist,axis=0)
	wkl_achsenbeschr = phi_indices[1:]/np.pi*180
	if edi2 == 99:
		lbl = r'w/ ELMs'
		plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=10)

	else:
		if edi2 == 9999:
			lbl = r'w/o ELMS'
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color = color4)
		elif shot==33134 and edi2==29:
			lbl = r'no lower PSL current fit'
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color=color_a[ind_c])
		elif shot==33134 and edi2 == 30:
			lbl = r'no upper PSL current fit'
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color=color_a[ind_c])
		elif shot ==33134 and edi2 ==31:
			lbl = r'no PSL current fit'
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color=color_a[ind_c])
		elif shot == 33134 and edi2 == 62 and 74 in ed_ls:
			lbl = r'IDE preprocessing'
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color=color_a[ind_c])
		elif shot == 33134 and edi2 == 74:
			lbl = r'CLISTE preprocessing'
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color=color_a[ind_c])
			
		else: 
			lbl = str(edi2)
			plt.plot(wkl_achsenbeschr,dist_mean*1000,label = str(lbl),lw=7,color=color_a[ind_c])
	
	ind_c=ind_c+1
#Ploteinstellungen
#plt.title(r'#'+str(shot)+' Edition:'+str(edi1)+'  t:'+str(t0)+'-'+str(t1)+'s',fontsize='35')

plt.xlabel('Poloidal Angle',fontsize='65')
plt.ylabel('Distance(mm)',fontsize='65')
plt.xticks(np.arange(-180, 181, 60))
plt.yticks(np.arange(-100, 100 , 2))
#plt.xticks(np.array([-180,150,120]))
plt.tick_params(axis='both', which='both', labelsize=40,length=10,width=3,pad=15)
#plt.axvspan(-93,-60,alpha = 0.3,facecolor='blue')#,label='Pslu')
#plt.axvspan(50,83,alpha = 0.3,facecolor='blue')#,label='Pslo')
plt.axhline(0,alpha=1,c='k',lw=10,linestyle='--')
plt.axvline(-100,alpha=1,c=color1,lw=10,linestyle='--')
plt.axvline(-75,alpha=1,c=color3,lw=10,linestyle='--')
plt.axvline(0,alpha=1,c=color2,lw=10,linestyle='--')

axes = plt.gca()
axes.set_xlim([-180,180])

for axis in ['top','bottom','left','right']:
  axes.spines[axis].set_linewidth(5)
#plt.tight_layout()
fs_txt = 50

#plt.legend(loc=4,fontsize=25)
plt.gcf().subplots_adjust(bottom=0.15)
plt.title('# '+str(shot)+' @ '+str(t0)+'-'+str(t1)+'s',fontsize=60,y=1.02)
#plt.title('#33134  Lower PSL: Measured Data',fontsize=60,y=1.02)

#5.2
if shot == 33134 and edi1 == 62 and 75 in ed_ls:
	print('fig.5.2')
	axes.text(-71.5,1.8,r'TS',fontsize=fs_txt,rotation=90,color=color3)
	axes.text(3,-2,r'Midplane',fontsize=fs_txt,rotation=90,color=color2)
	axes.text(-97.5,7.8,r'X-Point',fontsize=fs_txt,rotation=90,color=color1)
	axes.set_ylim([-11,19])
	plt.savefig('33134_sens_bsp_abst.svg',bbox_inches='tight',transparent=True)
	
#5.3
elif shot == 33134 and edi1 ==62 and 64 in ed_ls:
	print ('fig.5.3')
	axes.text(-71.5,-2.8,r'TS',fontsize=fs_txt,rotation=90,color=color3)
	axes.text(3,-6,r'Midplane',fontsize=fs_txt,rotation=90,color=color2)
	axes.text(-97.5,-2,r'X-Point',fontsize=fs_txt,rotation=90,color=color1)
	axes.set_ylim([-11,2])
	#plt.savefig('pol_arrays_dist_profile.eps',bbox_inches='tight',transparent=True)
	plt.savefig('pol_arrays_dist_profile.svg',bbox_inches='tight',transparent=True)
#5.7
elif shot == 33134 and edi1 ==99 and 74 in ed_ls:
	print ('fig.5.7')
	axes.text(-71.5,2.5,r'TS',fontsize=fs_txt,rotation=90,color=color3)
	axes.text(3,16,r'Midplane',fontsize=fs_txt,rotation=90,color=color2)
	axes.text(-112.5,18.5,r'X-Point',fontsize=fs_txt,rotation=90,color=color1)
	axes.set_ylim([-5,22])
	plt.legend(loc=1,fontsize=35)
	plt.savefig('33134_wkldist_eqh_pre.eps',bbox_inches='tight',transparent=True)
	
#5.8
elif shot == 33054 and edi1 ==1 and 2 in ed_ls:
	print ('fig.5.8')
	axes.text(-71.5,3,r'TS',fontsize=fs_txt,rotation=90,color=color3)
	axes.text(3,2,r'Midplane',fontsize=fs_txt,rotation=90,color=color2)
	axes.text(-97.5,3,r'X-Point',fontsize=fs_txt,rotation=90,color=color1)
	axes.set_ylim([-0.5,4.5])
	plt.savefig('dist_pol_field_coils.eps',bbox_inches='tight',transparent=True)

#5.10
elif shot == 33134 and edi1 ==62 and 30 in ed_ls or 29 in ed_ls or 31 in ed_ls:
	print ('fig.5.10')
	axes.text(-71.5,-2.5,r'TS',fontsize=fs_txt,rotation=90,color=color3)
	axes.text(3,-3.5,r'Midplane',fontsize=fs_txt,rotation=90,color=color2)
	axes.text(-112.5,-2.5,r'X-Point',fontsize=fs_txt,rotation=90,color=color1)
	plt.axvspan(-93,-60,alpha = 0.1,facecolor='k')#,label='Pslu')
	plt.axvspan(50,83,alpha = 0.1,facecolor='k')#,label='Pslo')
	axes.set_ylim([-8,5])
	#plt.legend(loc=4,fontsize=35)
#	plt.savefig('abstand_no_psl_current.pdf',bbox_inches='tight',transparent=True)
	plt.savefig('abstand_no_psl_current.svg',bbox_inches='tight',transparent=True)


#standard
else:
	axes.text(-71.5,1.8,r'TS',fontsize=fs_txt,rotation=90,color=color3)
	axes.text(3,-2,r'Midplane',fontsize=fs_txt,rotation=90,color=color2)
	axes.text(-97.5,7.8,r'X-Point',fontsize=fs_txt,rotation=90,color=color1)
	axes.set_ylim([-20,20])
	plt.savefig(str(shot)+'_all_eqh_wkl.eps',bbox_inches='tight',transparent=True)
	

plt.show()

	
	


'''np.load
wkl,t,dist = vier1.dist_med(360,0.5)
print np.shape(np.nanmean(dist,axis=1))
print np.shape(wkl)
print t

plt.plot(np.nanmean(wkl,axis=0),np.nanmean(dist,axis=0))
plt.show()'''
