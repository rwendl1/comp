import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

def make_ticklabels_invisible(fig):
    for i, ax in enumerate(fig.axes):
        ax.text(0.5, 0.5, "ax%d" % (i+1), va="center", ha="center")
        for tl in ax.get_xticklabels() + ax.get_yticklabels():
            tl.set_visible(False)



# gridspec inside gridspec

f = plt.figure()

gs0 = gridspec.GridSpec(2, 1)

gs00 = gridspec.GridSpecFromSubplotSpec(3, 2, subplot_spec=gs0[0])

ax1 = plt.Subplot(f, gs00[:, 0])
f.add_subplot(ax1)
ax2 = plt.Subplot(f, gs00[:, 1])
f.add_subplot(ax2)


gs01 = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=gs0[1])

ax4 = plt.Subplot(f, gs01[:, 0])
f.add_subplot(ax4)
ax5 = plt.Subplot(f, gs01[:, 1])
f.add_subplot(ax5)
ax6 = plt.Subplot(f, gs01[:, 2])
f.add_subplot(ax6)

plt.suptitle("GirdSpec Inside GridSpec")
make_ticklabels_invisible(plt.gcf())

plt.show()
