import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os
import pickleboi
import utils



if __name__ == "__main__":
    pickle_file = './shot_data.pickle'
    shotlist_file = './shotlist.txt'
    #scalars = ('Zgeri', 'Zgera', 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    scalars = ( 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)

    #extract shotnumber and iterate over all shotnumbers
    shot_numbers = [shot_info[0] for shot_info in shot_list] 
    for i, shot_number in enumerate(shot_numbers):

        print("current shot ", shot_number)
        shot = shot_data[shot_number]

        #print(shot['IDE']['H/L-facs'])
        #check if H-mode or L-mode
        try:
            if shot['IDE']['H/L-facs'] is not None:
                full_facs = shot['IDE']['H/L-facs']
                facs = full_facs[:,7] 
                plt.plot(shot['IDE']['time'],shot['IDE']['H/L-facs'])
                plt.show()
        except Exception as e:
            print(f"H/L-facs for shot {shot_number} not found. Error {e}")