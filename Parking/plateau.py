import aug_sfutils as sf
import numpy as np
import matplotlib.pyplot as plt

#from utils import find_plateau  # Import the function from utils.py
def find_plateau(signal, timescale):
    # Initialize variables to track the start and end of the plateau
    plateau_start = None
    plateau_end = None
    
    threshold = 0.03    #3% error
    median = np.median(signal)
    diff_pos = median * (1+threshold)
    diff_neg = median * (1-threshold)


    # Iterate through the signal to find the plateau
    for i in range(len(signal)):
        if plateau_start is None:
            # Check if the current value is within a tolerance range of the previous value
            if i > 0 and signal[i] > diff_neg:  # Adjust the tolerance range as needed
                # If plateau_start is not assigned, assign it
                plateau_start = timescale[i]
        # If the current value is not within the tolerance range, and a plateau has been started
        elif plateau_start is not None and signal[i] < diff_neg:
            plateau_end = timescale[i-1]
            break  # Exit loop since plateau end is found
        
    return plateau_start, plateau_end



#main
shots = [40128, 38472, 38474, 38819, 40869, 37388, 38457, 40419, 39870, 29110, 40635]
""" shots = [(40128, 0.90, 4.90),
        (38472, 1.50, 7.00),
        (38474, 1.54, 8,10),
        (38819, 1.50, 8.01), 
        (40869, 2.0, 4.0), 
        (37388, 2.0, 4.0), 
        (38457, 0.87, 5.50), 
        (40419, 1.32, 7.38),
        (39870, 1.30, 7.98),
        (29110, 0.85, 5,40),
        (40635, 1.02, 4,60)] """

# Relative path to the "figures" subfolder                                      
figures_folder = "figures"

for shotnumber in shots:
    fpc = sf.SFREAD(shotnumber, 'fpc')
    if fpc.status:
        print("Shotnumber: ", shotnumber)
        IpiFP= fpc.getobject('IpiFP')
        time = fpc.gettimebase('IpiFP')

        plateau_start, plateau_end = find_plateau(IpiFP, time)
        print("Plateau start:", plateau_start)
        print("Plateau end:", plateau_end)

        plt.plot(time, IpiFP)
        plt.plot(plateau_start, np.ones_like(plateau_start), 'rx')
        plt.plot(plateau_end, np.ones_like(plateau_end), 'ro')
        plt.title(f"{shotnumber}")
        plt.grid(True)

        plt.show()
        plt.savefig(f"{figures_folder}/{shotnumber}.png")
        plt.close()  # Close the plot to avoid displaying multiple plots     

        





plt.show
#plt.savefig("dummyname.png")