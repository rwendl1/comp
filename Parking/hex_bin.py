import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

# Custom formatter for contour labels
def fmt(x):
    print(x)
    return f"{x:.2f}%"

def plot_contour_from_data(x, y, percentage, write_level=False):
    # Calculate the kernel density estimate
    values = np.vstack([x, y])
    kde = gaussian_kde(values)
    density = kde(values)

    custom_labels = ['50%']

    # Create a grid of points and calculate their densities
    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()
    xx, yy = np.meshgrid(np.linspace(xmin, xmax, 100), np.linspace(ymin, ymax, 100))
    grid_points = np.vstack([xx.ravel(), yy.ravel()])
    grid_density = kde(grid_points).reshape(xx.shape)

    # Sort the densities and find the threshold for the given percentile
    sorted_density = np.sort(density)
    cumulative_density = np.cumsum(sorted_density)
    threshold_density = sorted_density[np.searchsorted(cumulative_density, percentage * 0.01 * cumulative_density[-1])]

    # Draw the contour at the threshold density
    contour = plt.contour(xx, yy, grid_density, levels=[threshold_density], colors='red')
    contour.collections[0].set_label(f'{percentage}')

    # Add contour labels
    if write_level:
        plt.clabel(contour, contour.levels, inline=True, fmt=dict(zip([threshold_density], [f'{percentage}%'])), fontsize=10)

    # Plot the scatter and contour
    plt.xlabel('X axis')
    plt.ylabel('Y axis')
    plt.legend()
    plt.title(f'Contour plot with {percentage}% data enclosed')

# Sample data
np.random.seed(0)
x = np.random.randn(1000)
y = np.random.randn(1000)

# Create a hexbin plot
hb = plt.hexbin(x, y, gridsize=30, cmap='Blues', mincnt=1)
plt.colorbar(hb, label='counts')

# Plot the contour for the desired area percentage
plot_contour_from_data(x, y, 50)
plt.savefig('figs/parking/hexbin.jpg')

plt.show()



