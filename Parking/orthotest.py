
import numpy as np
import matplotlib.pyplot as plt

def calculate_distances(point1, point2, angles):
    # Convert points to numpy arrays
    point1 = np.array(point1)
    point2 = np.array(point2)
    
    # Initialize plot
    plt.figure()
    
    # Plot points
    plt.plot(point1[0], point1[1], 'ro', label='Point 1')
    plt.plot(point2[0], point2[1], 'bo', label='Point 2')
    
    # Calculate distances
    distances = []
    for angle in angles:
        # Calculate direction vector of the line from point 1 in the direction of angle
        direction_vector = np.array([np.cos(angle), np.sin(angle)])
        
        # Calculate vector from point 1 to point 2
        vector_to_point2 = point2 - point1
        
        # Calculate orthogonal distance
        distance = np.abs(np.cross(vector_to_point2, direction_vector)) / np.linalg.norm(direction_vector)
        
        distances.append(distance)
        
        # Plot line
        line_end = point1 + direction_vector * 10  # Extend line for visualization
        plt.plot([point1[0], line_end[0]], [point1[1], line_end[1]], label=f'Angle: {np.degrees(angle)}°')
        
        # Plot the calculated lines
        line_start = point1
        line_end = point1 + direction_vector * np.linalg.norm(vector_to_point2)  # Extend line to cover the distance to point2
        plt.plot([line_start[0], line_end[0]], [line_start[1], line_end[1]], 'g--')
    
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Points, Lines, and Distances')
    plt.legend()
    plt.grid(True)
    plt.axis('equal')  # Equal aspect ratio
    
    plt.show()
    
    return distances

# Example usage
point1 = (0, 0)
point2 = (3, 4)
angles = [np.pi / 6, np.pi / 4, np.pi / 3]  # Angles in radians

distances = calculate_distances(point1, point2, angles)
print(distances)
