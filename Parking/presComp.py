import aug_sfutils as sf
import numpy as np
import matplotlib.pyplot as plt

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

#main
#shots = [40128, 38472, 38474, 38819, 40869, 37388, 38457, 40419, 39870, 29110, 40635]

shots = [(38819, 1.50, 8.01)]
""" shots = [(40128, 0.90, 4.90),
        (38472, 1.50, 7.00),
        (38474, 1.54, 8.10),
        (38819, 1.50, 8.01), 
        (40869, 2.0, 4.0), 
        (37388, 2.0, 4.0), 
        (38457, 0.87, 5.50), 
        (40419, 1.32, 7.38),
        (39870, 1.30, 7.98),
        (29110, 0.85, 5.40),
        (40635, 1.02, 4.60)] """
""" shots = [(40128, 0.90, 4.90),
        (38472, 1.50, 7.00), 
        (40869, 2.0, 4.0), 
        (37388, 2.0, 4.0), 
        (38457, 0.87, 5.50)] """

# Relative path to the "figures" subfolder                                      
figures_folder = "pressure"
sample_size = 3

for shotnumber, t_start, t_end in shots:
    print("Shotnumber: ", shotnumber)

    #fetch data from AUGD
    EQH = sf.EQU(shotnumber, diag = 'eqi')
    sfEQH = sf.SFREAD(shotnumber, 'eqi')
    IDA = sf.SFREAD(shotnumber, 'ida', exp='lrado' )
    IDE = sf.EQU(shotnumber, diag = 'ide',exp='lrado'   )
    sfIDE = sf.SFREAD(shotnumber, 'ide', exp='lrado' )
    fpc = sf.SFREAD(shotnumber, 'fpc')                  #for plasma strom
    #IDA_test = sf.EQU(shotnumber, diag = 'ida')        doesnt work -> no EQU od IDA?

    # Create a subplot grid for the current shot number
    fig, axes = plt.subplots(sample_size, 1, figsize=(8, 6 * sample_size))

    # Loop over sample size and define timepoint for each one
    for s in range(sample_size):
        t = (t_end - t_start) / sample_size * s + t_start
        ax = axes[s]
        
        if sfEQH.status:
            # EQH plotting
            idx_EQH = find_nearest(EQH.time, t)
            ax.plot(EQH.psiN.transpose()[:, idx_EQH], EQH.pres.transpose()[:, idx_EQH], label='EQH')

        # Plot IDA if available
        if IDA.status:
            #fetch pe (eletron pressure = ne * Te)
            p_ida = IDA.getobject('pe')
            t_ida = IDA.gettimebase('pe')
            rhop = IDA.getareabase('pe')
            Pe = IDA.getobject('pe')

            #get onto psiN scale as the other equilibria
            idx_IDA = find_nearest(t_ida, t)
            rhop = rhop[:, idx_IDA]
            psiN_rho = np.power(rhop,2) # rho_pol = sqrt von psi_norm
            idx_psi = np.argmax(psiN_rho > 1)   

            #plot
            #ax.plot(psiN_rho[:idx_psi,], Pe[:idx_psi, idx_IDA]/1000, label='pe_IDA') 
            ax.set_title('%s @ t=%3.2fs' % (Pe.descr, t_ida[idx_IDA]))
            ax.set_xlabel('psiN')
            ax.set_ylabel('kPa')


        if sfIDE.status:
        # IDE plotting
            idx_IDE = find_nearest(IDE.time, t)
            ax.plot(IDE.psiN.transpose()[:, idx_IDE], IDE.pres.transpose()[:, idx_IDE], label='IDE')
            #ax.plot(IDE.rho_tor_n.transpose()[:, idx_IDE], IDE.tfl.transpose()[:, idx_IDE], label='IDE')
            #ax.set_title('%s @ t=%3.2fs' % (Pe.descr, t_ida[idx_IDA]))
            #ax.set_xlabel('rho_tor_n')
            #ax.set_ylabel('Vs')
            

        ax.legend()

    plt.show()
    plt.tight_layout()  # Adjust layout to prevent overlap
    #plt.savefig(f"{figures_folder}/{shotnumber}_{sample_size}subplots.png")
    plt.close()  # Close the plot to avoid displaying multiple plots
