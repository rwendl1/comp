import aug_sfutils as sf
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import pickle
import os
import utils
import privmapeq

def get_scalars(shot, t_idx, R_cont, Z_cont):
    rmag = shot['Rmag'][t_idx]
    zmag = shot['Zmag'][t_idx]
    rxpu = shot['Rxpu'][t_idx]
    zxpu = shot['Zxpu'][t_idx]
    raus = shot['Raus'][t_idx]

    angle_mag2X = utils.angle_to_horizontal((rmag, zmag), (rxpu, zxpu))

    zaus_idx = utils.find_idx_of_nearest(R_cont[0, :, -1], raus)
    zaus = Z_cont[0, zaus_idx, -1]
    angle_mag2Raus = utils.angle_to_horizontal((rmag, zmag), (raus, zaus))

    data = {
        'Rmag': rmag,
        'Zmag': zmag,
        'Rxpu': rxpu,
        'Zxpu': zxpu,
        'Raus': raus,
        'Zaus': zaus,
        'angle_mag2x': angle_mag2X,
        'angle_mag2Raus': angle_mag2Raus
    }
    return data

def generate_shot_data(shot_data, shot_list):
    data_dict = {}
    shot_numbers = [shot_info[0] for shot_info in shot_list]

    for shot_number in shot_numbers:
        eqh_shot = shot_data[shot_number]['EQH']
        ide_shot = shot_data[shot_number]['IDE']
        times, eqh_idxs, ide_idxs = utils.create_timestamps(eqh_shot['time'], ide_shot['time'], eqh_shot['tcIps'], eqh_shot['tcIpe'], 1.0)
        
        eqh_equ = sf.EQU(shot_number, diag='EQH')
        ide_equ = sf.EQU(shot_number, diag='IDE')

        rhop = [0.4, 1]
        eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=times, coord_in='rho_pol')
        ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=times, coord_in='rho_pol')

        X_angle_list = []
        Raus_angle_list = []
        shot_data_list = []

        for jtime, time_index in enumerate(eqh_idxs):
            time = times[jtime]
            eqh_idx = eqh_idxs[jtime]
            ide_idx = ide_idxs[jtime]

            eqh_n_theta = len(eqh_r2[jtime][-1])
            ide_n_theta = len(ide_r2[jtime][-1])
            n_theta = min(eqh_n_theta, ide_n_theta)
            theta = np.linspace(-np.pi, np.pi, 2*n_theta)

            eqh_r1, eqh_z1 = sf.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol')
            ide_r1, ide_z1 = sf.rhoTheta2rz(ide_equ, rhop, theta, t_in=time, coord_in='rho_pol')

            eqh_scls = get_scalars(eqh_shot, eqh_idx, eqh_r1, eqh_z1)
            ide_scls = get_scalars(ide_shot, ide_idx, ide_r1, ide_z1)

            X_angle_list.append(ide_scls['angle_mag2x'])
            Raus_angle_list.append(ide_scls['angle_mag2Raus'])
            mag_center_offset = utils.mag_center_offset(eqh_scls, ide_scls, theta)

            shot_data_list.append({
                'time': time,
                'theta': theta,
                'eqh_r1': eqh_r1,
                'eqh_z1': eqh_z1,
                'ide_r1': ide_r1,
                'ide_z1': ide_z1,
                'eqh_scls': eqh_scls,
                'ide_scls': ide_scls,
                'mag_center_offset': mag_center_offset,
                'X_angle_list': X_angle_list,
                'Raus_angle_list': Raus_angle_list
            })

        data_dict[shot_number] = shot_data_list

    with open('processed_shot_data.pickle', 'wb') as f:
        pickle.dump(data_dict, f)

def plot_diff2center(shot_data, shot_list):
    pickle_file = 'processed_shot_data.pickle'
    
    if not os.path.exists(pickle_file):
        generate_shot_data(shot_data, shot_list)
    
    with open(pickle_file, 'rb') as f:
        data_dict = pickle.load(f)

    for shot_number in data_dict:
        shot_data_list = data_dict[shot_number]
        times = [shot_data['time'] for shot_data in shot_data_list]

        fig, axes = plt.subplots(2, 3, figsize=(15, 10), gridspec_kw={'height_ratios': [1, 1]})
        fig.suptitle(f"Difference profile of Shot {shot_number}", fontsize=16)
        plt.subplots_adjust(bottom=0.2)

        rhop = [0.4, 1]

        for shot_data in shot_data_list:
            theta = shot_data['theta']
            mag_center_offset = shot_data['mag_center_offset']
            X_angle_list = shot_data['X_angle_list']
            Raus_angle_list = shot_data['Raus_angle_list']

            for jrho, rho in enumerate(rhop):
                eqh_r1 = shot_data['eqh_r1'][0, :, jrho]
                eqh_z1 = shot_data['eqh_z1'][0, :, jrho]
                ide_r1 = shot_data['ide_r1'][0, :, jrho]
                ide_z1 = shot_data['ide_z1'][0, :, jrho]
                eqh_scls = shot_data['eqh_scls']
                ide_scls = shot_data['ide_scls']

                R_eqh = eqh_r1 - np.ones_like(eqh_r1) * eqh_scls['Rmag']
                z_eqh = eqh_z1 - np.ones_like(eqh_z1) * eqh_scls['Zmag']
                eqh_len = np.sqrt(R_eqh**2 + z_eqh**2)

                R_ide = ide_r1 - np.ones_like(ide_r1) * ide_scls['Rmag']
                z_ide = ide_z1 - np.ones_like(ide_z1) * ide_scls['Zmag']
                ide_len = np.sqrt(R_ide**2 + z_ide**2)

                diff = ide_len - (eqh_len + mag_center_offset)

                ax = axes[0, jrho]
                ax.plot(theta, diff * 1000, label=f'diff @{shot_data["time"]:.2f}s')
                ax.plot(theta, mag_center_offset * 1000, lw=1, linestyle='-.', c='green')

                if rho == 1:
                    scalar_value_eqh = eqh_scls['Raus']
                    scalar_value_ide = ide_scls['Raus']
                    diff_raus = scalar_value_ide - scalar_value_eqh
                    ax.plot(Raus_angle_list[-1], diff_raus*1000, 'rx')

                
                ax.set_title(f"Shot {shot_number} @ Rhop={rho:.2f}")
                ax.set_xlabel("Poloidal Angle theta (rad)")
                ax.set_ylabel('Difference (mm)')
                ax.axhline(0, alpha=1, lw=1, linestyle='--', c='k')
                ax.axvline(0, alpha=1, lw=1, linestyle='--', c='k')
                ax.axvspan(min(X_angle_list), max(X_angle_list), ymin=-10, ymax=10, facecolor='blueviolet', alpha=0.3)
                ax.axvspan(min(Raus_angle_list), max(Raus_angle_list), ymin=-10, ymax=10, facecolor='orange', alpha=0.3)
                ax.legend()

            ax_contour = axes[1, jrho]
            ax_contour.plot(eqh_r1, eqh_z1, label='EQH_rhoTheta2rz', c='r')
            ax_contour.plot(ide_r1, ide_z1, label='IDE_rhoTheta2rz', c='b')
            ax_contour.plot(eqh_r1, eqh_z1, label='EQH_rho2rz', c='r')
            ax_contour.plot(ide_r1, ide_z1, label='IDE_rho2rz', c='b')

            ax_contour.plot(eqh_scls['Rmag'], eqh_scls['Zmag'], 'rx', label='EQH mag center')
            ax_contour.plot(ide_scls['Rmag'], ide_scls['Zmag'], 'bx', label='IDE mag center')

            ax_contour.plot(ide_scls['Rxpu'], ide_scls['Zxpu'], 'x', label='X-Point', color='blueviolet')
            ax_contour.plot((ide_scls['Rmag'], ide_scls['Rxpu']), (ide_scls['Zmag'], ide_scls['Zxpu']),
                            linestyle='--', linewidth=1, color='blueviolet')

            ax_contour.plot(ide_scls['Raus'], ide_scls['Zaus'], 'x', label='Raus', color='orange')
            ax_contour.plot((ide_scls['Rmag'], ide_scls['Raus']), (ide_scls['Zmag'], ide_scls['Zaus']),
                            linestyle='--', linewidth=1, color='orange')

            ax_contour.axhline(y=0, color='black', linestyle='--', linewidth=1)
            ax_contour.axhline(y=eqh_scls['Zmag'], color='red', linestyle='--', linewidth=1)
            ax_contour.axhline(y=ide_scls['Zmag'], color='blue', linestyle='--', linewidth=1)
            if rho == 1.0:
                ax_contour.axvline(x=eqh_scls['Raus'], color='red', linestyle='--', linewidth=1)
                ax_contour.axvline(x=ide_scls['Raus'], color='blue', linestyle='--', linewidth=1)
            ax_contour.set_title(f"rhoTheta2rz contour @ {shot_data['time']:.2f}s")
            ax_contour.axis('scaled')
            ax_contour.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        def update(val):
            time_idx = int(slider.val)
            shot_data = shot_data_list[time_idx]
            eqh_scls = shot_data['eqh_scls']
            ide_scls = shot_data['ide_scls']
            eqh_r1 = shot_data['eqh_r1']
            eqh_z1 = shot_data['eqh_z1']
            ide_r1 = shot_data['ide_r1']
            ide_z1 = shot_data['ide_z1']
            X_angle_list = shot_data['X_angle_list']
            Raus_angle_list = shot_data['Raus_angle_list']

            for jrho, rho in enumerate(rhop):
                ax_contour = axes[1, jrho]
                ax_contour.clear()
                ax_contour.plot(eqh_r1[0, :, jrho], eqh_z1[0, :, jrho], label='EQH_rhoTheta2rz', c='r')
                ax_contour.plot(ide_r1[0, :, jrho], ide_z1[0, :, jrho], label='IDE_rhoTheta2rz', c='b')
                ax_contour.plot(eqh_r1[0, :, jrho], eqh_z1[0, :, jrho], label='EQH_rho2rz', c='r')
                ax_contour.plot(ide_r1[0, :, jrho], ide_z1[0, :, jrho], label='IDE_rho2rz', c='b')

                ax_contour.plot(eqh_scls['Rmag'], eqh_scls['Zmag'], 'rx', label='EQH mag center')
                ax_contour.plot(ide_scls['Rmag'], ide_scls['Zmag'], 'bx', label='IDE mag center')

                ax_contour.plot(ide_scls['Rxpu'], ide_scls['Zxpu'], 'x', label='X-Point', color='blueviolet')
                ax_contour.plot((ide_scls['Rmag'], ide_scls['Rxpu']), (ide_scls['Zmag'], ide_scls['Zxpu']),
                                linestyle='--', linewidth=1, color='blueviolet')

                ax_contour.plot(ide_scls['Raus'], ide_scls['Zaus'], 'x', label='Raus', color='orange')
                ax_contour.plot((ide_scls['Rmag'], ide_scls['Raus']), (ide_scls['Zmag'], ide_scls['Zaus']),
                                linestyle='--', linewidth=1, color='orange')

                ax_contour.axhline(y=0, color='black', linestyle='--', linewidth=1)
                ax_contour.axhline(y=eqh_scls['Zmag'], color='red', linestyle='--', linewidth=1)
                ax_contour.axhline(y=ide_scls['Zmag'], color='blue', linestyle='--', linewidth=1)
                if rho == 1.0:
                    ax_contour.axvline(x=eqh_scls['Raus'], color='red', linestyle='--', linewidth=1)
                    ax_contour.axvline(x=ide_scls['Raus'], color='blue', linestyle='--', linewidth=1)
                ax_contour.set_title(f"rhoTheta2rz contour @ {shot_data['time']:.2f}s")
                ax_contour.axis('scaled')
                ax_contour.legend(loc='center left', bbox_to_anchor=(1, 0.5))

            ax_pressure = axes[1, 2]
            ax_pressure.clear()
            ax_pressure.set_title("Pressure Curve")
            ax_pressure.set_xlabel("Time")
            ax_pressure.set_ylabel("Pressure")
            ax_pressure.plot(np.linspace(0, 1, len(times)), np.random.random(len(times)), label='Pressure Curve')
            ax_pressure.legend()

        ax_slider = plt.axes([0.1, 0.05, 0.65, 0.03])
        slider = Slider(ax_slider, 'Time Index', 0, len(times) - 1, valinit = 0, valstep=1)
        slider.on_changed(update)

        update(0)
        plt.show()

if __name__ == "__main__":
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)
    plot_diff2center(shot_data, shot_list)
