import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os
import pickleboi
import utils


def plot_cont_diff(shot_data, shot_list):

    shot_numbers = [shot_info[0] for shot_info in shot_list]  # Extract shot numbers
    num_shots = len(shot_numbers)
    rhop = [0.4, 1]

    for i, shot_number in enumerate(shot_numbers):

        #set data
        eqh_shot = shot_data[shot_number]['EQH']
        ide_shot = shot_data[shot_number]['IDE']
        times, eqh_idxs, ide_idxs = utils.create_timestamps(eqh_shot['time'], ide_shot['time'], eqh_shot['tcIps'], eqh_shot['tcIpe'], 1.0)
        time_indexes = eqh_idxs
        #time_indexes = utils.find_time_indexes(shot['time'], shot['tcIps'], shot['tcIpe'], 1.0)
        #time_indexes = [time_indexes[-1]] #temp so that only the last time index is plotted for now
        
        #load EQU shotfile for R, z
        eqh_equ = sf.EQU(shot_number, diag='EQH')
        ide_equ = sf.EQU(shot_number, diag='IDE')

        # Set a big title for the entire figure
        fig = plt.figure(figsize=(12, 10))  # Create a figure
        fig.suptitle(f"Difference profile of Shot {shot_number}", fontsize=16)

        for jtime, time_index in enumerate(eqh_idxs):

            time = eqh_shot['time'][time_index]
            eqh_idx = eqh_idxs[jtime] 
            ide_idx = ide_idxs[jtime]

            eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=time, coord_in='rho_pol')
            print(eqh_r2.shape)
            eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=times, coord_in='rho_pol')
            print(eqh_r2.shape)
            ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=times, coord_in='rho_pol')

            #print(shot_data[shot_number]['EQH']['Rmag'].shape)
            #print(shot_data[shot_number]['IDE']['Rmag'].shape)

            rmag_eqh = eqh_shot['Rmag'][eqh_idx]
            zmag_eqh = eqh_shot['Zmag'][eqh_idx]
            rmag_ide = ide_shot['Rmag'][ide_idx]
            zmag_ide = ide_shot['Zmag'][ide_idx]
            rxpu = eqh_shot['Rxpu'][eqh_idx]
            zxpu = eqh_shot['Zxpu'][eqh_idx]
            raus = eqh_shot['Raus'][eqh_idx]

            #get values existing for R to determine how to split up poloidal agnle theta
            eqh_n_theta = len(eqh_r2[0][-1])
            ide_n_theta = len(ide_r2[0][-1])

            #select to n_theta for the EQU with less values of R
            if ide_n_theta > eqh_n_theta:
                n_theta = eqh_n_theta
            else:
                n_theta = ide_n_theta
            theta = np.linspace(0, 2*np.pi, 2*n_theta)
            
            #map equ points along straights with different theta angle
            eqh_r1, eqh_z1 = sf.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol')
            ide_r1, ide_z1 = sf.rhoTheta2rz(ide_equ, rhop, theta, t_in=time, coord_in='rho_pol')

            for jrho, rho in enumerate(rhop):

                displ_time = round(time, 3)
                diff = np.sqrt((ide_r1[0,:,jrho] - eqh_r1[0,:,jrho])**2 + (ide_z1[0,:,jrho] - eqh_z1[0,:,jrho])**2)

                plt.subplot(2, len(rhop), jrho + 1)
                plt.plot(theta, diff*100, label=f"diff @ {displ_time}")

                plt.title(f"Shot {shot_number} Rho {rho}")
                plt.xlabel("Poloidal Angle theta")
                plt.ylabel('Difference (mm)')

                if time_index == time_indexes[-1]:
                    plt.subplot(2, len(rhop), jrho + 2 + 1)
                    plt.title("rhoTheta2rz contour")
                    #plt.plot(r2[0][jrho], z2[0][jrho],  label='EQH_rho2rz')
                    plt.plot(eqh_r1[0, :, jrho], eqh_z1[0, :, jrho],  label='EQH_rhoTheta2rz')
                    plt.plot(ide_r1[0, :, jrho], ide_z1[0, :, jrho],  label='IDE_rhoTheta2rz')
                    plt.plot(eqh_r2[0][jrho], eqh_z2[0][jrho],  label='EQH_rho2rz')
                    plt.plot(ide_r2[0][jrho], ide_z2[0][jrho],  label='IDE_rho2rz')
                    plt.plot(rmag_eqh, zmag_eqh, 'rx', label='EQH mag center')
                    plt.plot(rmag_ide, zmag_ide, 'bx', label='IDE mag center')
                    plt.plot(rxpu, zxpu, 'rx', label='raus') 
                    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
                    plt.axvline(x=raus, color='black', linestyle='--', linewidth=1)

                """ plt.subplot(3, len(rhop), jrho + 4 + 1)
                plt.title("rho2rz contour")
                #plt.plot(r2[0][jrho], z2[0][jrho],  label='EQH_rho2rz')
                plt.plot(eqh_r2[0][jrho], eqh_z2[0][jrho],  label='EQH_rho2rz')
                plt.plot(ide_r2[0][jrho], ide_z2[0][jrho],  label='EQH_rho2rz') """

        #plt.xticks(range(num_shots), shot_numbers)  # Set shot numbers as x-axis ticks
        # Adjust layout to prevent overlapping
        plt.legend()
        plt.tight_layout()
        plt.show()



if __name__ == "__main__":
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)
    #shot_list = [(40128, 0.90, 4.90)]

    plot_cont_diff(shot_data, shot_list)


