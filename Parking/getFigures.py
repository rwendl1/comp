import os
import aug_sfutils as sf                               
import matplotlib.pyplot as plt    # load plotting library                      
import numpy as np                # load math library                           

shots = [40128, 38472, 38474, 38819, 40869, 37388, 38457, 40419, 39870, 29110, \
40635]
timepoint = 1000

# Relative path to the "figures" subfolder                                      
figures_folder = "figures"

for shotnumber in shots:
    ida = sf.SFREAD(shotnumber, 'ida')
    if ida.status:
        tida = ida.gettimebase('pe')
        rhop = ida.getareabase('pe')
        Pe = ida.getobject('pe')
        print(rhop.shape, Pe.shape)
        plt.plot(rhop[:, timepoint], Pe[:, timepoint])    # create plot of 'timepoint'th time point                                                            
        plt.title('%s @ t=%3.2fs' % (Pe.descr, tida[timepoint]))
        plt.xlabel('rho')
        plt.ylabel(Pe.phys_unit)

        plt.grid(True)
        #plt.show()
        # Save the plot with shot number as filename in the relative "figures" subfolder                                                                       
        plt.savefig(f"{figures_folder}/{shotnumber}.png")
        plt.close()  # Close the plot to avoid displaying multiple plots        


print("All figures saved successfully.")


