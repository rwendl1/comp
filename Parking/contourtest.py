
import numpy as np
import matplotlib.pyplot as plt

# Example data for two egg shapes (replace with actual data)
theta = np.linspace(0, 2 * np.pi, 100)
r1 = 1 + 0.3 * np.sin(theta)  # Shape 1
r2 = 1 + 0.2 * np.sin(theta + np.pi / 4)  # Shape 2

# Centers of the shapes (example coordinates)
C1 = np.array([1, 1])
C2 = np.array([-3, 2])
d = C2 - C1

# Convert Shape 2 to Cartesian coordinates, shift and convert back to polar
x2 = r2 * np.cos(theta) + d[0]
y2 = r2 * np.sin(theta) + d[1]

# Calculate radii from the new center C1
r2_prime = np.sqrt((x2 - C1[0])**2 + (y2 - C1[1])**2)
theta2_prime = np.arctan2(y2 - C1[1], x2 - C1[0])

# Sort theta2_prime and r2_prime to ensure correct interpolation
sorted_indices = np.argsort(theta2_prime)
theta2_prime_sorted = theta2_prime[sorted_indices]
r2_prime_sorted = r2_prime[sorted_indices]

# Interpolate r2_prime to the same theta values as r1
r2_interpolated = np.interp(theta, theta2_prime_sorted, r2_prime_sorted, period=2*np.pi)

# Compute the differences
delta_r = r1 - r2_interpolated

# Convert polar coordinates to Cartesian for plotting the contours
x1 = r1 * np.cos(theta) + C1[0]
y1 = r1 * np.sin(theta) + C1[1]
x2_aligned = r2_interpolated * np.cos(theta) + C1[0]
y2_aligned = r2_interpolated * np.sin(theta) + C1[1]

# Create subplots
fig, axs = plt.subplots(2, 1, figsize=(10, 12))

# Plot the contours of the two shapes
axs[0].plot(x1, y1, label='Shape 1')
axs[0].plot(x2, y2, label='Shape 2 (Original)')
axs[0].plot(x2_aligned, y2_aligned, label='Shape 2 (Aligned)')
axs[0].scatter(*C1, color='red', zorder=5, label='Center 1')
axs[0].scatter(*C2, color='blue', zorder=5, label='Center 2')
axs[0].set_aspect('equal', 'box')
axs[0].legend()
axs[0].set_title('Contours of the Two Shapes')
axs[0].set_xlabel('X')
axs[0].set_ylabel('Y')

# Plot the difference in radius
axs[1].plot(theta, delta_r)
axs[1].set_title('Difference in Radius Over Ploidal Angle')
axs[1].set_xlabel('Theta (radians)')
axs[1].set_ylabel('Difference in Radius')

plt.tight_layout()
plt.show()
