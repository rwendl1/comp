import requests
import os
import csv
import aug_sfutils as sf
import matplotlib.pyplot as plt

# Function to read shot numbers from a text file and iterate over them
def iterate_shot_numbers(file_path):
    with open(file_path, "r") as file:
        shot_numbers = file.read().split(",")
        for shot_number in shot_numbers:
            shot_number = shot_number.strip()  # Remove any leading/trailing whitespace
            readShot(shot_number)


def readShot(shotnumber):
    print(shotnumber)
    ide = sf.SFREAD(int(shotnumber), 'ide')
    
    #eq_ide = sf.EQU(int(shotnumber), diag = 'ide')
    creation_time = ide.time  # Get the creation time of the shotfile

    
    # Determine if CSV file exists
    csv_exists = os.path.exists('shot_data.csv')
    
    # Write shot number and time to CSV file
    with open('shot_data.csv', 'a', newline='') as csvfile:
        fieldnames = ['Shot Number', 'Creation Time']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        # If CSV file doesn't exist, write header
        if not csv_exists:
            writer.writeheader()
        
        print(shotnumber, ", ", creation_time)
        # Write shot number and time as a row in the CSV file
        writer.writerow({'Shot Number': shotnumber, 'Creation Time': creation_time})



# Example usage
if __name__ == "__main__":
    shot_numbers_file = "shots.txt"  # Replace with the path to your shot numbers file
    iterate_shot_numbers(shot_numbers_file)