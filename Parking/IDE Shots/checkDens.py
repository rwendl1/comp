import csv
from datetime import datetime
import aug_sfutils as sf
import matplotlib.pyplot as plt

def filter_by_date(input_csv, output_csv):
    with open(input_csv, 'r') as infile, open(output_csv, 'w', newline='') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)
        
        #Write header to the output CSV file
        header = next(reader)
        writer.writerow(header)
        
        # Iterate over rows in the input CSV file
        for row in reader:
            # Extract shot number and date from the row
            shotnumber = int(row[0])  # Assuming shot number is in the first column
            date_str = row[1]  # Assuming date is in the second column

            #print(shotnumber)
            
            idr_sf = sf.SFREAD(shotnumber, 'idr')
            if not idr_sf.status:
                #print("IDE shotfile doesnt exist for ", shotnumber)        #handled by sfread anyway
                #add function for private shotfile fetching
                return
            idr = sf.SFREAD(shotnumber, 'idr')

            ne = idr.getobject('ne')
            if ne is None:
                next
            t_ne = idr.gettimebase('ne')
            rho_tor = idr.getareabase('ne')

            #print(rho_tor.shape)
            #print(rho_tor.shape, ne.shape)
            #print(ne.shape)

            try:
                timepoint = 500
                rho_rel = 0.5

                threshold = 3*pow(10,13)

                dens = ne[timepoint,int(rho_rel*100)]/threshold
                row[1] = dens 

                if dens < 1:
                    print(shotnumber, ": ", dens)
                    writer.writerow(row)
            except Exception as e:
                print(f"Error checking dens for  {shotnumber}: {e}")

            #writer.writerow(row)

            

            """plt.plot(rho_tor[:,:] , ne[timepoint,:])    # create plot of 'timepoint'th time point                                                            
            plt.title('%s @ t=%3.2fs' % (ne.descr, t_ne[timepoint]))
            plt.xlabel('rho')
            plt.ylabel(ne.phys_unit)

            plt.grid(True)
            plt.show()
             Save the plot with shot number as filename in the relative "figures" subfolder                                                                       
            plt.show()
            plt.close()  # Close the plot to avoid displaying multiple plots  """     
            
            

# Example usage:
input_csv = 'filtered_shot_data.csv'
output_csv = 'density_shot_data.csv'
filter_by_date(input_csv, output_csv)
