import csv
from datetime import datetime

def filter_by_date(input_csv, output_csv):
    with open(input_csv, 'r') as infile, open(output_csv, 'w', newline='') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)
        
        # Write header to the output CSV file
        header = next(reader)
        writer.writerow(header)
        
        # Iterate over rows in the input CSV file
        for row in reader:
            # Extract shot number and date from the row
            shot_number = row[0]  # Assuming shot number is in the first column
            date_str = row[1]  # Assuming date is in the second column
            
            # Remove milliseconds from date string if it exceeds the desired format length
            if len(date_str) > 19:
                date_str = date_str[:19]  # Truncate to keep only the first 19 characters
            
            # Convert date string to datetime object
            date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
            
            # Check if date is after March 2023
            if date > datetime(2023, 5, 12):
                # Write shot number and row to the output CSV file
                writer.writerow(row)

# Example usage:
input_csv = 'shot_data.csv'
output_csv = 'filtered_shot_data.csv'
filter_by_date(input_csv, output_csv)
