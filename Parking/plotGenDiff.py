import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from matplotlib.widgets import Slider, Button
import numpy as np
import pickle
import os
import pickleboi
import utils
import privmapeq

def get_scalars(shot, t_idx, R_cont, Z_cont):

    rmag = shot['Rmag'][t_idx]
    zmag = shot['Zmag'][t_idx]
    rxpu = shot['Rxpu'][t_idx]
    zxpu = shot['Zxpu'][t_idx]
    raus = shot['Raus'][t_idx]

    #calculates angle of the ray x point-mag center to the horizontal axis
    angle_mag2X = utils.angle_to_horizontal((rmag,zmag),(rxpu,zxpu))

    #calculates the 
    zaus_idx = utils.find_idx_of_nearest(R_cont[0, :, -1], raus)
    zaus = Z_cont[0, zaus_idx, -1]
    angle_mag2Raus = utils.angle_to_horizontal((rmag,zmag),(raus,zaus))

    data = {
        'Rmag': rmag,
        'Zmag': zmag,
        'Rxpu': rxpu,
        'Zxpu': zxpu,
        'Raus': raus,
        'Zaus': zaus,
        'angle_mag2x': angle_mag2X,
        'angle_mag2Raus': angle_mag2Raus
    }
    return data

def generate_plot_data(shot_data, shot_number, rhop):
    
    eqh_shot = shot_data[shot_number]['EQH']
    ide_shot = shot_data[shot_number]['IDE']
    times, eqh_idxs, ide_idxs = utils.create_timestamps(eqh_shot['time'], ide_shot['time'], eqh_shot['tcIps'], eqh_shot['tcIpe'], delta_t=1.0)
    
    eqh_equ = sf.EQU(shot_number, diag='EQH')
    ide_equ = sf.EQU(shot_number, diag='IDE')

    eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=times, coord_in='rho_pol')
    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=times, coord_in='rho_pol')

    X_angle_list = []
    Raus_angle_list = []
    shot_data_list = []
    shot_data[shot_number]['poloDiff'] = {}
    shot_data[shot_number]['poloDiff']['times'] = times
    shot_data[shot_number]['poloDiff']['eqh_idxs'] = eqh_idxs,
    shot_data[shot_number]['poloDiff']['ide_idxs'] = ide_idxs,
    shot_data[shot_number]['poloDiff']['rhop'] = rhop,

    for jtime, time_index in enumerate(eqh_idxs):
        time = times[jtime]
        eqh_idx = eqh_idxs[jtime]
        ide_idx = ide_idxs[jtime]

        eqh_n_theta = len(eqh_r2[jtime][-1])
        ide_n_theta = len(ide_r2[jtime][-1])
        n_theta = min(eqh_n_theta, ide_n_theta)
        theta = np.linspace(-np.pi, np.pi, 2*n_theta)

        eqh_r1, eqh_z1 = sf.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol')
        ide_r1, ide_z1 = sf.rhoTheta2rz(ide_equ, rhop, theta, t_in=time, coord_in='rho_pol')

        eqh_scls = get_scalars(eqh_shot, eqh_idx, eqh_r1, eqh_z1)
        ide_scls = get_scalars(ide_shot, ide_idx, ide_r1, ide_z1)

        X_angle_list.append(ide_scls['angle_mag2x'])
        Raus_angle_list.append(ide_scls['angle_mag2Raus'])
        mag_center_offset = utils.mag_center_offset(eqh_scls, ide_scls, theta)


        shot_data_list.append({
            'time': time,
            'theta': theta,
            'eqh_r1': eqh_r1,
            'eqh_z1': eqh_z1,
            'ide_r1': ide_r1,
            'ide_z1': ide_z1,
            'eqh_scls': eqh_scls,
            'ide_scls': ide_scls,
            'mag_center_offset': mag_center_offset
        })

    shot_data[shot_number]['poloDiff']['X_angle_list'] = X_angle_list
    shot_data[shot_number]['poloDiff']['Raus_angle_list'] = Raus_angle_list
    shot_data[shot_number]['poloDiff']['poloList'] = shot_data_list

    return shot_data

def plotDiffFromData(shot_data, shot_number):

    #set data
    eqh_shot = shot_data[shot_number]['EQH']
    ide_shot = shot_data[shot_number]['IDE']
    polodata = shot_data[shot_number]['poloDiff']

    rhop = polodata["rhop"]
    times = polodata["times"]
    eqh_idxs = polodata["eqh_idxs"]
    ide_idxs = polodata["ide_idxs"]

    # Set a big title for the entire figure
    fig = plt.figure(figsize=(12, 10))  # Create a figure
    fig.suptitle(f"Difference profile of Shot {shot_number}", fontsize=16)

    X_angle_list = polodata["X_angle_list"]
    Raus_angle_list = polodata["Raus_angle_list"]
    polodataList = polodata['poloList']

    print(eqh_idxs)

    for jtime, eqh_time in enumerate(eqh_idxs):

        time = times[jtime]
        eqh_idx = eqh_idxs[jtime] 
        ide_idx = ide_idxs[jtime]

        poloList = polodataList[jtime]
        theta = poloList['theta']
        mag_center_offset = poloList['mag_center_offset']

        print(rhop)
        
        for jrho, rho in enumerate(rhop):
            eqh_r1 = poloList['eqh_r1'][0, :, jrho]
            eqh_z1 = poloList['eqh_z1'][0, :, jrho]
            ide_r1 = poloList['ide_r1'][0, :, jrho]
            ide_z1 = poloList['ide_z1'][0, :, jrho]
            eqh_scls = poloList['eqh_scls']
            ide_scls = poloList['ide_scls']

            print(jrho)
            
            #calc arrays for vectors from magnetic center for EQH
            R_eqh = eqh_r1 - np.ones_like(eqh_r1) * eqh_scls['Rmag']
            z_eqh = eqh_z1 - np.ones_like(eqh_z1) * eqh_scls['Zmag']
            eqh_len = np.sqrt(R_eqh**2 + z_eqh**2)

            #calc arrays for vectors from magnetic center for IDE
            R_ide = ide_r1 - np.ones_like(ide_r1) * ide_scls['Rmag']
            z_ide = ide_z1 - np.ones_like(ide_z1) * ide_scls['Zmag']
            ide_len = np.sqrt(R_ide**2 + z_ide**2)

            #calculate difference between vector lengths
            diff = ide_len - (eqh_len + mag_center_offset)

            #plot diff and mag center offset
            plt.subplot(2, len(rhop), jrho + 1)
            plt.plot(theta, (diff) * 1000, label=f'diff @{time:.2f}s')
            plt.plot(theta, mag_center_offset * 1000, lw = 0.5, linestyle = '-.', c='green')

            if rho == 1:
                #define scalars
                scalar_value_eqh = eqh_shot['Raus'][eqh_idx]
                scalar_value_ide = ide_shot['Raus'][ide_idx]
                
                #calc diff for plot
                diff_raus = scalar_value_ide - scalar_value_eqh

                #plot difference
                plt.plot(Raus_angle_list[-1] , diff_raus*1000, 'rx')

            plt.legend()
            #plt.title(f"Shot {shot_number} @ Rhop={rho:.2f}")
            plt.xlabel("Poloidal Angle theta (rad)")
            plt.ylabel('Difference (mm)')

            #plot the box around where the mag to x-point axis over angle plot
            if jtime == len(eqh_idxs)-1:
                plt.subplot(2, len(rhop), jrho + 1)

                plt.axhline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots horizontal 0 axis
                plt.axvline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots vertical 0 axis
                plt.axvspan(min(X_angle_list), max(X_angle_list), ymin =-10, ymax = 10, facecolor = 'blueviolet', alpha = .3)
                plt.axvspan(min(Raus_angle_list), max(Raus_angle_list), ymin =-10, ymax = 10, facecolor = 'orange', alpha = .3)

            #plots the contour (lower plots)
            if jtime == 0:
                plt.subplot(2, len(rhop), jrho + len(rhop) + 1)         #define subplot
                plt.title(f"rhoTheta2rz contour @ {time:.2f}s" )

                #plot contours
                plt.plot(eqh_r1, eqh_z1,  label='EQH_rhoTheta2rz', c='r')
                plt.plot(ide_r1, ide_z1,  label='IDE_rhoTheta2rz', c='b')
                #plt.plot(eqh_r2[0][jrho], eqh_z2[0][jrho],  label='EQH_rho2rz', c='r')
                #plt.plot(ide_r2[0][jrho], ide_z2[0][jrho],  label='IDE_rho2rz', c='b')

                #plot magnetic centers
                plt.plot(eqh_scls['Rmag'], eqh_scls['Zmag'], 'rx', label='EQH mag center')
                plt.plot(ide_scls['Rmag'], ide_scls['Zmag'], 'bx', label='IDE mag center')

                #plot X-point info on contour
                plt.plot( ide_scls['Rxpu'], ide_scls['Zxpu'], 'x', label='X-Point', color = 'blueviolet') 
                plt.plot((ide_scls['Rmag'], ide_scls['Rxpu']),(ide_scls['Zmag'],ide_scls['Zxpu']), linestyle='--', linewidth=1, color = 'blueviolet')

                #plot the Raus info on contour
                plt.plot(ide_scls['Raus'], ide_scls['Zaus'], 'x', label='Raus', color = 'orange')
                plt.plot((ide_scls['Rmag'], ide_scls['Raus']),(ide_scls['Zmag'],ide_scls['Zaus']), linestyle='--', linewidth=1, color = 'orange')

                #plot the center axes
                plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
                plt.axhline(y=eqh_scls['Zmag'], color='red', linestyle='--', linewidth=1)
                plt.axhline(y=ide_scls['Zmag'], color='blue', linestyle='--', linewidth=1)
                if rho == 1.0:
                    plt.axvline(x=eqh_scls['Raus'] , color='red', linestyle='--', linewidth=1)
                    plt.axvline(x=ide_scls['Raus'] , color='blue', linestyle='--', linewidth=1)
                plt.axis('scaled')
                plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    # Adjust layout to prevent overlapping
    plt.legend()
    plt.tight_layout()
    plt.show()



def plot_diff2center(shot_data, shot_number, rhop):

    #set data
    eqh_shot = shot_data[shot_number]['EQH']
    ide_shot = shot_data[shot_number]['IDE']
    times, eqh_idxs, ide_idxs = utils.create_timestamps(eqh_shot['time'], ide_shot['time'], eqh_shot['tcIps'], eqh_shot['tcIpe'], 1.0)
    
    #load EQU shotfile for R, z
    eqh_equ = sf.EQU(shot_number, diag='EQH')
    ide_equ = sf.EQU(shot_number, diag='IDE')

    # Set a big title for the entire figure
    fig = plt.figure(figsize=(12, 10))  # Create a figure
    fig.suptitle(f"Difference profile of Shot {shot_number}", fontsize=16)
    
    eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=times, coord_in='rho_pol')
    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=times, coord_in='rho_pol')

    X_angle_list = [] 
    Raus_angle_list = [] 

    for jtime, eqh_time in enumerate(eqh_idxs):

        time = times[jtime]
        eqh_idx = eqh_idxs[jtime] 
        ide_idx = ide_idxs[jtime]

        #get values existing for R to determine how to split up poloidal agnle theta
        eqh_n_theta = len(eqh_r2[jtime][-1])
        ide_n_theta = len(ide_r2[jtime][-1])

        #select to n_theta for the EQU with less values of R
        if ide_n_theta > eqh_n_theta:
            n_theta = eqh_n_theta
        else:
            n_theta = ide_n_theta
        theta = np.linspace(-np.pi, np.pi, 2*n_theta)
        
        #map equ points along straights with different theta angle
        eqh_r1, eqh_z1 = sf.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol')
        ide_r1, ide_z1 = sf.rhoTheta2rz(ide_equ, rhop, theta, t_in=time, coord_in='rho_pol')

        #get scalar values like Rmag, Zmag, Raus, etc... and also angle to X-Point and angle to Raus
        eqh_scls = get_scalars(eqh_shot, eqh_idx, eqh_r1, eqh_z1)
        ide_scls = get_scalars(ide_shot, ide_idx, ide_r1, ide_z1)

        X_angle_list.append(ide_scls['angle_mag2x'])
        Raus_angle_list.append(ide_scls['angle_mag2Raus'])
        mag_center_offset = utils.mag_center_offset(eqh_scls, ide_scls, theta)   
        
        for jrho, rho in enumerate(rhop):
            
            #calc arrays for vectors from magnetic center for EQH
            R_eqh = eqh_r1[0,:,jrho] - np.ones_like(eqh_r1[0,:,jrho])*eqh_scls['Rmag']
            z_eqh = eqh_z1[0,:,jrho] - np.ones_like(eqh_z1[0,:,jrho])*eqh_scls['Zmag']
            eqh_len = np.sqrt(R_eqh**2 +z_eqh**2)

            #calc arrays for vectors from magnetic center for IDE
            R_ide = ide_r1[0,:,jrho] - np.ones_like(ide_r1[0,:,jrho])*ide_scls['Rmag']
            z_ide = ide_z1[0,:,jrho] - np.ones_like(ide_z1[0,:,jrho])*ide_scls['Zmag']
            ide_len = np.sqrt(R_ide**2 +z_ide**2)

            #calculate difference between vector lengths
            diff = ide_len - (eqh_len + mag_center_offset)

            #plot diff and mag center offset
            plt.subplot(2, len(rhop), jrho + 1)
            plt.plot(theta, (diff) * 1000, label=f'diff @{time:.2f}s')
            plt.plot(theta, mag_center_offset * 1000, lw = 0.5, linestyle = '-.', c='green')

            if rho == 1:
                #define scalars
                scalar_value_eqh = eqh_shot['Raus'][eqh_idx]
                scalar_value_ide = ide_shot['Raus'][ide_idx]
                
                #calc diff for plot
                diff_raus = scalar_value_ide - scalar_value_eqh

                #plot difference
                plt.plot(Raus_angle_list[-1] , diff_raus*1000, 'rx')

            plt.legend()
            plt.title(f"Shot {shot_number} @ Rhop={rho:.2f}")
            plt.xlabel("Poloidal Angle theta (rad)")
            plt.ylabel('Difference (mm)')

            #plot the box around where the mag to x-point axis over angle plot
            if jtime == len(eqh_idxs)-1:
                plt.subplot(2, len(rhop), jrho + 1)

                plt.axhline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots horizontal 0 axis
                plt.axvline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots vertical 0 axis
                plt.axvspan(min(X_angle_list), max(X_angle_list), ymin =-10, ymax = 10, facecolor = 'blueviolet', alpha = .3)
                plt.axvspan(min(Raus_angle_list), max(Raus_angle_list), ymin =-10, ymax = 10, facecolor = 'orange', alpha = .3)

            #plots the contour (lower plots)
            if jtime == 0:
                plt.subplot(2, len(rhop), jrho + len(rhop) + 1)         #define subplot
                plt.title(f"rhoTheta2rz contour @ {time:.2f}s" )

                #plot contours
                plt.plot(eqh_r1[0, :, jrho], eqh_z1[0, :, jrho],  label='EQH_rhoTheta2rz', c='r')
                plt.plot(ide_r1[0, :, jrho], ide_z1[0, :, jrho],  label='IDE_rhoTheta2rz', c='b')
                plt.plot(eqh_r2[0][jrho], eqh_z2[0][jrho],  label='EQH_rho2rz', c='r')
                plt.plot(ide_r2[0][jrho], ide_z2[0][jrho],  label='IDE_rho2rz', c='b')

                #plot magnetic centers
                plt.plot(eqh_scls['Rmag'], eqh_scls['Zmag'], 'rx', label='EQH mag center')
                plt.plot(ide_scls['Rmag'], ide_scls['Zmag'], 'bx', label='IDE mag center')

                #plot X-point info on contour
                plt.plot( ide_scls['Rxpu'], ide_scls['Zxpu'], 'x', label='X-Point', color = 'blueviolet') 
                plt.plot((ide_scls['Rmag'], ide_scls['Rxpu']),(ide_scls['Zmag'],ide_scls['Zxpu']), linestyle='--', linewidth=1, color = 'blueviolet')

                #plot the Raus info on contour
                plt.plot(ide_scls['Raus'], ide_scls['Zaus'], 'x', label='Raus', color = 'orange')
                plt.plot((ide_scls['Rmag'], ide_scls['Raus']),(ide_scls['Zmag'],ide_scls['Zaus']), linestyle='--', linewidth=1, color = 'orange')

                #plot the center axes
                plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
                plt.axhline(y=eqh_scls['Zmag'], color='red', linestyle='--', linewidth=1)
                plt.axhline(y=ide_scls['Zmag'], color='blue', linestyle='--', linewidth=1)
                if rho == 1.0:
                    plt.axvline(x=eqh_scls['Raus'] , color='red', linestyle='--', linewidth=1)
                    plt.axvline(x=ide_scls['Raus'] , color='blue', linestyle='--', linewidth=1)
                plt.axis('scaled')
                plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    # Adjust layout to prevent overlapping
    plt.legend()
    plt.tight_layout()
    plt.show()


def main():
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    shot_data = utils.load_shot_data(pickle_file)
    shot_numbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)

    rhop = [0.4, 1] 

    #check if dict already contains poloDiff data for that shot
    for x, shot_number in enumerate(shot_numbers):
        gen_Data = True
        if "poloDiff" in shot_data[shot_number]:
            print(shot_data[shot_number].keys())
            if shot_data[shot_number]["poloDiff"]['rhop'] == rhop:
                if shot_data[shot_number]["poloDiff"]['tcIps'] == tcIps_list[x] and shot_data[shot_number]['tcIpe'] == tcIpe_list[x]:
                    gen_Data = False
            
        if gen_Data:
            shot_data = generate_plot_data(shot_data, shot_number, rhop)
            utils.save_shot_data(pickle_file, shot_data)

        plotDiffFromData(shot_data, shot_number)

    return



if __name__ == "__main__":
    main()