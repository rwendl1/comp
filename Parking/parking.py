def populate_data_from_list_old():
    # populates the shot data from predefined list
    shot_data = {}
    
    # Populate shot data with choosen shots
    shots = [40128, 38472, 38474, 38819, 40869, 37388, 38457, 40419, 39870, 29110, 40635]
    for shotnumber in shots:  
        try:
            # Attempt to store data for the current shot number
            data = fetch_diag_data(shotnumber,'ide')
            shot_data[shotnumber] = data
        except Exception as e:
            print(f"Error populating data for shot {shotnumber}: {e}")
    
    return 
    
def pickle_shot_old(shotnumber, **kwargs):
   
    # Check if IDE shotfile exists
    ide_sf = sf.SFREAD(shotnumber, 'ide')       #to check if the shot has an actual IDE shoftile
    ide = sf.EQU(shotnumber, diag = 'ide')
    Geq = sf.SFREAD(shotnumber, 'Geq')

    # Get objects for specified signals from Geq
    signal_objects = get_signal_objects(Geq, 'Zgeri', 'Zgera', 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    
    # Create the shot data dictionary
    return {
        'pres': ide.pres,
        'psiN': ide.psiN,
        **signal_objects,
        'tIpcs': t_const_Ip_start if 'tIpcs' in kwargs else 0.0,
        'tIpce': t_const_Ip_end if 'tIpce' in kwargs else 8.0,
    }

def distance_between_orthogonals(eqh_sclas, ide_sclas, angles):

    point1 = [eqh_sclas['Rmag'], eqh_sclas['Zmag']]
    point2 = [ide_sclas['Rmag'], ide_sclas['Zmag']] 
    # Convert points to numpy arrays
    point1 = np.array(point1)
    point2 = np.array(point2)
    
    # Calculate slopes of the lines corresponding to given angles
    slopes = np.tan(angles)
    
    # Calculate slopes of the lines perpendicular to the given lines
    perpendicular_slopes = -1 / slopes
    
    distances = []
    for perp_slope in perpendicular_slopes:
        # Calculate intersection point
        x_intersect = (perp_slope * point1[0] - slopes * point2[0] + point2[1] - point1[1]) / (perp_slope - slopes)
        y_intersect = perp_slope * (x_intersect - point1[0]) + point1[1]
        intersection_point = np.array([x_intersect, y_intersect])
        
        # Calculate distance between intersection point and each given point
        distance1 = np.linalg.norm(point1 - intersection_point)
        distance2 = np.linalg.norm(point2 - intersection_point)
        
        # Take the minimum distance
        min_distance = min(distance1, distance2)
        distances.append(min_distance)
    
    return distances

def calculate_distances(eqh_sclas, ide_sclas, angles):

    point1 = [eqh_sclas['Rmag'], eqh_sclas['Zmag']]
    point2 = [ide_sclas['Rmag'], ide_sclas['Zmag']]
    
    # Convert points to numpy arrays
    point1 = np.array(point1)
    point2 = np.array(point2)
    
    # Calculate direction vector from point 1 to point 2
    centers_vector = point2 - point1
    centers_vec_len = np.sqrt(centers_vector[0]**2 + centers_vector[1]**2)

    # Calculate distances
    distances = []
    for angle in angles:

        # Calculate unit vector in direction of current angle
        dir_point = np.array([point1[0] + np.cos(angle), point1[1]+ np.sin(angle)])

        p1 = point1
        p2 = dir_point
        p3 = point2

        # Calculate orthogonal distance
        ortho_distance = np.cross(p2-p1,p3-p1)/np.linalg.norm(p2-p1)
        #calculate distance between vectors in direction of angle
        distance = 100* np.sqrt(centers_vec_len ** 2 - ortho_distance ** 2)
        #print(distance)
        
        distances.append(distance)
    
    return distances

def distance_between_orthogonals(eqh_sclas, ide_sclas, angles):

    point1 = [eqh_sclas['Rmag'], eqh_sclas['Zmag']]
    point2 = [ide_sclas['Rmag'], ide_sclas['Zmag']]

    # Convert points to numpy arrays
    point1 = np.array(point1)
    point2 = np.array(point2)

    # Convert angles to unit vectors representing direction
    directions = np.array([[np.cos(angle), np.sin(angle)] for angle in angles])
    
    # Calculate the vector between the two points
    vector_between_points = point2 - point1
    
    # Reshape the vector to be a column vector
    vector_between_points = vector_between_points.reshape(-1, 1)
    
    # Normalize the vector
    normalized_vector = vector_between_points / np.linalg.norm(vector_between_points)
    
    # Calculate the perpendicular vector
    perpendicular_vector = np.array([-normalized_vector[1], normalized_vector[0]])
    
    # Calculate the projection of each direction vector onto the perpendicular vector
    projections = np.dot(directions, perpendicular_vector)
    
    # Calculate the distance between the orthogonal projections
    distances = np.abs(np.dot(vector_between_points.T, projections))
    
    return distances[0]