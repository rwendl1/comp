import pickle
import os
import numpy as np
import aug_sfutils as sf  # Assuming you have your custom module aug_sfutils

class ShotDataset:
    def __init__(self, shotlist_file, diagnostics, experiments, pickle_file):
        self.shotlist_file = shotlist_file
        self.diagnostics = diagnostics
        self.experiments = experiments
        self.pickle_file = pickle_file
        self.data = {}

        # Populate data upon initialization
        self.populate_data_from_list()

    def load_shot_data(self):
        """Load shot data from a file if it exists, otherwise return an empty dictionary"""
        if os.path.exists(self.pickle_file):
            with open(self.pickle_file, 'rb') as file:
                self.data = pickle.load(file)
        else:
            self.data = {}

    def save_shot_data(self):
        """Save shot data to a file"""
        with open(self.pickle_file, 'wb') as file:
            pickle.dump(self.data, file)
            print("---shotdata pickled---")

    def get_signal_objects(self, Geq, *signal_names):
        """Get objects for specified signals from Geq"""
        signal_objects = {}
        for signal_name in signal_names:
            try:
                signal_objects[signal_name] = Geq.getobject(signal_name)
            except Exception as e:
                print(f"Error getting object for signal '{signal_name}': {e}")
                signal_objects[signal_name] = 0
        return signal_objects

    def get_DCN_objects(self, *signal_names):
        """Get objects for specified signals from Geq"""
        signal_objects = {}
        DCN = sf.SFREAD(shotnumber, 'DCN')
        DCK = sf.SFREAD(shotnumber, 'DCK')
        if DCK.status:
            signal_objects['DCN'] = DCN.getobject('H-1')
            signal_objects['DCK'] = DCK.getobject('H-1')
            signal_objects['DCK_status'] = True
        else:
            signal_objects['DCN'] = DCN.getobject('H-1')
            signal_objects['DCK_status'] = False
        return signal_objects

    def try_exp_list(self, shotnumber):
        """Try different experiments to get the data"""
        for exp in self.experiments:
            eval = sf.SFREAD(shotnumber, 'IDE', experiment=exp)
            if eval.status:
                return exp
        return None

    def fetch_diag_data(self, shotnumber, diagnostic, **kwargs):
        """Pickles shot data for the given shot number and diagnostic"""
        print("---Fetching data for ", shotnumber, ", diag: ", diagnostic, " ---")
        shot_data = {}
        try:
            if diagnostic.upper() == 'IDE':
                sfr = sf.SFREAD(shotnumber, 'IDE')
                if sfr.status:
                    exp = self.try_exp_list(shotnumber)
                    if exp:
                        equ = sf.EQU(shotnumber, diag='IDE', experiment=exp)
                        Geq = sf.SFREAD(shotnumber, 'IDG', experiment=exp)
                    else:
                        return shot_data
                else:
                    return shot_data
            elif diagnostic.upper() == 'EQH':
                sfr = sf.SFREAD(shotnumber, 'EQH')
                if sfr.status:
                    equ = sf.EQU(shotnumber, diag='EQH')
                    Geq = sf.SFREAD(shotnumber, 'GQH')
                else:
                    return shot_data
            else:
                raise ValueError(f"Diagnostic '{diagnostic}' not known to data structure")
        except Exception as e:
            print(f"Error: {e}")
            return shot_data

        #read scalar values for Geq (IDG and GQH)
        signal_objects = self.get_signal_objects(Geq, 'Zgeri', 'Zgera', 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
        #read power singals
        TOT_objects = self.get_signal_objects(sf.SFREAD(shotnumber, 'TOT'), 'PNBI_TOT', 'PICR_TOT','PECR_TOT', 'P_TOT')
        #read H/L mode qualifier, L-mode when <0,6
        TTH_objects = self.get_signal_objects(sf.SFREAD(shotnumber, 'TTH'), 'H/L-facs')
        #density readout correcte available when DCK_status = True otherwise DCN uncorrected for fringe jumps
        DCN_objects = self.get_DCN_objects(shotnumber)

        rhop_tor = sf.mapeq.rho2rho(equ, equ.rho_tor_n, coord_in="rho_tor", coord_out="rho_pol")
        rhop_psi = np.sqrt(equ.psiN)
        shot_data = {
            'pres': equ.pres,
            'dpres': equ.dpres,
            'psiN': equ.psiN,
            'rhot': equ.rho_tor_n,
            'rhop_tor': rhop_tor,
            'rhop_psi': rhop_psi,
            'time': equ.time,
            **signal_objects,
            **TOT_objects,
            **TTH_objects,
            **DCN_objects,
            'tcIps': kwargs.get('tcIps', equ.time[0]),
            'tcIpe': kwargs.get('tcIpe', equ.time[-1]),
        }
        return shot_data

    def populate_data_from_list(self):
        """Populates the shot data from a predefined list file"""
        try:
            with open(self.shotlist_file, 'r') as file:
                shot_list = eval(file.read())  # Assume shot list file contains a valid Python list
        except FileNotFoundError:
            print(f"Shot list file '{self.shotlist_file}' not found.")
            return

        for shot_info in shot_list:
            shotnumber, tcIps, tcIpe = shot_info
            try:
                self.data[shotnumber] = {}
                for dia in self.diagnostics:
                    data = self.fetch_diag_data(shotnumber, dia, tcIps=tcIps, tcIpe=tcIps)
                    self.data[shotnumber][dia] = data
                    print(f"---Saved {shotnumber}: {dia} to dict---")
            except Exception as e:
                print(f"Error populating data for shot {shotnumber}: {e}")

        self.save_shot_data()

if __name__ == "__main__":
    # Define file paths and parameters
    shotlist_file = 'shotlist.txt'
    pickle_file = 'shot_data_class.pickle'
    diagnostics = ['IDE', 'EQH']
    experiments = ['lrado', 'micdu']

    # Create and populate the dataset
    #dataset = ShotDataset(shotlist_file, diagnostics, experiments, pickle_file)
    dataset.load_shot_data()

    # Access the data for a specific shot
    shotnumber = 40128
    if shotnumber in dataset.data:
        temp_data = dataset.data[shotnumber]['EQH']
        #print(temp_data)
    else:
        print(f"No data available for shot number {shotnumber}")
