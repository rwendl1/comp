import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button
import numpy as np
import os
import pickleboi
import utils
import privmapeq

def get_scalars(shot, t_idx, R_cont, Z_cont):

    rmag = shot['Rmag'][t_idx]
    zmag = shot['Zmag'][t_idx]
    rxpu = shot['Rxpu'][t_idx]
    zxpu = shot['Zxpu'][t_idx]
    raus = shot['Raus'][t_idx]

    #calculates angle of the ray x point-mag center to the horizontal axis
    angle_mag2X = utils.angle_to_horizontal((rmag,zmag),(rxpu,zxpu))

    #calculates the 
    zaus_idx = utils.find_idx_of_nearest(R_cont[0, :, -1], raus)
    zaus = Z_cont[0, zaus_idx, -1]
    angle_mag2Raus = utils.angle_to_horizontal((rmag,zmag),(raus,zaus))

    data = {
        'Rmag': rmag,
        'Zmag': zmag,
        'Rxpu': rxpu,
        'Zxpu': zxpu,
        'Raus': raus,
        'Zaus': zaus,
        'angle_mag2x': angle_mag2X,
        'angle_mag2Raus': angle_mag2Raus
    }
    return data




def plot_diff2center(shot_data, shot_list, mag = False):

    shot_numbers = [shot_info[0] for shot_info in shot_list]  # Extract shot numbers
    num_shots = len(shot_numbers)
    rhop = [0.4, 1]

    pp = 1

    for i, shot_number in enumerate(shot_numbers):

        #set data
        eqh_shot = shot_data[shot_number]['EQH']
        ide_shot = shot_data[shot_number]['IDE']
        times, eqh_idxs, ide_idxs = utils.create_timestamps(eqh_shot['time'], ide_shot['time'], eqh_shot['tcIps'], eqh_shot['tcIpe'], 1.0)
        time_indexes = eqh_idxs
        
        #load EQU shotfile for R, z
        eqh_equ = sf.EQU(shot_number, diag='EQH')
        ide_equ = sf.EQU(shot_number, diag='IDE')

        # Set a big title for the entire figure
        fig = plt.figure(figsize=(16, 10))  # Create a figure
        fig.suptitle(f"Difference profile of Shot {shot_number}", fontsize=16)

        gs0 = gridspec.GridSpec(2, 1)
        
        eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=times, coord_in='rho_pol')
        ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=times, coord_in='rho_pol')

        X_angle_list = [] 
        Raus_angle_list = [] 

        for jtime, time_index in enumerate(eqh_idxs):

            time = times[jtime]
            eqh_idx = eqh_idxs[jtime] 
            ide_idx = ide_idxs[jtime]

            #get values existing for R to determine how to split up poloidal agnle theta
            eqh_n_theta = len(eqh_r2[jtime][-1])
            ide_n_theta = len(ide_r2[jtime][-1])

            #select to n_theta for the EQU with less values of R
            if ide_n_theta > eqh_n_theta:
                n_theta = eqh_n_theta
            else:
                n_theta = ide_n_theta
            theta = np.linspace(-np.pi, np.pi, 2*n_theta)
            
            if mag:
                reRmag = eqh_shot['Rmag'][eqh_idx] - ide_shot['Rmag'][ide_idx]
                reZmag = eqh_shot['Zmag'][eqh_idx] - ide_shot['Zmag'][ide_idx]
                
                #map equ points along straights with different theta angle
                eqh_r1, eqh_z1 = privmapeq.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol', reRmag = reRmag, reZmag = reZmag)
            else:
                #map equ points along straights with different theta angle
                eqh_r1, eqh_z1 = sf.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol')
            ide_r1, ide_z1 = sf.rhoTheta2rz(ide_equ, rhop, theta, t_in=time, coord_in='rho_pol')

            #get scalar values for specific timepoints like Rmag, Zmag, Raus, etc... and also angle to X-Point and angle to Raus
            eqh_scls = get_scalars(eqh_shot, eqh_idx, eqh_r1, eqh_z1)
            ide_scls = get_scalars(ide_shot, ide_idx, ide_r1, ide_z1)

            X_angle_list.append(ide_scls['angle_mag2x'])
            Raus_angle_list.append(ide_scls['angle_mag2Raus'])
            mag_center_offset = utils.mag_center_offset(eqh_scls, ide_scls, theta)   
            
            for jrho, rho in enumerate(rhop):

                #get arrays for vectors from magnetic center for IDE
                R_ide = ide_r1[0,:,jrho] - np.ones_like(ide_r1[0,:,jrho])*ide_scls['Rmag']
                z_ide = ide_z1[0,:,jrho] - np.ones_like(ide_z1[0,:,jrho])*ide_scls['Zmag']
                ide_len = np.sqrt(R_ide**2 +z_ide**2)
                
                if mag:
                    #get arrays for vectors from remapped magnetic center for EQH (so basically IDE mag center)
                    R_eqh = eqh_r1[0,:,jrho] - np.ones_like(eqh_r1[0,:,jrho])*ide_scls['Rmag']
                    z_eqh = eqh_z1[0,:,jrho] - np.ones_like(eqh_z1[0,:,jrho])*ide_scls['Zmag']
                    eqh_len = np.sqrt(R_eqh**2 +z_eqh**2)

                    diff = ide_len - eqh_len
                else:

                    #get arrays for vectors from magnetic center for EQH
                    R_eqh = eqh_r1[0,:,jrho] - np.ones_like(eqh_r1[0,:,jrho])*eqh_scls['Rmag']
                    z_eqh = eqh_z1[0,:,jrho] - np.ones_like(eqh_z1[0,:,jrho])*eqh_scls['Zmag']
                    eqh_len = np.sqrt(R_eqh**2 +z_eqh**2)
                    
                    #calculate difference between vector lengths
                    diff = ide_len - (eqh_len + mag_center_offset)

                
                #plot diff and mag center offset
                plt.subplot(2, len(rhop)+pp, jrho + 1)
                #gs00 = gridspec.GridSpecFromSubplotSpec(3, len(rhop), subplot_spec=gs0[0])
                #ax = plt.Subplot(fig, gs00[:, jrho])
                plt.plot(theta, (diff) * 1000, label=f'diff @{time:.2f}s')
                plt.plot(theta, mag_center_offset * 1000, lw = 1, linestyle = '-.', c='green')

                if rho == 1:
                    #define scalars
                    eqh_Raus = eqh_scls['Raus']
                    eqh_Zaus = eqh_scls['Zaus']
                    ide_Raus = ide_scls['Raus']
                    ide_Zaus = ide_scls['Zaus']
                    
                    #calc diff for plot
                    diff_raus = ide_Raus - eqh_Raus

                    #plot difference
                    plt.plot(Raus_angle_list[-1] , diff_raus*1000, 'rx')
                    plt.plot(0 , diff_raus*1000, 'bx')

                    #define scalars
                    eqh_Raus = eqh_scls['Rxpu']
                    eqh_Zaus = eqh_scls['Zxpu']
                    ide_Raus = ide_scls['Rxpu']
                    ide_Zaus = ide_scls['Zxpu']
                    
                    #calc diff for plot
                    diff_xpu = np.sqrt((ide_Raus-eqh_Raus)**2 +(ide_Zaus-eqh_Zaus)**2)


                    #plot difference
                    #plt.plot(X_angle_list[-1] , diff_xpu*1000, 'kx')

                    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

                plt.title(f"Shot {shot_number} @ Rhop={rho:.2f}")
                plt.xlabel("Poloidal Angle theta (rad)")
                plt.ylabel('Difference (mm)')

                #plot the box around where the mag to x-point axis over angle plot
                if jtime == len(eqh_idxs)-1:
                    plt.subplot(2, len(rhop)+pp, jrho + 1)

                    #plt.axhline(y=0, color='black', linestyle='--', )
                    plt.axhline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots horizontal 0 axis
                    plt.axvline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots vertical 0 axis
                    #plt.plot(theta, mag_center_offset, lw = 1, linestyle = '-.', c='green')

                    plt.subplot(2, len(rhop)+pp, jrho + 1)
                    plt.axvspan(min(X_angle_list), max(X_angle_list), ymin =-10, ymax = 10, facecolor = 'blueviolet', alpha = .3)
                    plt.axvspan(min(Raus_angle_list), max(Raus_angle_list), ymin =-10, ymax = 10, facecolor = 'orange', alpha = .3)
                

                #plots the contour (lower plots)
                if jtime == 1:
                    plt.subplot(2, len(rhop)+pp, jrho + len(rhop) +pp+ 1)         #define subplot
                    plt.title(f"rhoTheta2rz contour @ {time:.2f}s" )

                    #plot contours
                    plt.plot(eqh_r1[0, :, jrho], eqh_z1[0, :, jrho],  label='EQH_rhoTheta2rz', c='r')
                    plt.plot(ide_r1[0, :, jrho], ide_z1[0, :, jrho],  label='IDE_rhoTheta2rz', c='b')
                    plt.plot(eqh_r2[0][jrho], eqh_z2[0][jrho], ls= 'dotted', label='EQH_rho2rz', c='r')
                    plt.plot(ide_r2[0][jrho], ide_z2[0][jrho], ls = 'dotted', label='IDE_rho2rz', c='b')

                    #plot magnetic centers
                    plt.plot(eqh_scls['Rmag'], eqh_scls['Zmag'], 'rx', label='EQH mag center')
                    plt.plot(ide_scls['Rmag'], ide_scls['Zmag'], 'bx', label='IDE mag center')

                    #plot the center axes
                    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
                    plt.axhline(y=eqh_scls['Zmag'], color='red', linestyle='--', linewidth=1)
                    plt.axhline(y=ide_scls['Zmag'], color='blue', linestyle='--', linewidth=1)
                    if rho == 1.0:

                        #plot X-point info on contour
                        plt.plot( ide_scls['Rxpu'], ide_scls['Zxpu'], 'x', label='X-Point', color = 'blueviolet') 
                        plt.plot((ide_scls['Rmag'], ide_scls['Rxpu']),(ide_scls['Zmag'],ide_scls['Zxpu']), linestyle='--', linewidth=1, color = 'blueviolet')

                        #plot the Raus info on contour
                        plt.plot(ide_scls['Raus'], ide_scls['Zaus'], 'x', label='Raus', color = 'orange')
                        plt.plot((ide_scls['Rmag'], ide_scls['Raus']),(ide_scls['Zmag'],ide_scls['Zaus']), linestyle='--', linewidth=1, color = 'orange')

                        plt.axvline(x=eqh_scls['Raus'] , color='red', linestyle='--', linewidth=1)
                        plt.axvline(x=ide_scls['Raus'] , color='blue', linestyle='--', linewidth=1)
                        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                    plt.axis('scaled')
                    plt.xlim((1.0,2.4))
                    plt.ylim((-1.1,1.1))

                if rho == 1 and pp == 1:
                    plt.subplot(2, len(rhop)+pp, len(rhop) + pp)
                    plt.plot(eqh_shot['rhop_psi'][eqh_idx,:], eqh_shot['pres'][eqh_idx,:], linestyle = '--', label =f"EQH @ {time:.2f}s")
                    plt.plot(ide_shot['rhop_psi'][ide_idx,:], ide_shot['pres'][ide_idx,:], label =f"IDE @ {time:.2f}s")
                    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

                    

        #plt.xticks(range(num_shots), shot_numbers)  # Set shot numbers as x-axis ticks
        # Adjust layout to prevent overlapping
        #plt.legend()
        plt.tight_layout()
        if mag:
            poloDiffs = 'poloDiffs'
        else:
            poloDiffs = 'poloMaps'
        plt.savefig(f'figs/{poloDiffs}/plot_{shot_number}')
        plt.show()



if __name__ == "__main__":
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)
    #shot_list = [(40128, 0.90, 4.90)]

    plot_diff2center(shot_data, shot_list, mag=False)


