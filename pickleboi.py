import pickle
import os
from datetime import datetime 
import aug_sfutils as sf
import numpy as np
import fetch_data
import utils

def populate_data_from_list(shot_list, diagnostics, exps, pickle_file):

    shot_data = {}

    # Get current timestamp
    timestamp = datetime.now()
    with open("evals/lIDE_not_found.txt", "a") as file:
        file.write(f"{timestamp}: IDE not found\n")

    # Populate shot data with chosen shots from the list
    for shot_info in shot_list:  
        shotnumber, tcIps, tcIpe = shot_info
        shot_data[shotnumber] = {}
        try:
            for dia in diagnostics:
                # Attempt to store data for the current shot number
                diag_data = fetch_data.fetch_diag_data(shotnumber, dia, exps, tcIps = tcIps, tcIpe = tcIpe)
                shot_data[shotnumber][dia] = diag_data
                print(f"---Saved {shotnumber}: {dia} to dict---")
        except Exception as e:
            print(f"Error populating data for shot {shotnumber}: {e}")
    
    utils.save_shot_data(pickle_file, shot_data)
    return 


if __name__ == "__main__":
    # Populate the shot data
    shotlist_file = 'shotlist.txt'
    pickle_file = 'shot_data.pickle'
    diag = ['IDE', 'EQH']
    exps = ['lrado', 'micdu']

    shot_list = utils.readShotlist(shotlist_file)
    populate_data_from_list(shot_list, diag, exps, pickle_file)
    shotnumber = 40128

    loaded_shot_data = utils.load_shot_data(pickle_file)

    #print('keys')
    print(loaded_shot_data.keys())
    #print(loaded_shot_data[shotnumber])
    print(loaded_shot_data[shotnumber].keys())

    if shotnumber in loaded_shot_data:
        temp_data = loaded_shot_data[shotnumber]['IDE']
        #print(temp_data)
    else:
        print(f"No data available for shot number {shotnumber}")
        
    #print(loaded_shot_data[shotnumber]['ide'])
    #temp_Data = loaded_shot_data[shotnumber]['IDE']
    #print(temp_data)

