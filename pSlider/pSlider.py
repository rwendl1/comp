import aug_sfutils as sf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import argparse
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def update(val, sfEQH, EQH, sfIDE, IDE, ax, shotnumber, slider):
    time = slider.val
    ax.clear()

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax.plot(rhop, pres/1000 , label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')

    if sfIDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 12)
    ax.set_ylabel('Pressure [kPa]', fontsize = 12)
    ax.legend(fontsize = 12)
    ax.set_title(f'Discharge {shotnumber} @ {time:.2f}s')
    plt.draw()

def main(shotnumber, time=None, print_plot=True):
    # Fetch EQH data from AUGD
    sfEQH = sf.SFREAD(shotnumber, 'eqh')
    if sfEQH.status:    
        EQH = sf.EQU(shotnumber, diag = 'eqh')

    # Fetch IDE data from AUGD
    sfIDE = sf.SFREAD(shotnumber, 'ide')
    IDE = None
    if sfIDE.status:
        IDE = sf.EQU(shotnumber, diag = 'ide')

    if time is None:
        ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
        t_start, t_end, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
        time = round(t_start + ((t_end - t_start) / 2), 2)

    # Create a subplot grid for the current shot number
    fig, ax = plt.subplots(figsize=(6, 5))
    plt.subplots_adjust(left=0.1, bottom=0.25)

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax.plot(rhop, pres/1000 , label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')

    if IDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 12)
    ax.set_ylabel('Pressure [kPa]', fontsize = 12)
    ax.legend(fontsize = 12)
    ax.set_title(f'Discharge {shotnumber} @ {time:.2f}s')

    ax_slider = plt.axes([0.1, 0.05, 0.8, 0.03], facecolor='lightgoldenrodyellow')
    slider = Slider(ax_slider, 'Time', EQH.time[0], EQH.time[-1], valinit=time)

    ax.annotate('hello', xy=(2, -1), xycoords='data', xytext=(2, 1.5),
                textcoords='data', arrowprops=dict(facecolor='black', shrink=0.05))

    # Add custom ticks to the slider
    custom_ticks = [EQH.time[10], EQH.time[200]]
    ax_slider.xaxis.set_visible(True)
    ax_slider.set_xticks(custom_ticks)
    ax_slider.set_xticklabels([f'{custom_ticks[0]:.2f}', f'{custom_ticks[1]:.2f}'])

    slider.on_changed(lambda val: update(val, sfEQH, EQH, sfIDE, IDE, ax, shotnumber, slider))

    plt.show()

    if print_plot:
        plt.savefig(f'figs/pPlot/pSlider_{shotnumber}_{time}s.png')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot data for a specific shot number and time.')
    parser.add_argument('shotnumber', type=int, help='The shot number to plot data for')
    parser.add_argument('time', type=float, nargs='?', default=None, help='The time (in seconds) to plot data for (optional)')

    args = parser.parse_args()
    main(args.shotnumber, args.time)
