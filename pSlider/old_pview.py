import tkinter as tk
from tkinter import ttk
import aug_sfutils as sf
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.widgets import Slider
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def safe_sqrt(array):
    array = np.nan_to_num(array, nan=0.0, posinf=0.0, neginf=0.0)
    return np.sqrt(np.where(array >= 0, array, 0))

def update(val, sfEQH, EQH, sfIDE, IDE, ax, shotnumber, slider):
    time = slider.val
    ax.clear()

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        psiN = EQH.psiN[idx_EQH,:]
        psiN[0] = 0.0
        rhop = np.sqrt(psiN)
        ax.plot(rhop, pres/1000 , label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')


    if sfIDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        psiN = IDE.psiN[idx_IDE,:]
        psiN[0] = 0.0
        rhop = np.sqrt(psiN)
        ax.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 12)
    ax.set_ylabel('Pressure [kPa]', fontsize = 12)
    ax.legend(fontsize = 12)
    ax.set_title(f'Discharge {shotnumber} @ {time:.2f}s')
    plt.draw()

def plot_data(shotnumber, time=None, print_plot=True):
    # Fetch EQH data from AUGD
    sfEQH = sf.SFREAD(shotnumber, 'eqh')
    if sfEQH.status:    
        EQH = sf.EQU(shotnumber, diag = 'eqh')

    # Fetch IDE data from AUGD
    sfIDE = sf.SFREAD(shotnumber, 'ide')
    IDE = None
    if sfIDE.status:
        IDE = sf.EQU(shotnumber, diag = 'ide')

    if time is None:
        ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
        t_start, t_end, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
        time = round(t_start + ((t_end - t_start) / 2), 2)

    # Create a subplot grid for the current shot number
    fig, ax = plt.subplots(figsize=(6, 5))
    plt.subplots_adjust(left=0.1, bottom=0.25)

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        psiN = EQH.psiN[idx_EQH,:]
        psiN[0] = 0.0
        rhop = np.sqrt(psiN)
        ax.plot(rhop, pres/1000 , label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')



    if IDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        psiN = IDE.psiN[idx_IDE,:]
        psiN[0] = 0.0
        rhop = np.sqrt(psiN)
        ax.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 12)
    ax.set_ylabel('Pressure [kPa]', fontsize = 12)
    ax.legend(fontsize = 12)
    ax.set_title(f'Discharge {shotnumber} @ {time:.2f}s')

    ax_slider = plt.axes([0.1, 0.05, 0.8, 0.03], facecolor='lightgoldenrodyellow')
    slider = Slider(ax_slider, 'Time', EQH.time[0], EQH.time[-1], valinit=time)
    
    # Add custom ticks to the slider
    custom_ticks = [EQH.time[0], EQH.time[-1]]
    ax_slider.set_xticks(custom_ticks)
    ax_slider.set_xticklabels([f'{custom_ticks[0]:.2f}', f'{custom_ticks[1]:.2f}'])
    
    slider.on_changed(lambda val: update(val, sfEQH, EQH, sfIDE, IDE, ax, shotnumber, slider))

    return fig, ax, slider

def on_plot_button_click():
    shotnumber = int(shotnumber_entry.get())
    fig, ax, slider = plot_data(shotnumber)
    canvas = FigureCanvasTkAgg(fig, master=plot_frame)
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
    # Update the global canvas object to allow further updates
    global current_canvas
    if current_canvas is not None:
        current_canvas.get_tk_widget().pack_forget()
    current_canvas = canvas

# Create the main window
root = tk.Tk()
root.title("Plasma Equilibria Plotter")

# Create and grid the widgets
ttk.Label(root, text="Shot Number:").grid(row=0, column=0, padx=10, pady=10)
shotnumber_entry = ttk.Entry(root)
shotnumber_entry.grid(row=0, column=1, padx=10, pady=10)

plot_button = ttk.Button(root, text="Plot", command=on_plot_button_click)
plot_button.grid(row=1, column=0, columnspan=2, pady=10)

plot_button = ttk.Button(root, text="Reset", command=on_plot_button_click)
plot_button.grid(row=2, column=0, columnspan=2, pady=10)

# Frame for the plot
plot_frame = ttk.Frame(root)
plot_frame.grid(row=2, column=0, columnspan=2, padx=10, pady=10)

current_canvas = None

# Start the GUI event loop
root.mainloop()
