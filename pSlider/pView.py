import tkinter as tk
from tkinter import ttk
import aug_sfutils as sf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.widgets import Slider

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def update(val, sfEQH, EQH, sfIDE, IDE, ax, shotnumber, slider, var_name, scale=1.0):
    time = slider.val
    ax.clear()

    if var_name == 'q':
        ylabel = 'Safety factor q'
        scale = 1
    elif var_name == 'pres':
        ylabel = 'Pressure [kPa]'
        scale = 1/1000

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        val = getattr(EQH, var_name)[idx_EQH, :]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax.plot(rhop, val * scale, label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')

    if sfIDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        val = getattr(IDE, var_name)[idx_IDE, :]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax.plot(rhop, val * scale, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 12)
    ax.set_ylabel(ylabel, fontsize = 12)
    ax.legend(fontsize = 12)
    ax.set_title(f'Discharge {shotnumber} @ {time:.2f}s')
    plt.draw()

def plot_data(shotnumber, var_name, time=None, scale=1.0):
    # Fetch EQH data from AUGD
    sfEQH = sf.SFREAD(shotnumber, 'eqh')
    if sfEQH.status:    
        EQH = sf.EQU(shotnumber, diag = 'eqh')
        if time is None:
            time = EQH.time[-1]/2 # Default time      

    # Fetch IDE data from AUGD
    sfIDE = sf.SFREAD(shotnumber, 'ide')
    IDE = None
    if sfIDE.status:
        IDE = sf.EQU(shotnumber, diag = 'ide')

    # Create a subplot grid for the current shot number
    fig, ax = plt.subplots(figsize=(6, 5))
    plt.subplots_adjust(left=0.1, bottom=0.25)

    if var_name == 'q':
        ylabel = 'Safety factor q'
        scale = 1
    elif var_name == 'pres':
        ylabel = 'Pressure [kPa]'
        scale = 1/1000

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        val = getattr(EQH, var_name)[idx_EQH, :]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax.plot(rhop, val * scale, label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')

    if IDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        val = getattr(IDE, var_name)[idx_IDE, :]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax.plot(rhop, val * scale, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 12)
    ax.set_ylabel(ylabel, fontsize = 12)
    ax.legend(fontsize = 12)
    ax.set_title(f'Discharge #{shotnumber} @ {time:.2f}s')

    ax_slider = plt.axes([0.1, 0.05, 0.8, 0.03], facecolor='lightgoldenrodyellow')
    slider = Slider(ax_slider, 'Time', EQH.time[0], EQH.time[-1], valinit=time)
    
    # Add custom ticks to the slider
    custom_ticks = [EQH.time[0], EQH.time[-1]]
    ax_slider.set_xticks(custom_ticks)
    ax_slider.set_xticklabels([f'{custom_ticks[0]:.2f}', f'{custom_ticks[1]:.2f}'])
    
    slider.on_changed(lambda val: update(val, sfEQH, EQH, sfIDE, IDE, ax, shotnumber, slider, var_name, scale))

    return fig, ax, slider

def on_plot_button_click(var_name):
    shotnumber = int(shotnumber_entry.get())
    fig, ax, slider = plot_data(shotnumber, var_name)
    
    # Create a new top-level window for the plot
    plot_window = tk.Toplevel(root)
    plot_window.title(f'Plot for Shot Number {shotnumber}')
    
    # Create a canvas and attach it to the new window
    canvas = FigureCanvasTkAgg(fig, master=plot_window)
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
    
    # Store the canvas object to allow further updates
    global current_canvas
    if current_canvas is not None:
        current_canvas.get_tk_widget().pack_forget()
    current_canvas = canvas

def on_close():
    root.quit()
    root.destroy()


if __name__ == "__main__":

    # Create the main window
    root = tk.Tk()
    root.title("Plasma Equilibria Plotter")

    # Create and grid the widgets
    ttk.Label(root, text="Shot Number:").grid(row=0, column=0, padx=10, pady=10)
    shotnumber_entry = ttk.Entry(root)
    shotnumber_entry.grid(row=0, column=1, padx=10, pady=10)

    # Create buttons for plotting different variables
    plot_button_pres = ttk.Button(root, text="Plot p-Profile", command=lambda: on_plot_button_click('pres'))
    plot_button_pres.grid(row=1, column=0, columnspan=2, pady=10)

    plot_button_q = ttk.Button(root, text="Plot q-Profile", command=lambda: on_plot_button_click('q'))
    plot_button_q.grid(row=2, column=0, columnspan=2, pady=10)

    close_button = ttk.Button(root, text="Close", command=on_close)
    close_button.grid(row=4, column=0, columnspan=2, pady=10)

    # Frame for the plot
    plot_frame = ttk.Frame(root)
    plot_frame.grid(row=3, column=0, columnspan=2, padx=10, pady=10)

    current_canvas = None

    # Start the GUI event loop
    root.mainloop()
