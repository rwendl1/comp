import aug_sfutils as sf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import argparse
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def update(val, sfEQH, EQH, sfIDE, IDE, ax1, ax2, shotnumber, slider, IpiFP, IpiFP_TB):
    time = slider.val
    ax1.clear()
    ax2.clear()

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax1.plot(rhop, pres/1000, label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax1.axhline(0, c='k', linestyle='dotted')

    if sfIDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax1.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax1.set_xlabel(r'$\rho_{pol}$', fontsize=12)
    ax1.set_ylabel('Pressure [kPa]', fontsize=12)
    ax1.legend(fontsize=12)
    ax1.set_title(f'Discharge {shotnumber} @ {time:.2f}s')

    ax2.plot(IpiFP_TB, IpiFP, label='IpiFP')
    ax2.axvline(time, color='r', linestyle='--', label=f'Time @ {time:.2f}s')
    ax2.set_xlabel('Time [s]', fontsize=12)
    ax2.set_ylabel('Plasma Current [A]', fontsize=12)
    ax2.legend(fontsize=12)

    plt.draw()

def main(shotnumber, time=None, print_plot=False):
    # Fetch EQH data from AUGD
    sfEQH = sf.SFREAD(shotnumber, 'eqh')
    if sfEQH.status:    
        EQH = sf.EQU(shotnumber, diag='eqh')

    # Fetch IDE data from AUGD
    sfIDE = sf.SFREAD(shotnumber, 'ide')
    IDE = None
    if sfIDE.status:
        IDE = sf.EQU(shotnumber, diag='ide')

    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    IpiFP_TB, IpiFP = ipc_obj['IpiFP_TB'], ipc_obj['IpiFP']
    
    if time is None:
        t_start, t_end, avg = utils.filter_and_trim_current(IpiFP_TB, IpiFP, 3, 10, True)
        time = round(t_start + ((t_end - t_start) / 2), 2)

    # Create a subplot grid for the current shot number
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 10))
    plt.subplots_adjust(left=0.1, bottom=0.25, hspace=0.4)

    if sfEQH.status:
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax1.plot(rhop, pres/1000, label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax1.axhline(0, c='k', linestyle='dotted')

    if IDE is not None and sfIDE.status:
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax1.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax1.set_xlabel(r'$\rho_{pol}$', fontsize=12)
    ax1.set_ylabel('Pressure [kPa]', fontsize=12)
    ax1.legend(fontsize=12)
    ax1.set_title(f'Discharge {shotnumber} @ {time:.2f}s')

    ax2.plot(IpiFP_TB, IpiFP, label='IpiFP')
    ax2.axvline(time, color='r', linestyle='--', label=f'Time @ {time:.2f}s')
    ax2.set_xlabel('Time [s]', fontsize=12)
    ax2.set_ylabel('Plasma Current [A]', fontsize=12)
    ax2.legend(fontsize=12)

    ax_slider = plt.axes([0.1, 0.05, 0.8, 0.03], facecolor='lightgoldenrodyellow')
    slider = Slider(ax_slider, 'Time', EQH.time[0], EQH.time[-1], valinit=time)

    slider.on_changed(lambda val: update(val, sfEQH, EQH, sfIDE, IDE, ax1, ax2, shotnumber, slider, IpiFP, IpiFP_TB))

    plt.show()

    if print_plot:
        plt.savefig(f'figs/pPlot/pSlider_{shotnumber}_{time}s.png')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot data for a specific shot number and time.')
    parser.add_argument('shotnumber', type=int, help='The shot number to plot data for')
    parser.add_argument('time', type=float, nargs='?', default=None, help='The time (in seconds) to plot data for (optional)')

    args = parser.parse_args()
    main(args.shotnumber, args.time)
