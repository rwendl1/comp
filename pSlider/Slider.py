import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import aug_sfutils as sf
import argparse
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

# Assuming find_nearest is a utility function you have defined
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

class CombinedSliderPlot:
    def __init__(self, shotnumber, var_name, scale=1, time = None):
        # Fetch EQH data from AUGD
        sfEQH = sf.SFREAD(shotnumber, 'eqh')
        if sfEQH.status:    
            EQH = sf.EQU(shotnumber, diag = 'eqh')

        # Fetch IDE data from AUGD
        sfIDE = sf.SFREAD(shotnumber, 'ide')
        IDE = None
        if sfIDE.status:
            IDE = sf.EQU(shotnumber, diag = 'ide')
        self.EQH = EQH
        self.IDE = IDE
        self.var_name = var_name
        self.scale = scale
        self.fig, self.ax = plt.subplots()
        plt.subplots_adjust(bottom=0.25)

        # Define a mapping of var_names to axis labels
        self.axis_labels = {
            'pres': (r'$\rho_{pol}$', 'Pressure [Pa]'),
            'q': (r'$\rho_{pol}$', 'Safety Factor q'),
            'pfl': (r'$\rho_{pol}$', 'Poloidal Flux [Vs]'),
            'ipipsi': (r'$\rho_{pol}$', 'Poloidal Flux [Vs]'),
            'pfl': (r'$\rho_{pol}$', 'Poloidal Flux [Vs]'),
            # Add more mappings as necessary
        }

        if time is None:
            ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
            t_start, t_end, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
            time = round(t_start + ((t_end - t_start) / 2), 2)

        # Slider setup
        
        self.slider_ax = plt.axes([0.25, 0.1, 0.65, 0.03])
        self.slider = Slider(self.slider_ax, 'Time', 0.0, 10.0, valinit=0.0)
        self.slider.on_changed(self.update_plot)
        # Add custom ticks to the slider
        custom_ticks = [EQH.time[10], EQH.time[200]]
        self.slider_ax.xaxis.set_visible(True)
        self.slider_ax.set_xticks(custom_ticks)
        self.slider_ax.set_xticklabels([f'{custom_ticks[0]:.2f}', f'{custom_ticks[1]:.2f}'])
        print(time)
        self.update_plot(time)

    def update_plot(self, time):
        self.ax.clear()
        # Your plotting logic here, adapted from the active selection
        # For example:
        if self.EQH:
            idx_EQH = find_nearest(self.EQH.time, time)
            val = getattr(self.EQH, self.var_name)[idx_EQH, :]
            rhop = np.sqrt(self.EQH.psiN[idx_EQH,:])
            self.ax.plot(rhop, val * self.scale, label=f'EQH @ {self.EQH.time[idx_EQH]:.3f}s')
            self.ax.axhline(0, c='k', linestyle='dotted')

        if self.IDE:
            idx_IDE = find_nearest(self.IDE.time, time)
            val = getattr(self.IDE, self.var_name)[idx_IDE, :]
            rhop = np.sqrt(self.IDE.psiN[idx_IDE,:])
            self.ax.plot(rhop, val * self.scale, label=f'IDE @ {self.IDE.time[idx_IDE]:.3f}s')

        # Set axis labels based on var_name
        if self.var_name in self.axis_labels:
            xlabel, ylabel = self.axis_labels[self.var_name]
            self.ax.set_xlabel(xlabel)
            self.ax.set_ylabel(ylabel)

        self.ax.legend()
        self.fig.canvas.draw_idle()

# Example usage
# You would need to replace `EQH` and `IDE` with actual instances of your data classes
shotnumber = 40128
var_name = 'ipipsi'
combined_plot = CombinedSliderPlot(shotnumber, var_name)
plt.savefig('figs/slider/test.png')
plt.show()