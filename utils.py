import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import math
from scipy.signal import savgol_filter
#import pickleboi

def readShotlist(shotlist_file):
    """ read shotlist from shotlist_file.txt """
    dir = str(os.path.dirname(__file__))            #gets directory of executed file
    shotlist_file = dir + "/" + shotlist_file
    # Read shot list from file
    try:
        with open(shotlist_file, 'r') as file:
            shot_list = eval(file.read())  # Assume shot list file contains a valid Python list
    except FileNotFoundError:
        print(f"Shot list file '{shotlist_file}' not found.")
        exit(1)
    return shot_list

def readShotlistData(shotlist_file):
    """ read data from shotlist from shotlist_file.txt """
    shot_list = readShotlist(shotlist_file)
    shotnumbers = []
    tcIps_list = []
    tcIpe_list = []
    for shot_info in shot_list:  
        shotnumber, tcIps, tcIpe = shot_info
        shotnumbers.append(shotnumber)
        tcIps_list.append(tcIps)
        tcIpe_list.append(tcIpe)
    return shotnumbers, tcIps_list, tcIpe_list

def load_shot_data(filename):  
    """-- Load shot data from a file if it exists, otherwise return an empty dictionary --"""
    dir = str(os.path.dirname(__file__))            #gets directory of executed file
    filename = dir + "/" + filename                                 
    if os.path.exists(filename):
        with open(filename, 'rb') as file:
            return pickle.load(file)
    else:
        print("!!!!!!!!!Picklefile not found!!!!!!!!!")
        exit(1)
        return {}

def save_shot_data(filename, shot_data):
    """-- Save shot data to a file called filename --"""                       
    with open(filename, 'wb') as file:
        pickle.dump(shot_data, file)
        print("---shotdata pickled---")


def find_idx_of_nearest(array, value): 
    """ finds index of array whose array-value is closest to input value """             
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def create_timestamps(eqh_array, ide_array, start_time, end_time, delta_t = 1.0, full_secs = False):
    """ creates timestamp and indices list of two input array with a start time and end time where delta_t is the time between timestamps  """
    times = []
    ide_times = []
    eqh_idxs = [] 
    ide_idxs = [] 
    acc_threshold = 0.050


    if full_secs:
        current_time = math.ceil(start_time)
    else:
        current_time = start_time

    #iterate over array starting from start time to end time and add the indexes of those tto index list
    while current_time <= end_time:
        eqh_idx = find_idx_of_nearest(eqh_array, current_time)
        ide_idx = find_idx_of_nearest(ide_array, current_time)

        #check that the found timestap is not more than 50ms away from actual timestep
        if acc_threshold <= abs(eqh_array[eqh_idx]-current_time):
            print("EQH timestep not accurate enough - getting different one")
            current_time = current_time + (acc_threshold * 1.5)

        elif  acc_threshold <= abs(ide_array[ide_idx]-current_time):
            print("IDE timestep not accurate enough - getting different one")
            current_time = current_time + (acc_threshold * 1.5)
                
        else:
            times.append(eqh_array[eqh_idx])
            ide_times.append(ide_array[ide_idx])

            eqh_idxs.append(eqh_idx)
            ide_idxs.append(ide_idx)
            current_time += delta_t
            
   
    return times, eqh_idxs, ide_idxs

def angle_to_horizontal(point1, point2):
    """
    Calculate the angle (in radians) to the horizontal axis between two points.
    """
    dx = point2[0] - point1[0]
    dy = point2[1] - point1[1]
    angle_rad = math.atan2(dy, dx)
    #angle_deg = math.degrees(angle_rad)
    return angle_rad

def mag_center_offset(eqh_scls, ide_scls, theta):
    """
    Calculates the difference of 2 magnetic centers in the direction of angle for an array of angles theta
    the distances are pos/negative measured from the second passed point 
    """

    point1 = [eqh_scls['Rmag'], eqh_scls['Zmag']]
    point2 = [ide_scls['Rmag'], ide_scls['Zmag']] 
    # Convert points to numpy arrays
    point1 = np.array(point1)
    point2 = np.array(point2)

    distances = [] 

    dis_points = np.sqrt((point2[0]-point1[0]) **2 + (point2[1]-point1[1])**2)      #length between the 2 points
    angle2horz = angle_to_horizontal(point1, point2)                          #angle to the horizontal axis, zero is x axis in right direction
    gamma = np.pi - angle2horz

    for angle in theta:
        distance = dis_points * np.cos(gamma + angle)
        distances.append(distance)

    return np.array(distances)

def find_times(time_array, start_time, end_time, delta_t):
    """ creates timestamp list of array with a start time and end time and delta t between timestamps  """
    times = []
    current_time = start_time

    #iterate over array starting from start time to end time and add the indexes of those tto index list
    while current_time <= end_time:
        cur_idx = find_idx_of_nearest(time_array, current_time)
        times.append(time_array[cur_idx])
        current_time += delta_t      
    return times

def find_time_indexes(time_array, start_time, end_time, delta_t):
    """ creates indices list of array with a start time and end time and delta t between indeces """ 

    indexes = []
    current_time = start_time
    acc_threshold = 0.050

    #iterate over array starting from start time to end time and add the indexes of those tto index list
    while current_time <= end_time:
        cur_idx = find_idx_of_nearest(time_array, current_time)
        #check that the found timestap is not more than 50ms away from actual timestep
        if abs(time_array[cur_idx]-current_time) <= acc_threshold:
            indexes.append(cur_idx)
            current_time += delta_t
        else:
            print("timestep not accurate enough - getting another one")
            current_time +=  acc_threshold * 2
    return indexes

def filter_and_trim_current(time, current, cutoff_percentage=5, trim_percentage=10, smoothing = False):
    current = np.array(current)
    time = np.array(time)
    
    # Handle negative currents by inverting the array if the mean is negative
    if np.mean(current) < 0:
        current = -current

    if smoothing:
        # Smooth the current signal using a Savitzky-Golay filter
        current = savgol_filter(current, 101, 3)
    
    while True:
        avg_current = np.mean(current)
        cutoff_value = avg_current * cutoff_percentage / 100
        
        # Calculate the lower and upper bounds
        lower_bound = avg_current - cutoff_value
        
        # Filter the current array based on the bound
        filtered_indices = (current >= lower_bound)
        filtered_current = current[filtered_indices]
        filtered_time = time[filtered_indices]
        
        # If the filtered array is the same as the current array, we are done
        if np.array_equal(filtered_current, current):
            break
        
        # Update current and time arrays for the next iteration
        current = filtered_current
        time = filtered_time
    
    # Perform trimming at the end
    trim_count = int(len(current) * trim_percentage / 100)
    trimmed_time = time[trim_count:-trim_count]
    
    # Return the start and end time of the trimmed array
    return trimmed_time[0], trimmed_time[-1], avg_current



def get_signal_objects(Geq, *signal_names, TB=[]):
    signal_objects = {}
    for signal_name in signal_names:
        try:
            #print('getting ', signal_name)
            signal_objects[signal_name] = Geq.getobject(signal_name)
            if signal_name in TB:
                signal_objects[str(signal_name+'_TB')] = Geq.gettimebase(signal_name)
        except Exception as e:
            print(f"Error getting object for signal '{signal_name}': {e}")
            signal_objects[signal_name] = None
    return signal_objects


def get_sig_from_dia(shotnumber, diag, signal_name):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        signal = sfr.getobject(signal_name)
        timebase = sfr.gettimebase(signal_name)
    else:
        signal = None
        timebase = None
    return signal, timebase