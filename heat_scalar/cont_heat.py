import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button
import numpy as np
import math
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils
import pickle
import privmapeq
import pandas as pd
import seaborn as sns

def populate_data(data, shotnumber, rhop, t_res, output_file):

    diagnostic = ['EQH', 'IDE']
    diagnostic_g = ['GQH', 'IDG']

    prev_data = data

    data[shotnumber]={}

    #create start adn end times of constant plasma current
    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)

    data[shotnumber]={
        'IpiFP': ipc_obj['IpiFP'],
        'IpiFP_TB': ipc_obj['IpiFP_TB'],
        'start_time': start_time,
        'end_time': end_time,
    }
    
    #populate different dias with scalarvalues
    for g, dia in enumerate(diagnostic):

        data[shotnumber][dia]={}

        Geq = sf.SFREAD(shotnumber, diagnostic_g[g])  # Check if EQI shotfile exists
        if Geq.status:
            signal_objects = utils.get_signal_objects(Geq, 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu')
            timebase = Geq.getobject('TIMEF')

            data[shotnumber][dia]={
                **signal_objects,
                'time': timebase
            }
        else:
            print(f'{diagnostic_g[g]} not found')
            return prev_data

    times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(data[shotnumber]['EQH']['time'], data[shotnumber]['IDE']['time'], start_time, end_time, t_res)

    #load EQU shotfile for R, z
    eqh_equ = sf.EQU(shotnumber, diag='EQH')
    ide_equ = sf.EQU(shotnumber, diag='IDE')


    zaus_eqh_list = []
    zaus_ide_list = []
    for j, time in enumerate(times):

        eqh_idx = eqh_time_idxs[j]
        ide_idx = ide_time_idxs[j]

        eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=time, coord_in='rho_pol')
        ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=time, coord_in='rho_pol')

        #calculates the zaus from contour
        zaus_idx_eqh = utils.find_idx_of_nearest(eqh_r2[0][0], data[shotnumber]['EQH']['Raus'][eqh_idx])
        zaus_idx_ide = utils.find_idx_of_nearest(ide_r2[0][0], data[shotnumber]['IDE']['Raus'][ide_idx])

        zaus_eqh = eqh_z2[0][0][zaus_idx_eqh]
        zaus_ide = ide_z2[0][0][zaus_idx_ide]

        zaus_eqh_list.append(zaus_eqh)
        zaus_ide_list.append(zaus_ide)

    data[shotnumber]['EQH']['Zaus'] = zaus_eqh_list
    data[shotnumber]['IDE']['Zaus'] = zaus_ide_list
    data[shotnumber]['IDE']['Zaus_times'] = times


    # Save data to a file
    with open(output_file, 'wb') as f:
        pickle.dump(data, f)   

    # Load data from file to reset it
    with open(output_file, 'rb') as f:
        data = pickle.load(f)

    return data




def sca_diff(scalar, IDE, EQH,  ide_t_idx, eqh_t_idx):
    return EQH[scalar][eqh_t_idx] - IDE[scalar][ide_t_idx]

def arr_threshold(arr1, arr2, arr_ref, percentage = 95):

    arr1 = np.array(arr1)
    arr2 = np.array(arr2)
    arr_ref = np.array(arr_ref)

    threshold = np.percentile(arr_ref, percentage)

    # Create a mask for values below the threshold
    mask = arr_ref <= threshold

    # Filter the lists using the mask
    arr1 = arr1[mask]
    arr2 = arr2[mask]
    arr_ref = arr_ref[mask]

    return arr1, arr2, arr_ref


def find_90th_percentile_value(arr):
    """
    Finds the value at the 90% mark of all the values in a NumPy array.

    Parameters:
    arr (np.array): The input NumPy array.

    Returns:
    float: The value at the 90% mark.
    """
    # Sort the array
    sorted_arr = np.sort(arr)
    
    # Calculate the index for the 90% mark
    index_90 = math.floor(len(sorted_arr) * 0.9) - 1  # Subtracting 1 because indices start at 0
    
    # Access the value at this index
    value_at_90 = sorted_arr[index_90]
    
    return value_at_90



def main_norm():
    pickle_file = './data_files/sca_diff_data_zaus.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE_temp2.csv'
    save_file = 'figs/cont_heat/cont_heat_norm.png'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = False                  #define if new dataset for picklefile shall be created
    rhop = [1.0]

    t_res = 0.1
    ref_shotnumber = 40263

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, rhop, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)

    #create start adn end times of constant plasma current for refernce shotnuber
    ipc_obj = utils.get_signal_objects(sf.SFREAD(ref_shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
    ref_ide_times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(scalar_data[ref_shotnumber]['EQH']['time'], scalar_data[ref_shotnumber]['IDE']['time'], start_time, end_time, t_res)

    ref_ide_idx = ide_time_idxs[int(len(ref_ide_times)/2)]

    ref_Rmag = scalar_data[ref_shotnumber]['IDE']['Rmag'][ref_ide_idx]
    ref_Zmag = scalar_data[ref_shotnumber]['IDE']['Zmag'][ref_ide_idx]
    ref_Raus = scalar_data[ref_shotnumber]['IDE']['Raus'][ref_ide_idx]
    ref_Rxpu = scalar_data[ref_shotnumber]['IDE']['Rxpu'][ref_ide_idx]
    ref_Zxpu = scalar_data[ref_shotnumber]['IDE']['Zxpu'][ref_ide_idx]

    diff_Rmag = []
    diff_Zmag = []
    diff_Raus = []
    diff_Rxpu = []
    diff_Zxpu = []

    mag_dis = []
    aus_dis = []
    xpoi_dis = []

    #load EQU shotfile for R, z
    ide_equ = sf.EQU(ref_shotnumber, diag='IDE')

    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=ref_ide_times[int(len(ref_ide_times)/2)], coord_in='rho_pol')

    Zaus_idx = utils.find_idx_of_nearest(ide_r2[0][0], ref_Raus)
    ref_Zaus = ide_z2[0][0][Zaus_idx]

    plt.figure(1, figsize=(12, 9))
    plt.title(f"Histogram of scalar differences over contour" )
    plt.plot(ide_r2[0][0], ide_z2[0][0], ls = 'dotted', label=f'IDE reference contour #{ref_shotnumber} @ {ref_ide_times[int(len(ref_ide_times)/2)]:.2f}s', c='b')
    #plt.legend(loc='center left', bbox_to_anchor=(0, -0.115))
    #plt.legend(loc='best')

    #plot magnetic centers
    plt.plot(ref_Rmag, ref_Zmag, 'rx', label='IDE mag center')
    plt.plot(ref_Raus, ref_Zaus, 'rx', label='IDE Raus')
    plt.plot(ref_Rxpu, ref_Zxpu, 'rx', label='IDE X-Point')

    #plot the center axes
    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
    plt.axhline(y=ref_Zmag, color='blue', linestyle='--', linewidth=1)

    for i, shotnumber in enumerate(shotnumbers):

        if shotnumber in scalar_data.keys():

            #checks if the EQH and IDE are keys in dict and goes to next shot if not so
            if not 'EQH' in scalar_data[shotnumber].keys() or not 'IDE' in scalar_data[shotnumber].keys():
                print(f'EQH or IDE data in pickle dict for {shotnumber} was empty')
                continue

            eqh_scls = scalar_data[shotnumber]['EQH']
            ide_scls = scalar_data[shotnumber]['IDE']

            #checks if the dicts are empty and goes to next shot if so
            if not bool(eqh_scls) or not bool(ide_scls):
                print('dict of EQH or IDE for that shot was empty')
                continue

            #create start adn end times of constant plasma current
            ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
            start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
            if end_time - start_time < 1.0:
                print('discharge to short, prob not const Ip')
            
            else:
                times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(eqh_scls['time'], ide_scls['time'], start_time, end_time, t_res)
                
                for j, time in enumerate(times):
                    eqh_t_idx = eqh_time_idxs[j]
                    ide_t_idx = ide_time_idxs[j]

                    #'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu'...............  EQH-IDE
                    cur_diff_Rmag = sca_diff('Rmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zmag = sca_diff('Zmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Raus = sca_diff('Raus', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Rxpu = sca_diff('Rxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zxpu = sca_diff('Zxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)

                    cur_mag_dis = np.sqrt((cur_diff_Rmag)**2 + (cur_diff_Zmag)**2)
                    cur_aus_dis = abs(cur_diff_Raus)
                    cur_xpoi_dis = np.sqrt((cur_diff_Rxpu)**2 + (cur_diff_Zxpu)**2)

                    diff_Rmag.append(cur_diff_Rmag)
                    diff_Zmag.append(cur_diff_Zmag)
                    diff_Raus.append(cur_diff_Raus)
                    diff_Rxpu.append(cur_diff_Rxpu)
                    diff_Zxpu.append(cur_diff_Zxpu)

                    mag_dis.append(cur_mag_dis)
                    aus_dis.append(cur_aus_dis)
                    xpoi_dis.append(cur_xpoi_dis)

    #plt.plot(diff_Rmag, diff_Zmag, 'bx', label='IDE mag center')
    ref_Zaus_list = []
    for k, val in enumerate(diff_Raus):
        ref_Zaus_list.append(ref_Zaus)

    scale = 7
    
    #converts list to np array and cuts of all the values above the percentage based on distance
    diff_Rmag, diff_Zmag, mag_dis = arr_threshold(diff_Rmag, diff_Zmag, mag_dis, percentage = 98)
    diff_Raus, ref_Zaus_list, aus_dis = arr_threshold(diff_Raus, ref_Zaus_list, aus_dis, percentage = 98)
    diff_Rxpu, diff_Zxpu, xpoi_dis = arr_threshold(diff_Rxpu, diff_Zxpu, xpoi_dis, percentage = 98)

    plot_Rmag = find_90th_percentile_value(diff_Rmag)
    plot_Zmag = find_90th_percentile_value(diff_Zmag) 

    """ plt.scatter(scale * diff_Rmag + ref_Rmag, scale * diff_Zmag + ref_Zmag, c=mag_dis, cmap='plasma')
    plt.colorbar(label='Mag Difference (m)')
    plt.scatter(scale * diff_Raus + ref_Raus, ref_Zaus_list, c=aus_dis, cmap='plasma')
    plt.colorbar(label='Aus Difference (m)')
    plt.scatter(scale * diff_Rxpu + ref_Rxpu, scale * diff_Zxpu + ref_Zxpu, c=xpoi_dis, cmap='plasma')
    plt.colorbar(label='XPoi Difference (m)') """


    """ plt.scatter(diff_Rmag, diff_Zmag, c=mag_dis, cmap='plasma')
    plt.colorbar(label='Mag Difference (m)')
    plt.scatter(diff_Raus, ref_Zaus_list, c=aus_dis, cmap='plasma')
    plt.colorbar(label='Aus Difference (m)')
    plt.scatter(diff_Rxpu, diff_Zxpu, c=xpoi_dis, cmap='plasma')
    plt.colorbar(label='XPoi Difference (m)') """

    #plt.plot(scale * plot_Rmag + ref_Rmag, ref_Zmag, 'ro')
    #plt.text(scale * plot_Rmag + ref_Rmag, ref_Zmag, f'{plot_Rmag:.2f}mm', color='red', fontsize=9)  # Adjust text position as needed

    # Plot hexbins
    hb2 = plt.hexbin(scale * diff_Raus + ref_Raus, ref_Zaus_list, bins = 'log', gridsize=50,  cmap='plasma')
    cb2 = plt.colorbar(hb2, label='R_aus Count', fontsize = 14)

    hb3 = plt.hexbin(scale * diff_Rxpu + ref_Rxpu, scale * diff_Zxpu + ref_Zxpu, bins = 'log', gridsize=50, cmap='plasma')
    cb3 = plt.colorbar(hb3, label='X-Point Count', fontsize = 14)

    hb1 = plt.hexbin(scale * diff_Rmag + ref_Rmag, scale * diff_Zmag + ref_Zmag, bins = 'log', gridsize=50, cmap='plasma')
    cb1 = plt.colorbar(hb1, label='Magnetic Center Count', fontsize = 14)

    plt.tight_layout()
    plt.xlabel('R [m]', fontsize = 14)
    plt.ylabel(f"z [m]", fontsize = 14)
    plt.axis('scaled')
    plt.tight_layout()
    plt.savefig(save_file)
    plt.show()

    

    #plt.hexbin()
    #RdYlGn_r


def main_zaus():
    pickle_file = './data_files/sca_diff_data_zaus.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE.csv'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = False                  #define if new dataset for picklefile shall be created
    rhop = [1.0]

    t_res = 0.1
    ref_shotnumber = 40263

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, rhop, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)

    #create start adn end times of constant plasma current for refernce shotnuber
    ipc_obj = utils.get_signal_objects(sf.SFREAD(ref_shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
    ref_ide_times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(scalar_data[ref_shotnumber]['EQH']['time'], scalar_data[ref_shotnumber]['IDE']['time'], start_time, end_time, t_res)

    ref_ide_idx = ide_time_idxs[int(len(ref_ide_times)/2)]

    ref_Rmag = scalar_data[ref_shotnumber]['IDE']['Rmag'][ref_ide_idx]
    ref_Zmag = scalar_data[ref_shotnumber]['IDE']['Zmag'][ref_ide_idx]
    ref_Raus = scalar_data[ref_shotnumber]['IDE']['Raus'][ref_ide_idx]
    #ref_Zaus = scalar_data[ref_shotnumber]['IDE']['Zaus'][int(len(ref_ide_times)/2)]
    ref_Rxpu = scalar_data[ref_shotnumber]['IDE']['Rxpu'][ref_ide_idx]
    ref_Zxpu = scalar_data[ref_shotnumber]['IDE']['Zxpu'][ref_ide_idx]
    

    diff_Rmag = []
    diff_Zmag = []
    diff_Raus = []
    diff_Zaus = []
    diff_Rxpu = []
    diff_Zxpu = []

    mag_dis = []
    aus_dis = []
    xpoi_dis = []

    #load EQU shotfile for R, z
    ide_equ = sf.EQU(ref_shotnumber, diag='IDE')

    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=ref_ide_times[int(len(ref_ide_times)/2)], coord_in='rho_pol')

    Zaus_idx = utils.find_idx_of_nearest(ide_r2[0][0], ref_Raus)
    ref_Zaus = ide_z2[0][0][Zaus_idx]

    plt.title(f"rhoTheta2rz contour @ {ref_ide_times[int(len(ref_ide_times)/2)]:.2f}s" )
    plt.plot(ide_r2[0][0], ide_z2[0][0], ls = 'dotted', label='IDE_rho2rz', c='b')

    #plot magnetic centers
    plt.plot(ref_Rmag, ref_Zmag, 'bx', label='IDE mag center')
    plt.plot(ref_Raus, ref_Zaus, 'kx', label='IDE Raus')
    plt.plot(ref_Rxpu, ref_Zxpu, 'rx', label='IDE X-Point')

    #plot the center axes
    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
    plt.axhline(y=ref_Zmag, color='blue', linestyle='--', linewidth=1)

    for i, shotnumber in enumerate(shotnumbers):

        if shotnumber in scalar_data.keys():

            #checks if the EQH and IDE are keys in dict and goes to next shot if not so
            if not 'EQH' in scalar_data[shotnumber].keys() or not 'IDE' in scalar_data[shotnumber].keys():
                print(f'EQH or IDE data in pickle dict for {shotnumber} was empty')
                continue

            eqh_scls = scalar_data[shotnumber]['EQH']
            ide_scls = scalar_data[shotnumber]['IDE']

            #checks if the dicts are empty and goes to next shot if so
            if not bool(eqh_scls) or not bool(ide_scls):
                print('dict of EQH or IDE for that shot was empty')
                continue

            #create start adn end times of constant plasma current
            ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
            start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
            if end_time - start_time < 1.0:
                print('discharge to short, prob not const Ip')
            
            else:
                times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(eqh_scls['time'], ide_scls['time'], start_time, end_time, t_res)
                
                for j, time in enumerate(times):
                    eqh_t_idx = eqh_time_idxs[j]
                    ide_t_idx = ide_time_idxs[j]

                    #'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu'...............  EQH-IDE
                    cur_diff_Rmag = sca_diff('Rmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zmag = sca_diff('Zmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Raus = sca_diff('Raus', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Rxpu = sca_diff('Rxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zxpu = sca_diff('Zxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)

                    cur_diff_Zaus = ide_scls['Zaus'][j] - eqh_scls['Zaus'][j]

                    cur_mag_dis = np.sqrt((cur_diff_Rmag)**2 + (cur_diff_Zmag)**2)
                    #cur_aus_dis = abs(cur_diff_Raus)
                    cur_aus_dis = np.sqrt((cur_diff_Raus)**2 + (cur_diff_Zaus)**2)
                    cur_xpoi_dis = np.sqrt((cur_diff_Rxpu)**2 + (cur_diff_Zxpu)**2)

                    scale = 5

                    diff_Rmag.append(cur_diff_Rmag)
                    diff_Zmag.append(cur_diff_Zmag)
                    diff_Raus.append(cur_diff_Raus)
                    diff_Zaus.append(cur_diff_Zaus)
                    diff_Rxpu.append(cur_diff_Rxpu)
                    diff_Zxpu.append(cur_diff_Zxpu)

                    """ diff_Rmag.append(scale * cur_diff_Rmag + ref_Rmag)
                    diff_Zmag.append(scale * cur_diff_Zmag + ref_Zmag)
                    diff_Raus.append(scale * cur_diff_Raus + ref_Raus)
                    diff_Rxpu.append(scale * cur_diff_Rxpu + ref_Rxpu)
                    diff_Zxpu.append(scale * cur_diff_Zxpu + ref_Zxpu) """

                    mag_dis.append(cur_mag_dis)
                    aus_dis.append(cur_aus_dis)
                    xpoi_dis.append(cur_xpoi_dis)

    #plt.plot(diff_Rmag, diff_Zmag, 'bx', label='IDE mag center')
    ref_Zaus_list = []
    for k, val in enumerate(diff_Raus):
        ref_Zaus_list.append(ref_Zaus)

    #converts list to np array and cuts of all the values above the percentage based on distance
    diff_Rmag, diff_Zmag, mag_dis = arr_threshold(diff_Rmag, diff_Zmag, mag_dis, percentage = 98)
    diff_Raus, diff_Zaus, aus_dis = arr_threshold(diff_Raus, diff_Zaus, aus_dis, percentage = 98)
    diff_Rxpu, diff_Zxpu, xpoi_dis = arr_threshold(diff_Rxpu, diff_Zxpu, xpoi_dis, percentage = 98)

    """ plt.scatter(scale * diff_Rmag + ref_Rmag, scale * diff_Zmag + ref_Zmag, c=mag_dis, cmap='plasma')
    plt.colorbar(label='Mag Difference (m)')
    plt.scatter(scale * diff_Raus + ref_Raus, ref_Zaus_list, c=aus_dis, cmap='plasma')
    plt.colorbar(label='Aus Difference (m)')
    plt.scatter(scale * diff_Rxpu + ref_Rxpu, scale * diff_Zxpu + ref_Zxpu, c=xpoi_dis, cmap='plasma')
    plt.colorbar(label='XPoi Difference (m)') """


    """ plt.scatter(diff_Rmag, diff_Zmag, c=mag_dis, cmap='plasma')
    plt.colorbar(label='Mag Difference (m)')
    plt.scatter(diff_Raus, ref_Zaus_list, c=aus_dis, cmap='plasma')
    plt.colorbar(label='Aus Difference (m)')
    plt.scatter(diff_Rxpu, diff_Zxpu, c=xpoi_dis, cmap='plasma')
    plt.colorbar(label='XPoi Difference (m)') """

    # Plot hexbins
    hb1 = plt.hexbin(scale * diff_Rmag + ref_Rmag, scale * diff_Zmag + ref_Zmag, bins = 'log', gridsize=50, cmap='plasma')
    cb1 = plt.colorbar(hb1, label='Mag Count')

    hb2 = plt.hexbin(scale * diff_Raus + ref_Raus, scale * diff_Zaus + ref_Zaus, bins = 'log', gridsize=50,  cmap='plasma')
    cb2 = plt.colorbar(hb2, label='Aus Count')

    hb3 = plt.hexbin(scale * diff_Rxpu + ref_Rxpu, scale * diff_Zxpu + ref_Zxpu, bins = 'log', gridsize=50, cmap='plasma')
    cb3 = plt.colorbar(hb3, label='XPoi Count') 

    plt.xlabel('R [m]', fontsize = 14)
    plt.ylabel("z [m]", fontsize = 14)
    plt.axis('scaled')
    plt.savefig('figs/cont_heat/cont_heat_zaus.png')
    plt.show()

    #RdYlGn_r






def new_data():
    pickle_file = './data_files/sca_diff_data_zaus.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE.csv'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = True                  #define if new dataset for picklefile shall be created
    rhop = [1.0]

    t_res = 0.1
    ref_shotnumber = 40263

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, rhop, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)    

    


if __name__ == "__main__":
    main_norm()