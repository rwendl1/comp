import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button
import numpy as np
import math
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils
import pickle
import privmapeq
import pandas as pd
import seaborn as sns
from scipy.spatial import ConvexHull
from scipy.stats import gaussian_kde
from mpl_toolkits.axes_grid1 import make_axes_locatable

def populate_data(data, shotnumber, rhop, t_res, output_file):

    diagnostic = ['EQH', 'IDE']
    diagnostic_g = ['GQH', 'IDG']

    prev_data = data

    data[shotnumber]={}

    #create start adn end times of constant plasma current
    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)

    data[shotnumber]={
        'IpiFP': ipc_obj['IpiFP'],
        'IpiFP_TB': ipc_obj['IpiFP_TB'],
        'start_time': start_time,
        'end_time': end_time,
    }
    
    #populate different dias with scalarvalues
    for g, dia in enumerate(diagnostic):

        data[shotnumber][dia]={}

        Geq = sf.SFREAD(shotnumber, diagnostic_g[g])  # Check if EQI shotfile exists
        if Geq.status:
            signal_objects = utils.get_signal_objects(Geq, 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu')
            timebase = Geq.getobject('TIMEF')

            data[shotnumber][dia]={
                **signal_objects,
                'time': timebase
            }
        else:
            print(f'{diagnostic_g[g]} not found')
            return prev_data

    times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(data[shotnumber]['EQH']['time'], data[shotnumber]['IDE']['time'], start_time, end_time, t_res)

    #load EQU shotfile for R, z
    eqh_equ = sf.EQU(shotnumber, diag='EQH')
    ide_equ = sf.EQU(shotnumber, diag='IDE')


    zaus_eqh_list = []
    zaus_ide_list = []
    for j, time in enumerate(times):

        eqh_idx = eqh_time_idxs[j]
        ide_idx = ide_time_idxs[j]

        eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=time, coord_in='rho_pol')
        ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=time, coord_in='rho_pol')

        #calculates the zaus from contour
        zaus_idx_eqh = utils.find_idx_of_nearest(eqh_r2[0][0], data[shotnumber]['EQH']['Raus'][eqh_idx])
        zaus_idx_ide = utils.find_idx_of_nearest(ide_r2[0][0], data[shotnumber]['IDE']['Raus'][ide_idx])

        zaus_eqh = eqh_z2[0][0][zaus_idx_eqh]
        zaus_ide = ide_z2[0][0][zaus_idx_ide]

        zaus_eqh_list.append(zaus_eqh)
        zaus_ide_list.append(zaus_ide)

    data[shotnumber]['EQH']['Zaus'] = zaus_eqh_list
    data[shotnumber]['IDE']['Zaus'] = zaus_ide_list
    data[shotnumber]['IDE']['Zaus_times'] = times


    # Save data to a file
    with open(output_file, 'wb') as f:
        pickle.dump(data, f)   

    # Load data from file to reset it
    with open(output_file, 'rb') as f:
        data = pickle.load(f)

    return data

def plot_hexbin_contour(hb, percentage):
    # Extract the data from the hexbin object
    counts = hb.get_array()
    verts = hb.get_offsets()

    # Create the data arrays from the hexbin vertices
    x = verts[:, 0]
    y = verts[:, 1]

    # Calculate the kernel density estimate
    values = np.vstack([x, y])
    kde = gaussian_kde(values)
    density = kde(values)

    # Create a grid of points and calculate their densities
    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()
    xx, yy = np.meshgrid(np.linspace(xmin, xmax, 100), np.linspace(ymin, ymax, 100))
    grid_points = np.vstack([xx.ravel(), yy.ravel()])
    grid_density = kde(grid_points).reshape(xx.shape)

    # Sort the grid densities and find the density threshold for the given percentile
    sorted_density = np.sort(density)
    cumulative_density = np.cumsum(sorted_density)
    density_threshold = np.percentile(density, 100 - percentage)

    # Draw the contour at the threshold density
    contour = plt.contour(xx, yy, grid_density, levels=[density_threshold], colors='red')
    contour.collections[0].set_label(f'{percentage}% contour')


def calculate_contour_points(x_values, y_values, distances, percentage):
    # Step 1: Sort distances and find threshold
    sorted_distances = np.sort(distances)
    threshold_index = int(len(sorted_distances) * (percentage / 100))
    distance_threshold = sorted_distances[threshold_index]

    # Step 2: Filter x and y arrays based on the distance threshold
    within_threshold_indices = distances <= distance_threshold
    filtered_x = np.array(x_values)[within_threshold_indices]
    filtered_y = np.array(y_values)[within_threshold_indices]

    # Step 3: Calculate contour points using Convex Hull for simplicity
    points = np.vstack((filtered_x, filtered_y)).T  # Combine x and y for Convex Hull
    if len(points) < 3:
        raise ValueError("Not enough points to form a contour.")
    hull = ConvexHull(points)
    contour_x = points[hull.vertices, 0]
    contour_y = points[hull.vertices, 1]

    # Step 4: Return the contour points
    return contour_x, contour_y

def plot_contour_from_data(x, y, percentage, write_level=False):
    # Calculate the kernel density estimate
    percentage = 100 - percentage
    values = np.vstack([x, y])
    kde = gaussian_kde(values)
    density = kde(values)

    # Create a grid of points and calculate their densities
    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()
    xx, yy = np.meshgrid(np.linspace(xmin, xmax, 100), np.linspace(ymin, ymax, 100))
    grid_points = np.vstack([xx.ravel(), yy.ravel()])
    grid_density = kde(grid_points).reshape(xx.shape)

    # Sort the densities and find the threshold for the given percentile
    sorted_density = np.sort(density)
    cumulative_density = np.cumsum(sorted_density)
    threshold_density = sorted_density[np.searchsorted(cumulative_density, percentage * 0.01 * cumulative_density[-1])]

    # Draw the contour at the threshold density
    contour = plt.contour(xx, yy, grid_density, levels=[threshold_density], colors='red')
    #contour.collections[0].set_label(f'{percentage}% contour')

    # Add contour labels
    if write_level:
        plt.clabel(contour, contour.levels, inline=True, fmt=dict(zip([threshold_density], [f'{percentage}%'])), fontsize=10)

def filter_points_edges_based_on_distance(raus_values, distances, percentage):
    # Sort distances and find threshold
    sorted_distances = np.sort(distances)
    threshold_index = int(len(sorted_distances) * (percentage / 100))
    distance_threshold = sorted_distances[threshold_index]

    # Filter the Raus array based on the distance threshold
    within_threshold_indices = distances <= distance_threshold
    filtered_raus = np.array(raus_values)[within_threshold_indices]

    # Return the edge values (min and max) of the filtered points
    if len(filtered_raus) > 0:
        return np.min(filtered_raus), np.max(filtered_raus)
    else:
        return None 

def sca_diff(scalar, IDE, EQH,  ide_t_idx, eqh_t_idx):
    return EQH[scalar][eqh_t_idx] - IDE[scalar][ide_t_idx]

def arr_threshold(arr1, arr2, arr_ref, percentage = 95):

    arr1 = np.array(arr1)
    arr2 = np.array(arr2)
    arr_ref = np.array(arr_ref)

    threshold = np.percentile(arr_ref, percentage)

    # Create a mask for values below the threshold
    mask = arr_ref <= threshold

    # Filter the lists using the mask
    arr1 = arr1[mask]
    arr2 = arr2[mask]
    arr_ref = arr_ref[mask]

    return arr1, arr2, arr_ref


def main_norm():
    pickle_file = './data_files/sca_diff_data_zaus.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE.csv'
    save_file = 'figs/cont_heat/cont_heat_norm_scaled.png'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = False                  #define if new dataset for picklefile shall be created
    closeup = False
    closeup_cb = True
    rhop = [1.0]

    t_res = 0.1
    ref_shotnumber = 40263
    scale = 1     #1/8
    unit = 1000 #mm conversion

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, rhop, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)

    #create start adn end times of constant plasma current for refernce shotnuber
    ipc_obj = utils.get_signal_objects(sf.SFREAD(ref_shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
    ref_ide_times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(scalar_data[ref_shotnumber]['EQH']['time'], scalar_data[ref_shotnumber]['IDE']['time'], start_time, end_time, t_res)

    ref_ide_idx = ide_time_idxs[int(len(ref_ide_times)/2)]

    ref_Rmag = scalar_data[ref_shotnumber]['IDE']['Rmag'][ref_ide_idx]
    ref_Zmag = scalar_data[ref_shotnumber]['IDE']['Zmag'][ref_ide_idx]
    ref_Raus = scalar_data[ref_shotnumber]['IDE']['Raus'][ref_ide_idx]
    ref_Rxpu = scalar_data[ref_shotnumber]['IDE']['Rxpu'][ref_ide_idx]
    ref_Zxpu = scalar_data[ref_shotnumber]['IDE']['Zxpu'][ref_ide_idx]

    diff_Rmag = []
    diff_Zmag = []
    diff_Raus = []
    diff_Rxpu = []
    diff_Zxpu = []

    mag_dis = []
    aus_dis = []
    xpoi_dis = []

    #load EQU shotfile for R, z
    ide_equ = sf.EQU(ref_shotnumber, diag='IDE')

    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=ref_ide_times[int(len(ref_ide_times)/2)], coord_in='rho_pol')

    Zaus_idx = utils.find_idx_of_nearest(ide_r2[0][0], ref_Raus)
    ref_Zaus = ide_z2[0][0][Zaus_idx]

    if scale == 1:
        title_text = f"Histogram of scalar differences over contour"
        save_file = 'figs/cont_heat/cont_heat_norm_unscaled.png'
    else:
        title_text = f"scaled Histogram of scalar differences over contour" 

    plt.figure(1, figsize=(8, 6))
    plt.title(title_text, fontsize = 14)
    plt.plot(unit*scale * ide_r2[0][0], unit*scale * ide_z2[0][0], ls = 'dotted', label=f'ref. contour (not to scale)', c='b')
    #plt.legend(loc='center left', bbox_to_anchor=(0, -0.115))
    #plt.legend(loc='best')

    #plot magnetic centers
    plt.plot(unit*scale * ref_Rmag, unit* scale * ref_Zmag, 'rx', label='IDE mag center')
    plt.plot(unit*scale * ref_Raus, unit* scale * ref_Zaus, 'rx', label='IDE Raus')
    plt.plot(unit*scale * ref_Rxpu, unit* scale * ref_Zxpu, 'rx', label='IDE X-Point')

    #plot the center axes
    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
    plt.axhline(y=unit*scale * ref_Zmag, color='blue', linestyle='--', linewidth=1)

    for i, shotnumber in enumerate(shotnumbers):

        if shotnumber in scalar_data.keys():

            #checks if the EQH and IDE are keys in dict and goes to next shot if not so
            if not 'EQH' in scalar_data[shotnumber].keys() or not 'IDE' in scalar_data[shotnumber].keys():
                print(f'EQH or IDE data in pickle dict for {shotnumber} was empty')
                continue

            eqh_scls = scalar_data[shotnumber]['EQH']
            ide_scls = scalar_data[shotnumber]['IDE']

            #checks if the dicts are empty and goes to next shot if so
            if not bool(eqh_scls) or not bool(ide_scls):
                print('dict of EQH or IDE for that shot was empty')
                continue

            #create start adn end times of constant plasma current
            ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
            start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
            if end_time - start_time < 1.0:
                print('discharge to short, prob not const Ip')
            
            else:
                times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(eqh_scls['time'], ide_scls['time'], start_time, end_time, t_res)
                
                for j, time in enumerate(times):
                    eqh_t_idx = eqh_time_idxs[j]
                    ide_t_idx = ide_time_idxs[j]

                    #'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu'...............  EQH-IDE
                    cur_diff_Rmag = sca_diff('Rmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zmag = sca_diff('Zmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Raus = sca_diff('Raus', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Rxpu = sca_diff('Rxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zxpu = sca_diff('Zxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)

                    cur_mag_dis = np.sqrt((cur_diff_Rmag)**2 + (cur_diff_Zmag)**2)
                    cur_aus_dis = abs(cur_diff_Raus)
                    cur_xpoi_dis = np.sqrt((cur_diff_Rxpu)**2 + (cur_diff_Zxpu)**2)

                    diff_Rmag.append(cur_diff_Rmag)
                    diff_Zmag.append(cur_diff_Zmag)
                    diff_Raus.append(cur_diff_Raus)
                    diff_Rxpu.append(cur_diff_Rxpu)
                    diff_Zxpu.append(cur_diff_Zxpu)

                    mag_dis.append(cur_mag_dis)
                    aus_dis.append(cur_aus_dis)
                    xpoi_dis.append(cur_xpoi_dis)


    #plt.plot(diff_Rmag, diff_Zmag, 'bx', label='IDE mag center')
    ref_Zaus_list = []
    for k, val in enumerate(diff_Raus):
        ref_Zaus_list.append(ref_Zaus)
    ref_Zaus_list = np.array(ref_Zaus_list)
    
    #converts list to np array and cuts of all the values above the percentage based on distance
    diff_Rmag, diff_Zmag, mag_dis = arr_threshold(diff_Rmag, diff_Zmag, mag_dis, percentage = 98)
    diff_Raus, ref_Zaus_list, aus_dis = arr_threshold(diff_Raus, ref_Zaus_list, aus_dis, percentage = 98)
    diff_Rxpu, diff_Zxpu, xpoi_dis = arr_threshold(diff_Rxpu, diff_Zxpu, xpoi_dis, percentage = 98)

    # Plot hexbins
    hb2 = plt.hexbin(unit*diff_Raus + unit*scale * ref_Raus, unit*scale *ref_Zaus_list, bins = 'log', gridsize=50,  cmap='plasma')
    cb2 = plt.colorbar(hb2, label='R_aus Count')
    cb2.ax.tick_params(labelsize=14) 

    hb3 = plt.hexbin(unit*diff_Rxpu + unit*scale * ref_Rxpu, unit*diff_Zxpu + unit*scale * ref_Zxpu, bins = 'log', gridsize=50, cmap='plasma')
    cb3 = plt.colorbar(hb3, label='X-Point Count')
    cb3.ax.tick_params(labelsize=14) 

    hb1 = plt.hexbin(unit*diff_Rmag + unit*scale * ref_Rmag, unit*diff_Zmag + unit*scale * ref_Zmag, bins = 'log', gridsize=50, cmap='plasma')
    cb1 = plt.colorbar(hb1, label='Magnetic Center Count')
    cb1.ax.tick_params(labelsize=14) 
    hb1.figure.axes[0].tick_params(axis='both', labelsize = 14)
    
    plt.tight_layout()
    plt.xlabel('R [mm]', fontsize = 14)
    plt.ylabel(f"z [mm]", fontsize = 14)
    plt.axis('scaled')
    plt.tight_layout()
    plt.savefig(save_file)
    #plt.show()

    if closeup:
        percentage = 25
        con_Rmag, con_Zmag = calculate_contour_points(diff_Rmag, diff_Zmag, mag_dis, percentage = percentage)
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage = percentage)
        con_Rxpu, con_Zxpu = calculate_contour_points(diff_Rxpu, diff_Zxpu, xpoi_dis, percentage = percentage)

        plt.close()
        plt.figure( figsize=(4, 4))
        plt.title('Raus', fontsize = 14)
        hb2 = plt.hexbin(unit * diff_Raus, unit * ref_Zaus_list, bins = 'log', gridsize=50,  cmap='plasma')
        
        plt.plot(0, unit*ref_Zaus, 'rx', label='Ref. Raus')
        perc=25
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage = perc)
        plt.axvline(x = unit * min_zaus, linestyle='--', c='r', label=f'{perc}th percentile', alpha = (100-perc)/100)
        plt.axvline(x = unit * max_zaus, linestyle='--', c='r', label=f'{perc}th percentile', alpha = (100-perc)/100)
        plt.text(unit * (max_zaus)+0.2, 105, f'{perc}%', c= 'r',rotation=90, verticalalignment='bottom', horizontalalignment='left')
        perc=50
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage = perc)
        plt.axvline(x = unit * min_zaus, linestyle='--', c='b', label=f'{perc}th percentile', alpha = (100-perc)/100)
        plt.axvline(x = unit * max_zaus, linestyle='--', c='b', label=f'{perc}th percentile', alpha = (100-perc)/100)
        plt.text(unit * (max_zaus)+0.2, 105, f'{perc}%', c= 'b',rotation=90, verticalalignment='bottom', horizontalalignment='left')
        perc=75
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage = perc)
        plt.axvline(x = unit * min_zaus, linestyle='--', c='g', label=f'{perc}th percentile', alpha = (100-perc)/100)
        plt.axvline(x = unit * max_zaus, linestyle='--', c='g', label=f'{perc}th percentile', alpha = (100-perc)/100)
        plt.text(unit * (max_zaus)+0.2, 105, f'{perc}%', c= 'g',rotation=90, verticalalignment='bottom', horizontalalignment='left')

        #cb2 = plt.colorbar(hb2, label='R_aus Count')
        plt.xlabel(r'$\Delta R$ [mm]', fontsize = 14)
        plt.ylabel(f"z [mm]", fontsize = 14)
        plt.axis('scaled')
        #plt.legend()
        plt.tight_layout()
        plt.savefig('figs/cont_heat/closeup_raus.png')
        plt.close()

        plt.close()
        plt.figure(figsize=(4, 4))
        plt.title('X-Point', fontsize = 14)
        hb3 = plt.hexbin(unit*diff_Rxpu, unit*diff_Zxpu, bins = 'log', gridsize=50, cmap='plasma')
        plt.plot(0, 0, 'rx', label='Ref. X-Point')
        #plt.plot(unit * con_Rxpu, unit * con_Zxpu, ls = 'dotted', label=f'{percentage}th percentile', c='red')
        #plot_hexbin_contour(hb3, percentage)
        plot_contour_from_data(unit * diff_Rxpu, unit * diff_Zxpu, 25)
        plot_contour_from_data(unit * diff_Rxpu, unit * diff_Zxpu, 50)
        plot_contour_from_data(unit * diff_Rxpu, unit * diff_Zxpu, 75)
        #cb3 = plt.colorbar(hb3, label='X-Point Count')
        plt.xlabel(r'$\Delta R$ [mm]', fontsize = 14)
        plt.ylabel(r'$\Delta z$ [mm]', fontsize = 14)
        plt.axis('scaled')
        #plt.legend()
        plt.tight_layout()
        plt.savefig('figs/cont_heat/closeup_rxp.png')
        plt.close()

        plt.close()
        plt.figure( figsize=(4, 4))
        plt.title('Magnetic Center', fontsize = 14)
        hb1 = plt.hexbin(unit*diff_Rmag, unit*diff_Zmag, bins = 'log', gridsize=50, cmap='plasma')
        plt.plot(0, 0, 'rx', label='Ref. mag center')
        #plt.plot(unit * con_Rmag, unit * con_Zmag, ls = 'dotted', label=f'{percentage}th percentile', c='red')
        #plot_hexbin_contour(hb1, percentage)
        plot_contour_from_data(unit * diff_Rmag, unit * diff_Zmag, 25)
        plot_contour_from_data(unit * diff_Rmag, unit * diff_Zmag, 50)
        plot_contour_from_data(unit * diff_Rmag, unit * diff_Zmag, 75)
        #cb1 = plt.colorbar(hb1, label='Magnetic Center Count')
        plt.xlabel(r'$\Delta R$ [mm]', fontsize = 14)
        plt.ylabel(r'$\Delta z$ [mm]', fontsize = 14)
        plt.axis('scaled')
        #plt.legend()
        plt.tight_layout()
        plt.savefig('figs/cont_heat/closeup_rmag.png')
        plt.close()

    if closeup_cb:

        # Raus Plot
        plt.figure(figsize=(4, 4))
        plt.title('Raus', fontsize=14)
        hb2 = plt.hexbin(unit * diff_Raus, unit * ref_Zaus_list, bins='log', gridsize=50, cmap='plasma')

        plt.plot(0, unit * ref_Zaus, 'rx', label='Ref. Raus')

        perc = 25
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage=perc)
        plt.axvline(x=unit * min_zaus, linestyle='--', c='r', label=f'{perc}th percentile', alpha=(100 - perc) / 100)
        plt.axvline(x=unit * max_zaus, linestyle='--', c='r', label=f'{perc}th percentile', alpha=(100 - perc) / 100)
        plt.text(unit * (max_zaus) + 0.2, 105, f'{perc}%', c='r', rotation=90, verticalalignment='bottom', horizontalalignment='left')

        perc = 50
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage=perc)
        plt.axvline(x=unit * min_zaus, linestyle='--', c='b', label=f'{perc}th percentile', alpha=(100 - perc) / 100)
        plt.axvline(x=unit * max_zaus, linestyle='--', c='b', label=f'{perc}th percentile', alpha=(100 - perc) / 100)
        plt.text(unit * (max_zaus) + 0.2, 105, f'{perc}%', c='b', rotation=90, verticalalignment='bottom', horizontalalignment='left')

        perc = 75
        min_zaus, max_zaus = filter_points_edges_based_on_distance(diff_Raus, aus_dis, percentage=perc)
        plt.axvline(x=unit * min_zaus, linestyle='--', c='g', label=f'{perc}th percentile', alpha=(100 - perc) / 100)
        plt.axvline(x=unit * max_zaus, linestyle='--', c='g', label=f'{perc}th percentile', alpha=(100 - perc) / 100)
        plt.text(unit * (max_zaus) + 0.2, 105, f'{perc}%', c='g', rotation=90, verticalalignment='bottom', horizontalalignment='left')

        plt.xlabel(r'$\Delta R$ [mm]', fontsize=14)
        plt.ylabel(f"z [mm]", fontsize=14)
        plt.axis('scaled')

        # Adjust colorbar height
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cb2 = plt.colorbar(hb2, cax=cax)
        cb2.set_label('R_aus Count')

        plt.tight_layout()
        plt.savefig('figs/cont_heat/closeup_raus_cb.png')
        plt.close()

        # X-Point Plot
        plt.figure(figsize=(4, 4))
        plt.title('X-Point', fontsize=14)
        hb3 = plt.hexbin(unit * diff_Rxpu, unit * diff_Zxpu, bins='log', gridsize=50, cmap='plasma')
        plt.plot(0, 0, 'rx', label='Ref. X-Point')

        plot_contour_from_data(unit * diff_Rxpu, unit * diff_Zxpu, 25)
        plot_contour_from_data(unit * diff_Rxpu, unit * diff_Zxpu, 50)
        plot_contour_from_data(unit * diff_Rxpu, unit * diff_Zxpu, 75)

        plt.xlabel(r'$\Delta R$ [mm]', fontsize=14)
        plt.ylabel(r'$\Delta z$ [mm]', fontsize=14)
        plt.axis('scaled')

        # Adjust colorbar height
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cb3 = plt.colorbar(hb3, cax=cax)
        cb3.set_label('X-Point Count')

        plt.tight_layout()
        plt.savefig('figs/cont_heat/closeup_rxpu_cb.png')
        plt.close()

        # Magnetic Center Plot
        plt.figure(figsize=(4, 4))
        plt.title('Magnetic Center', fontsize=14)
        hb1 = plt.hexbin(unit * diff_Rmag, unit * diff_Zmag, bins='log', gridsize=50, cmap='plasma')
        plt.plot(0, 0, 'rx', label='Ref. mag center')

        plot_contour_from_data(unit * diff_Rmag, unit * diff_Zmag, 25)
        plot_contour_from_data(unit * diff_Rmag, unit * diff_Zmag, 50)
        plot_contour_from_data(unit * diff_Rmag, unit * diff_Zmag, 75)

        plt.xlabel(r'$\Delta R$ [mm]', fontsize=14)
        plt.ylabel(r'$\Delta z$ [mm]', fontsize=14)
        plt.axis('scaled')

        # Adjust colorbar height
        divider = make_axes_locatable(plt.gca())
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cb1 = plt.colorbar(hb1, cax=cax)
        cb1.set_label('Magnetic Center Count')

        plt.tight_layout()
        plt.savefig('figs/cont_heat/closeup_rmag_cb.png')
        plt.close()

    

    




    #plt.hexbin()
    #RdYlGn_r


def main_zaus():
    pickle_file = './data_files/sca_diff_data_zaus.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE.csv'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = False                  #define if new dataset for picklefile shall be created
    rhop = [1.0]

    t_res = 0.1
    ref_shotnumber = 40263

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, rhop, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)

    #create start adn end times of constant plasma current for refernce shotnuber
    ipc_obj = utils.get_signal_objects(sf.SFREAD(ref_shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
    ref_ide_times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(scalar_data[ref_shotnumber]['EQH']['time'], scalar_data[ref_shotnumber]['IDE']['time'], start_time, end_time, t_res)

    ref_ide_idx = ide_time_idxs[int(len(ref_ide_times)/2)]

    ref_Rmag = scalar_data[ref_shotnumber]['IDE']['Rmag'][ref_ide_idx]
    ref_Zmag = scalar_data[ref_shotnumber]['IDE']['Zmag'][ref_ide_idx]
    ref_Raus = scalar_data[ref_shotnumber]['IDE']['Raus'][ref_ide_idx]
    #ref_Zaus = scalar_data[ref_shotnumber]['IDE']['Zaus'][int(len(ref_ide_times)/2)]
    ref_Rxpu = scalar_data[ref_shotnumber]['IDE']['Rxpu'][ref_ide_idx]
    ref_Zxpu = scalar_data[ref_shotnumber]['IDE']['Zxpu'][ref_ide_idx]
    

    diff_Rmag = []
    diff_Zmag = []
    diff_Raus = []
    diff_Zaus = []
    diff_Rxpu = []
    diff_Zxpu = []

    mag_dis = []
    aus_dis = []
    xpoi_dis = []

    #load EQU shotfile for R, z
    ide_equ = sf.EQU(ref_shotnumber, diag='IDE')

    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=ref_ide_times[int(len(ref_ide_times)/2)], coord_in='rho_pol')

    Zaus_idx = utils.find_idx_of_nearest(ide_r2[0][0], ref_Raus)
    ref_Zaus = ide_z2[0][0][Zaus_idx]

    plt.title(f"rhoTheta2rz contour @ {ref_ide_times[int(len(ref_ide_times)/2)]:.2f}s" )
    plt.plot(ide_r2[0][0], ide_z2[0][0], ls = 'dotted', label='IDE_rho2rz', c='b')

    #plot magnetic centers
    plt.plot(ref_Rmag, ref_Zmag, 'bx', label='IDE mag center')
    plt.plot(ref_Raus, ref_Zaus, 'kx', label='IDE Raus')
    plt.plot(ref_Rxpu, ref_Zxpu, 'rx', label='IDE X-Point')

    #plot the center axes
    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
    plt.axhline(y=ref_Zmag, color='blue', linestyle='--', linewidth=1)

    for i, shotnumber in enumerate(shotnumbers):

        if shotnumber in scalar_data.keys():

            #checks if the EQH and IDE are keys in dict and goes to next shot if not so
            if not 'EQH' in scalar_data[shotnumber].keys() or not 'IDE' in scalar_data[shotnumber].keys():
                print(f'EQH or IDE data in pickle dict for {shotnumber} was empty')
                continue

            eqh_scls = scalar_data[shotnumber]['EQH']
            ide_scls = scalar_data[shotnumber]['IDE']

            #checks if the dicts are empty and goes to next shot if so
            if not bool(eqh_scls) or not bool(ide_scls):
                print('dict of EQH or IDE for that shot was empty')
                continue

            #create start adn end times of constant plasma current
            ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
            start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
            if end_time - start_time < 1.0:
                print('discharge to short, prob not const Ip')
            
            else:
                times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(eqh_scls['time'], ide_scls['time'], start_time, end_time, t_res)
                
                for j, time in enumerate(times):
                    eqh_t_idx = eqh_time_idxs[j]
                    ide_t_idx = ide_time_idxs[j]

                    #'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu'...............  EQH-IDE
                    cur_diff_Rmag = sca_diff('Rmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zmag = sca_diff('Zmag', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Raus = sca_diff('Raus', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Rxpu = sca_diff('Rxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)
                    cur_diff_Zxpu = sca_diff('Zxpu', ide_scls, eqh_scls, ide_t_idx, eqh_t_idx)

                    cur_diff_Zaus = ide_scls['Zaus'][j] - eqh_scls['Zaus'][j]

                    cur_mag_dis = np.sqrt((cur_diff_Rmag)**2 + (cur_diff_Zmag)**2)
                    #cur_aus_dis = abs(cur_diff_Raus)
                    cur_aus_dis = np.sqrt((cur_diff_Raus)**2 + (cur_diff_Zaus)**2)
                    cur_xpoi_dis = np.sqrt((cur_diff_Rxpu)**2 + (cur_diff_Zxpu)**2)

                    scale = 5

                    diff_Rmag.append(cur_diff_Rmag)
                    diff_Zmag.append(cur_diff_Zmag)
                    diff_Raus.append(cur_diff_Raus)
                    diff_Zaus.append(cur_diff_Zaus)
                    diff_Rxpu.append(cur_diff_Rxpu)
                    diff_Zxpu.append(cur_diff_Zxpu)

                    """ diff_Rmag.append(scale * cur_diff_Rmag + ref_Rmag)
                    diff_Zmag.append(scale * cur_diff_Zmag + ref_Zmag)
                    diff_Raus.append(scale * cur_diff_Raus + ref_Raus)
                    diff_Rxpu.append(scale * cur_diff_Rxpu + ref_Rxpu)
                    diff_Zxpu.append(scale * cur_diff_Zxpu + ref_Zxpu) """

                    mag_dis.append(cur_mag_dis)
                    aus_dis.append(cur_aus_dis)
                    xpoi_dis.append(cur_xpoi_dis)

    #plt.plot(diff_Rmag, diff_Zmag, 'bx', label='IDE mag center')
    ref_Zaus_list = []
    for k, val in enumerate(diff_Raus):
        ref_Zaus_list.append(ref_Zaus)

    #converts list to np array and cuts of all the values above the percentage based on distance
    diff_Rmag, diff_Zmag, mag_dis = arr_threshold(diff_Rmag, diff_Zmag, mag_dis, percentage = 98)
    diff_Raus, diff_Zaus, aus_dis = arr_threshold(diff_Raus, diff_Zaus, aus_dis, percentage = 98)
    diff_Rxpu, diff_Zxpu, xpoi_dis = arr_threshold(diff_Rxpu, diff_Zxpu, xpoi_dis, percentage = 98)

    """ plt.scatter(scale * diff_Rmag + ref_Rmag, scale * diff_Zmag + ref_Zmag, c=mag_dis, cmap='plasma')
    plt.colorbar(label='Mag Difference (m)')
    plt.scatter(scale * diff_Raus + ref_Raus, ref_Zaus_list, c=aus_dis, cmap='plasma')
    plt.colorbar(label='Aus Difference (m)')
    plt.scatter(scale * diff_Rxpu + ref_Rxpu, scale * diff_Zxpu + ref_Zxpu, c=xpoi_dis, cmap='plasma')
    plt.colorbar(label='XPoi Difference (m)') """


    """ plt.scatter(diff_Rmag, diff_Zmag, c=mag_dis, cmap='plasma')
    plt.colorbar(label='Mag Difference (m)')
    plt.scatter(diff_Raus, ref_Zaus_list, c=aus_dis, cmap='plasma')
    plt.colorbar(label='Aus Difference (m)')
    plt.scatter(diff_Rxpu, diff_Zxpu, c=xpoi_dis, cmap='plasma')
    plt.colorbar(label='XPoi Difference (m)') """

    # Plot hexbins
    hb1 = plt.hexbin(scale * diff_Rmag + ref_Rmag, scale * diff_Zmag + ref_Zmag, bins = 'log', gridsize=50, cmap='plasma')
    cb1 = plt.colorbar(hb1, label='Mag Count')

    hb2 = plt.hexbin(scale * diff_Raus + ref_Raus, scale * diff_Zaus + ref_Zaus, bins = 'log', gridsize=50,  cmap='plasma')
    cb2 = plt.colorbar(hb2, label='Aus Count')

    hb3 = plt.hexbin(scale * diff_Rxpu + ref_Rxpu, scale * diff_Zxpu + ref_Zxpu, bins = 'log', gridsize=50, cmap='plasma')
    cb3 = plt.colorbar(hb3, label='XPoi Count') 

    plt.xlabel('R [m]', fontsize = 14)
    plt.ylabel("z [m]", fontsize = 14)
    plt.axis('scaled')
    plt.savefig('figs/cont_heat/cont_heat_zaus.png')
    plt.show()

    #RdYlGn_r






def new_data():
    pickle_file = './data_files/sca_diff_data_zaus.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE.csv'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = True                  #define if new dataset for picklefile shall be created
    rhop = [1.0]

    t_res = 0.1
    ref_shotnumber = 40263

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, rhop, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)    

    


if __name__ == "__main__":
    main_norm()