import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button
import numpy as np
import math
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils
import pickle
import privmapeq
import pandas as pd
import seaborn as sns

def main():

    att = '_t001'

    pickle_file = './data_files/Wmhd_heat'+ att + '.pickle'
    shotlist_file = './shotlist.txt'
    ide_list_file = 'checkHeat/checkIDE.csv'
    png_save_file = 'figs/Wmhd/Wmhd_heat'+ att + '.png'

    
    from_Shotlist = False               #define if form shotlist.txt or based on all available IDE
    gen_new_pickle = False                  #define if new dataset for picklefile shall be created

    t_res = 0.02

    new_data(pickle_file, shotlist_file, ide_list_file, t_res, from_Shotlist, gen_new_pickle)

    plot_data(pickle_file, png_save_file)

def populate_data(data, shotnumber, t_res, output_file):

    diagnostic = ['EQH', 'IDE']
    diagnostic_g = ['GQH', 'IDG']

    prev_data = data

    data[shotnumber]={}

    #create start adn end times of constant plasma current
    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)

    data[shotnumber]={
        'IpiFP': ipc_obj['IpiFP'],
        'IpiFP_TB': ipc_obj['IpiFP_TB'],
        'start_time': start_time,
        'end_time': end_time,
    }
    
    #populate different dias with scalarvalues
    for g, dia in enumerate(diagnostic):

        data[shotnumber][dia]={}

        Geq = sf.SFREAD(shotnumber, diagnostic_g[g])  # Check if EQI shotfile exists
        if Geq.status:
            signal_objects = utils.get_signal_objects(Geq, 'Wmhd')
            timebase = Geq.getobject('TIMEF')

            data[shotnumber][dia]={
                **signal_objects,
                'time': timebase
            }
        else:
            print(f'{diagnostic_g[g]} not found')
            return prev_data

    times, eqh_time_idxs, ide_time_idxs = utils.create_timestamps(data[shotnumber]['EQH']['time'], data[shotnumber]['IDE']['time'], start_time, end_time, t_res)

    ratio_Wmhd_list = []
    for j, time in enumerate(times):

        ratio_Wmhd = data[shotnumber]['EQH']['Wmhd'][eqh_time_idxs[j]] / data[shotnumber]['IDE']['Wmhd'][ide_time_idxs[j]]
        ratio_Wmhd_list.append(ratio_Wmhd)

    data[shotnumber]['Wmhd_ratio'] = ratio_Wmhd_list
    data[shotnumber]['Wmhd_times'] = times


    # Save data to a file
    with open(output_file, 'wb') as f:
        pickle.dump(data, f)   

    # Load data from file to reset it
    with open(output_file, 'rb') as f:
        data = pickle.load(f)

    return data

def plot_data(pickle_file, png_save_file):

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)

    shotnumbers = list(scalar_data.keys())

    Wmhds = np.array([])
    times = np.array([])

    for i, shotnumber in enumerate(shotnumbers):

        if shotnumber in scalar_data.keys():

            #checks if the EQH and IDE are keys in dict and goes to next shot if not so
            if not 'EQH' in scalar_data[shotnumber].keys() or not 'IDE' in scalar_data[shotnumber].keys():
                print(f'EQH or IDE data in pickle dict for {shotnumber} was empty')
                continue

            #checks if the dicts are empty and goes to next shot if so
            if not bool(scalar_data[shotnumber]['EQH']) or not bool(scalar_data[shotnumber]['IDE']):
                print('dict of EQH or IDE for that shot was empty')
                continue

            #calculate Wmhd ratio and concatenate
            cur_ratio_Wmhd = scalar_data[shotnumber]['Wmhd_ratio']
            time = scalar_data[shotnumber]['Wmhd_times']
            Wmhds = np.concatenate((Wmhds, cur_ratio_Wmhd))
            times = np.concatenate((times, time))



    unit = 1000
    scale = 1

    
    # Plot hexbins
    hb1 = plt.hexbin(times, Wmhds*100, bins = 'log', gridsize=100,  cmap='plasma')
    #hb1 = plt.hexbin(times, Wmhds*100, gridsize=100,  cmap='plasma')
    cb1 = plt.colorbar(hb1, label='Wmhd')
    cb1.ax.tick_params(labelsize=14) 

    plt.axhline(y=100, color='red', linestyle='--', linewidth=1, alpha=0.5)
    
    #plt.tight_layout()
    plt.xlabel('time [s]', fontsize = 14)
    plt.ylabel(f"Wmhd ratio (EQH/IDE) [%]", fontsize = 14)
    plt.title('Wmhd Ratio Histogram', fontsize = 14)
    plt.ylim(0, 300)
    #plt.axis('scaled')
    #plt.tight_layout()
    plt.savefig(png_save_file)
    plt.show()

def new_data(pickle_file, shotlist_file, ide_list_file, t_res, from_Shotlist=False, gen_new_pickle=False):

    if from_Shotlist:
        shotnumbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)
    else:
        # Load the shotnumbers and statuses from th csv
        data = pd.read_csv(ide_list_file, delimiter=',')
        data[['ShotNumber', 'EQH_status', 'IDE_status']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)
        shotnumbers = data['ShotNumber']

    if gen_new_pickle:
        scalar_data = {}
        for i, shotnumber in enumerate(shotnumbers):
            print(shotnumber)

            #check if EQH adn IDE are available 
            if data['EQH_status'][i] == 1 and data['IDE_status'][i] == 1:

                scalar_data = populate_data(scalar_data, shotnumber, t_res, pickle_file)

    # Load data from file to reset it
    with open(pickle_file, 'rb') as f:
        scalar_data = pickle.load(f)    

    


if __name__ == "__main__":
    main()