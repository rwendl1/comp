import aug_sfutils as sf
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import pickle
import os
import utils
import privmapeq


def legend_thingy(ax1, ax2, exlines = [] , exlabels =[], plot = True):

    lines, labels = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    fiLines = lines + lines2 + exlines
    fiLabels =  labels + labels2 + exlabels
    if plot:
        ax2.legend(fiLines, fiLabels, loc='center left', bbox_to_anchor=(1.05, 0.5))

    return fiLines, fiLabels

def plot_scalarTime(shot_data, shot_number, scalars, t_res = 0.1):

    print(f'plotting shotnumber: {shot_number}')
    diag = ['IDE', 'EQH']
    linestyles = ['solid', 'dotted'] 

    if shot_number == 39870:
        return

    #set data
    shot = shot_data[shot_number]

    #create start adn end times of constant plasma current
    ipc_obj = utils.get_signal_objects(sf.SFREAD(shot_number, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
    
    #define time indexes for varaible resolution timescales of IDA and EQH
    times, eqh_time_indexes, ide_time_indexes = utils.create_timestamps(shot['EQH']['time'], shot['IDE']['time'], start_time, end_time, t_res)
    #times, eqh_time_indexes, ide_time_indexes = utils.create_timestamps(shot['EQH']['time'], shot['IDE']['time'], shot['EQH']['tcIps'], shot['EQH']['tcIpe'], t_res)

    displ_BTF_idx = utils.find_idx_of_nearest(shot_data[shot_number]['IDE']['BTF_TB'], times[int(len(times)/2)])
    displ_BTF = shot_data[shot_number]['IDE']['BTF'][displ_BTF_idx]
    # Create a figure
    fig, axes = plt.subplots(4, 1, figsize=(16, 10), sharex=True)
    fig.suptitle(f"Discharge {shot_number} @ BTF {displ_BTF:.2f} T ", fontsize=16)

    exLines= []; exLabels = [] 

    for i, dia in enumerate(diag):

        lstyle  = linestyles[i] 
        
        plotdata = shot_data[shot_number][dia]
        time = plotdata['time']
        Ip = plotdata['IpiFP']
        t_IP = plotdata['IpiFP_TB']
        BTF = plotdata['BTF']
        t_BTF = plotdata['BTF_TB']
        
        delRoben = plotdata['delRoben'] 
        delRuntn = plotdata['delRuntn'] 
        kappa = plotdata['k'] 

        if not plotdata['DCK_status'] is False:
            H_0 = plotdata['DCK_H0']
            H_1 = plotdata['DCK_H1']
            t_H1 = plotdata['DCK_TB']
            H_text = 'DCK'
        else:
            H_0 = plotdata['DCN_H0']
            H_1 = plotdata['DCN_H1']
            t_H1 = plotdata['DCN_TB']
            H_text = 'DCN'


        if i == 0:
            # First subplot with two y-axes
            ax1 = axes[0]
            ax1.plot(t_IP, Ip/(1000*1000), 'g', lw=1, ls = lstyle, label='IpiFP')
            ax1.set_ylabel('IpiFP [MA]', color='g')
            ax1.tick_params(axis='y', labelcolor='g')
            ax1.set_xlabel('Time [s]')

            ax2 = ax1.twinx()
            t_TOT = plotdata['P_TOT_TB']
            ax2.plot(t_TOT, plotdata['PNBI_TOT']/(1000*1000), 'b', lw=1, ls = lstyle, label='PNBI_TOT')
            ax2.plot(t_TOT, plotdata['PICR_TOT']/(1000*1000), 'darkorange', lw=1, ls = lstyle, label='PICR_TOT')
            ax2.plot(t_TOT, plotdata['PECR_TOT']/(1000*1000), 'm', lw=1, ls = lstyle, label='PECR_TOT')
            ax2.plot(t_TOT, plotdata['P_TOT']/(1000*1000), 'k', lw=1, ls = lstyle, label='P_TOT', alpha = 0.6)
            ax2.set_ylabel(r'$P_{Heating} [MW]$')
            #ax2.tick_params(axis='y', labelcolor='b')
            legend_thingy(ax1, ax2)

            # Second subplot
            axes[1].plot(t_H1, H_0, 'r', lw=1, ls = lstyle, label=H_text + ' H-0')
            axes[1].plot(t_H1, H_1, 'b', lw=1, ls = lstyle, label=H_text + ' H-1')
            axes[1].set_ylabel(r"$n_{e} [\frac{1}{m^2}]$")
            axes[1].set_xlabel('Time [s]')
            axes[1].legend(loc='center left', bbox_to_anchor=(1.05, 0.5))

        
        # Third subplot

        eqh_data = shot_data[shot_number]['EQH']
        ide_data = shot_data[shot_number]['IDE']

        eqh_delRoben = eqh_data['delRoben']
        eqh_delRuntn = eqh_data['delRuntn']
        eqh_kappa = eqh_data['k']
        ide_Wmhd = ide_data['Wmhd']
        eqh_time = eqh_data['time']

        ide_delRoben = ide_data['delRoben']
        ide_delRuntn = ide_data['delRuntn']
        ide_kappa = ide_data['k']
        ide_Wmhd = ide_data['Wmhd']
        ide_time = ide_data['time']

        ratio_delRoben = []
        ratio_delRuntn = []
        ratio_Wmhd = []
        ratio_kappa = []
        del_time = []

        for j, time_index in enumerate(eqh_time_indexes):
            #define scalars
            cur_eqh_delRoben = eqh_data['delRoben'][eqh_time_indexes[j]]
            cur_ide_delRoben = ide_data['delRoben'][ide_time_indexes[j]]

            cur_eqh_delRuntn = eqh_data['delRuntn'][eqh_time_indexes[j]]
            cur_ide_delRuntn = ide_data['delRuntn'][ide_time_indexes[j]]

            cur_eqh_Wmhd = eqh_data['Wmhd'][eqh_time_indexes[j]]
            cur_ide_Wmhd = ide_data['Wmhd'][ide_time_indexes[j]]

            cur_eqh_kappa = eqh_data['k'][eqh_time_indexes[j]]
            cur_ide_kappa = ide_data['k'][ide_time_indexes[j]]

            ratio_delRoben.append(cur_eqh_delRoben/cur_ide_delRoben)
            ratio_delRuntn.append(cur_eqh_delRuntn/cur_ide_delRuntn)
            ratio_Wmhd.append(cur_eqh_Wmhd/cur_ide_Wmhd)
            ratio_kappa.append(cur_eqh_kappa/cur_ide_kappa)
            del_time.append(eqh_time[eqh_time_indexes[j]])
        
        ratio_delRoben = np.array(ratio_delRoben)
        ratio_delRuntn = np.array(ratio_delRuntn)
        ratio_kappa = np.array(ratio_kappa)
        del_time = np.array(del_time)


        if i == 0:
            ax1 = axes[2]
            ax1.plot(times, ratio_kappa, 'g', lw=1, ls = lstyle, label=f"ratio_kappa")
            ax1.plot(times, ratio_delRoben, lw=1, ls = lstyle, c ='darkblue', label=f"ratio_delRoben")
            ax1.plot(times, ratio_delRuntn, lw=1, ls = lstyle, c ='red', label=f"ratio_delRuntn")
            ax1.plot(times, ratio_Wmhd, lw=1, ls = lstyle, c ='m', label=f"ratio_Wmhd")
            ax1.axhline(y=1, color='black', linestyle='--', linewidth=1, alpha = 0.5)
            ax1.set_ylabel('ratio (EQH/IDE)')
            ax1.set_ylim(top = 2)
            ax1.set_xlabel('Time [s]')
            ax1.legend(loc='center left', bbox_to_anchor=(1.00, 0.5))

            

        if i == 0:
            for scalar in scalars:
                diffs =[] 
                for j, time_index in enumerate(eqh_time_indexes):
                    #define scalars
                    scalar_value_eqh = shot_data[shot_number]['EQH'][scalar][eqh_time_indexes[j]]
                    scalar_value_ide = shot_data[shot_number]['IDE'][scalar][ide_time_indexes[j]]

                    #calc diff for plot
                    diff = 1000*(scalar_value_eqh- scalar_value_ide)
                    diffs.append(diff)
                if not scalar == 'Wmhd':
                    # Fourth subplot
                    ax1 = axes[3]
                    ax1.plot(times, diffs, lw=1, label=f"diff {scalar} ")
                    ax1.set_ylabel('Difference (EQH-IDE) [mm]')
                    ax1.tick_params(axis='y')
                    ax1.set_xlabel('Time (s)')
                else:
                    Wmhd_rel = scalar_value_eqh/scalar_value_ide
                    ax2 = ax1.twinx()
                    ax2.plot(times, Wmhd_rel, lw=1, label=f"{scalar} relative ")
                    ax2.set_ylabel('Wmhd', color='b')
                    ax2.tick_params(axis='y', labelcolor='b')
            #legend_thingy(ax1, ax2)
            ax1.legend(loc='center left', bbox_to_anchor=(1.00, 0.5))
            ax2.legend(loc='center left', bbox_to_anchor=(1.00, 0.5))



    # Set x-axis limits for all subplots
    axes[0].set_xlim((shot['EQH']['tcIps']-1, shot['EQH']['tcIpe']+1))
    """ for k, ax in enumerate(axes):
        axes[k].legend(loc='center left', bbox_to_anchor=(1.05, 0.5)) """


    plt.tight_layout()
    plt.savefig(f'figs/timetraces/timetrace_{shot_number}')
    plt.show()
    plt.close()

    return


if __name__ == "__main__":
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)

    #define scalars for comparison
    scalars = ( 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95', 'k')
    scalars = ( 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu')

    shot_numbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)

    #check if dict already contains poloDiff data for that shot
    for x, shot_number in enumerate(shot_numbers):
        
        plot_scalarTime(shot_data, shot_number, scalars, t_res= 0.01)



    