import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button
from matplotlib.lines import Line2D
import numpy as np
import os
import pickleboi
import utils
import privmapeq
import argparse


def get_scalars(shot, t_idx, R_cont, Z_cont):

    rmag = shot['Rmag'][t_idx]
    zmag = shot['Zmag'][t_idx]
    rxpu = shot['Rxpu'][t_idx]
    zxpu = shot['Zxpu'][t_idx]
    raus = shot['Raus'][t_idx]

    #calculates angle of the ray x point-mag center to the horizontal axis
    angle_mag2X = utils.angle_to_horizontal((rmag,zmag),(rxpu,zxpu))

    #calculates the 
    zaus_idx = utils.find_idx_of_nearest(R_cont[0, :, -1], raus)
    zaus = Z_cont[0, zaus_idx, -1]
    angle_mag2Raus = utils.angle_to_horizontal((rmag,zmag),(raus,zaus))

    data = {
        'Rmag': rmag,
        'Zmag': zmag,
        'Rxpu': rxpu,
        'Zxpu': zxpu,
        'Raus': raus,
        'Zaus': zaus,
        'angle_mag2x': angle_mag2X,
        'angle_mag2Raus': angle_mag2Raus
    }
    return data




def plot_diff2center(shot_data, shot_number, rhop, time = None, mag = False):

    #set data
    eqh_shot = shot_data[shot_number]['EQH']
    ide_shot = shot_data[shot_number]['IDE']

    if time == None:
    
        times, eqh_idxs, ide_idxs = utils.create_timestamps(eqh_shot['time'], ide_shot['time'], eqh_shot['tcIps'], eqh_shot['tcIpe'], 1.0)

    else:
        times = [time]
        eqh_idxs = [utils.find_idx_of_nearest(eqh_shot['time'], time)]
        ide_idxs = [utils.find_idx_of_nearest(ide_shot['time'], time)]
    
    #load EQU shotfile for R, z
    eqh_equ = sf.EQU(shot_number, diag='EQH')
    ide_equ = sf.EQU(shot_number, diag='IDE')

    # Set a big title for the entire figure
    fig = plt.figure(figsize=(12, 6))  # Create a figure
    fig.suptitle(f"Contour difference profile of #{shot_number}", fontsize=16)
    
    eqh_r2, eqh_z2 = sf.rho2rz(eqh_equ, rhop, t_in=times, coord_in='rho_pol')
    ide_r2, ide_z2 = sf.rho2rz(ide_equ, rhop, t_in=times, coord_in='rho_pol')

    X_angle_list = [] 
    Raus_angle_list = [] 

    for jtime, time_index in enumerate(eqh_idxs):

        time = times[jtime]
        eqh_idx = eqh_idxs[jtime] 
        ide_idx = ide_idxs[jtime]

        #get values existing for R to determine how to split up poloidal agnle theta
        eqh_n_theta = len(eqh_r2[jtime][-1])
        ide_n_theta = len(ide_r2[jtime][-1])

        #select to n_theta for the EQU with less values of R
        if ide_n_theta > eqh_n_theta:
            n_theta = eqh_n_theta
        else:
            n_theta = ide_n_theta
        theta = np.linspace(-np.pi, np.pi, 2*n_theta)
        
        if mag:
            reRmag = eqh_shot['Rmag'][eqh_idx] - ide_shot['Rmag'][ide_idx]
            reZmag = eqh_shot['Zmag'][eqh_idx] - ide_shot['Zmag'][ide_idx]
            
            #map equ points along straights with different theta angle
            eqh_r1, eqh_z1 = privmapeq.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol', reRmag = reRmag, reZmag = reZmag)
        else:
            #map equ points along straights with different theta angle
            eqh_r1, eqh_z1 = sf.rhoTheta2rz(eqh_equ, rhop, theta, t_in=time, coord_in='rho_pol')
        ide_r1, ide_z1 = sf.rhoTheta2rz(ide_equ, rhop, theta, t_in=time, coord_in='rho_pol')

        #get scalar values for specific timepoints like Rmag, Zmag, Raus, etc... and also angle to X-Point and angle to Raus
        eqh_scls = get_scalars(eqh_shot, eqh_idx, eqh_r1, eqh_z1)
        ide_scls = get_scalars(ide_shot, ide_idx, ide_r1, ide_z1)

        X_angle_list.append(ide_scls['angle_mag2x'])
        Raus_angle_list.append(ide_scls['angle_mag2Raus'])
        mag_center_offset = utils.mag_center_offset(eqh_scls, ide_scls, theta)   
        
        for jrho, rho in enumerate(rhop):

            #get arrays for vectors from magnetic center for IDE
            R_ide = ide_r1[0,:,jrho] - np.ones_like(ide_r1[0,:,jrho])*ide_scls['Rmag']
            z_ide = ide_z1[0,:,jrho] - np.ones_like(ide_z1[0,:,jrho])*ide_scls['Zmag']
            ide_len = np.sqrt(R_ide**2 +z_ide**2)
            
            if mag:
                #get arrays for vectors from remapped magnetic center for EQH (so basically IDE mag center)
                R_eqh = eqh_r1[0,:,jrho] - np.ones_like(eqh_r1[0,:,jrho])*ide_scls['Rmag']
                z_eqh = eqh_z1[0,:,jrho] - np.ones_like(eqh_z1[0,:,jrho])*ide_scls['Zmag']
                eqh_len = np.sqrt(R_eqh**2 +z_eqh**2)

                diff = ide_len - eqh_len
            else:

                #get arrays for vectors from magnetic center for EQH
                R_eqh = eqh_r1[0,:,jrho] - np.ones_like(eqh_r1[0,:,jrho])*eqh_scls['Rmag']
                z_eqh = eqh_z1[0,:,jrho] - np.ones_like(eqh_z1[0,:,jrho])*eqh_scls['Zmag']
                eqh_len = np.sqrt(R_eqh**2 +z_eqh**2)
                
                #calculate difference between vector lengths
                diff = ide_len - (eqh_len + mag_center_offset)

            
            #plot diff and mag center offset
            plt.subplot(1, 2, 1)
            #gs00 = gridspec.GridSpecFromSubplotSpec(3, len(rhop), subplot_spec=gs0[0])
            #ax = plt.Subplot(fig, gs00[:, jrho])
            plt.plot(theta, (diff) * 1000, label=f'diff @ {rho:.2f}' + r'$\rho_{pol}$' + f' @{time:.2f}s')
            #plt.plot(theta, mag_center_offset * 1000, lw = 1, linestyle = '-.', c='crimson', label='mag center offset')

            if rho == 1:
                #define scalars
                eqh_Raus = eqh_scls['Raus']
                eqh_Zaus = eqh_scls['Zaus']
                ide_Raus = ide_scls['Raus']
                ide_Zaus = ide_scls['Zaus']
                
                #calc diff for plot
                diff_raus = ide_Raus - eqh_Raus

                #plot difference
                #plt.plot(Raus_angle_list[-1] , diff_raus*1000, 'rx')
                #plt.plot(0 , diff_raus*1000, 'bx')

                #define scalars
                eqh_Raus = eqh_scls['Rxpu']
                eqh_Zaus = eqh_scls['Zxpu']
                ide_Raus = ide_scls['Rxpu']
                ide_Zaus = ide_scls['Zxpu']
                
                #calc diff for plot
                diff_xpu = np.sqrt((ide_Raus-eqh_Raus)**2 +(ide_Zaus-eqh_Zaus)**2)

                #plot difference
                #plt.plot(X_angle_list[-1] , diff_xpu*1000, 'kx')

                #plt.legend(loc='center left', bbox_to_anchor=(-0.4, -0.2))
                #plt.legend(loc='best')

            plt.title(f"Difference Profile of #{shot_number} @ {time:.2f}s", fontsize = 14)
            plt.xlabel(r"Poloidal Angle $\theta$ [rad]", fontsize = 14)
            plt.ylabel('Difference [mm]', fontsize = 14)

            #plot the box around where the mag to x-point axis over angle plot
            if jtime == 0 and jrho == 0:
                plt.subplot(1, 2, 1)
                
                vert_height = 20
                max_diff = 1000 * min(diff)
                max_cent = 1000 * min(mag_center_offset)
                if max_diff >= max_cent:
                    vert_height = max_cent
                else:
                    vert_height = max_diff + 2

                vert_height = -35               

                plt.axhline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots horizontal 0 axis
                plt.axvline(0, alpha=1, lw=1, linestyle='--', c='k')    #plots vertical 0 axis
                plt.text(-0.3, vert_height, r'$R_{mag}$', rotation=90, fontsize=12, verticalalignment='bottom', horizontalalignment='left')

                plt.text(X_angle_list[0], vert_height, 'X-Point', rotation=90, verticalalignment='bottom', horizontalalignment='right')
                plt.axvline(X_angle_list[0], alpha=1, lw=1, linestyle='--', c='blueviolet')

                plt.text(Raus_angle_list[0]+0.33, vert_height, r'$R_{aus}$', rotation=90, fontsize=12, verticalalignment='bottom', horizontalalignment='right')
                plt.axvline(Raus_angle_list[0], alpha=1, lw=1, linestyle='--', c='green')
                plt.xlim((-np.pi,np.pi))
                

            
            #plots the contour (lower plots)
            if jtime == 0:
                plt.subplot(1, 2, 2)         #define subplot
                plt.title(f"rhoTheta2rz contour @ {time:.2f}s", fontsize = 14 )

                #plot contours
                plt.plot(eqh_r1[0, :, jrho], eqh_z1[0, :, jrho],  label='EQH_rhoTheta2rz', c='r')
                plt.plot(ide_r1[0, :, jrho], ide_z1[0, :, jrho],  label='IDE_rhoTheta2rz', c='b')
                plt.plot(eqh_r2[0][jrho], eqh_z2[0][jrho], ls= 'dotted', label='EQH_rho2rz', c='r')
                plt.plot(ide_r2[0][jrho], ide_z2[0][jrho], ls = 'dotted', label='IDE_rho2rz', c='b')

                print(max(eqh_z1[0, :, jrho]))
                print(max(ide_z1[0, :, jrho]))


                if rho == 1.0:

                    if shot_number == 40128:

                        plt.plot((1.530, 1.523), (0.798, 0.836), linewidth=3, color = 'orange', label='diff')
                        plt.plot((1.728, 1.781), (0.219, 0.340), linewidth=3, color = '#1f77b4', label='diff')
                        plt.plot((1.523, ide_scls['Rmag']),(0.798, ide_scls['Zmag']), linestyle='--', linewidth=1, color = 'k', alpha = 0.5)
                        plt.plot((1.728, ide_scls['Rmag']),(0.219, ide_scls['Zmag']), linestyle='--', linewidth=1, color = 'k', alpha = 0.5)

                    if shot_number == 40263:

                        plt.plot((1.525, 1.520), (0.827, 0.851), linewidth=3, color = 'orange', label='diff')
                        plt.plot((1.747, 1.767), (0.248, 0.287), linewidth=3, color = '#1f77b4', label='diff')
                        plt.plot((1.525, ide_scls['Rmag']),(0.827, ide_scls['Zmag']), linestyle='--', linewidth=1, color = 'k', alpha = 0.5)
                        plt.plot((1.747, ide_scls['Rmag']),(0.248, ide_scls['Zmag']), linestyle='--', linewidth=1, color = 'k', alpha = 0.5)

                    #plot magnetic centers
                    plt.plot(eqh_scls['Rmag'], eqh_scls['Zmag'], 'rx', label='EQH mag center')
                    plt.plot(ide_scls['Rmag'], ide_scls['Zmag'], 'bx', label='IDE mag center')

                    #plot the center axes
                    plt.axhline(y=0, color='black', linestyle='--', linewidth=1)
                    plt.axhline(y=eqh_scls['Zmag'], color='red', linestyle='--', linewidth=1)
                    plt.axhline(y=ide_scls['Zmag'], color='blue', linestyle='--', linewidth=1)

                    #plot X-point info on contour
                    plt.plot( ide_scls['Rxpu'], ide_scls['Zxpu'], 'x', label='X-Point', color = 'blueviolet') 
                    plt.plot((ide_scls['Rmag'], ide_scls['Rxpu']),(ide_scls['Zmag'],ide_scls['Zxpu']), linestyle='--', linewidth=1, color = 'blueviolet')

                    #plot the Raus info on contour
                    plt.plot(ide_scls['Raus'], ide_scls['Zaus'], 'x', label='Raus', color = 'green')
                    plt.plot((ide_scls['Rmag'], ide_scls['Raus']),(ide_scls['Zmag'],ide_scls['Zaus']), linestyle='--', linewidth=1, color = 'green')

                    plt.axvline(x=eqh_scls['Raus'] , color='red', linestyle='--', linewidth=1)
                    plt.axvline(x=ide_scls['Raus'] , color='blue', linestyle='--', linewidth=1)

                    #plt.legend(loc='center left', bbox_to_anchor=(-0, 1.1))
                plt.axis('scaled')
                plt.xlabel("R [m]", fontsize = 14)
                plt.ylabel('z [m]', fontsize = 14)
                plt.xlim((1.0,2.4))
                plt.ylim((-1.1,1.1))


    legend_elements = [
        Line2D([0], [0], color='r', lw=2, label='EQH Contour'),
        Line2D([0], [0], color='b', lw=2, label='IDE Contour'),
        #Line2D([0], [0], color='r', lw=2, linestyle=':', label='EQH_rho2rz'),
        #Line2D([0], [0], color='b', lw=2, linestyle=':', label='IDE_rho2rz'),
        #Line2D([0], [0], color='r', lw=2, linestyle='--', label='EQH_rhoTheta2rz'),
        #Line2D([0], [0], color='b', lw=2, linestyle='--', label='IDE_rhoTheta2rz'),
        #Line2D([0], [0], color='r', lw=2, linestyle='-.', label='EQH_rho2rz'),
        #Line2D([0], [0], color='b', lw=2, linestyle='-.', label='IDE_rho2rz'),
        Line2D([0], [0], marker='x', color='r', linestyle='None', label='EQH mag center'),
        Line2D([0], [0], marker='x', color='b', linestyle='None', label='IDE mag center'),
        Line2D([0], [0], marker='x', color='purple', linestyle='None', label='X-Point'),
        Line2D([0], [0], marker='x', color='green', linestyle='None', label='Raus'),
        Line2D([0], [0], color='orange', lw = 2 , label=f'diff @ 0.4' + r'$\rho_{pol}$' + f' @{time:.2f}s'),
        Line2D([0], [0], color='#1f77b4', lw = 2 , label=f'diff @ 1.0' + r'$\rho_{pol}$' + f' @{time:.2f}s')
    ]
    plt.legend(handles = legend_elements, loc='center left', bbox_to_anchor=(1.05, 0.5))
    plt.tight_layout()
    plt.savefig(f'figs/singleMags/plot_{shot_number}')
    plt.show()


def main():
    pickle_file = 'shot_data.pickle'
    shot_data = utils.load_shot_data(pickle_file)

    #shotnumber = 40128;    time = 3.01
    shotnumber = 40263;    time = 3.96
    rhop = [0.4, 1]

    plot_diff2center(shot_data, shotnumber, rhop, time, mag = True)



if __name__ == "__main__":
    main()


