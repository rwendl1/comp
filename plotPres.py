import aug_sfutils as sf
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def main(shotnumber, time=None, print = True):

    #fetch EQH data from AUGD
    sfEQH = sf.SFREAD(shotnumber, 'eqh')
    if sfEQH.status:    
        EQH = sf.EQU(shotnumber, diag = 'eqh')

    #fetch IDE data from AUGD
    sfIDE = sf.SFREAD(shotnumber, 'ide' )
    if sfIDE.status:
        IDE = sf.EQU(shotnumber, diag = 'ide' )


    if time is None:
        #create start and end times of constant plasma current
        ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
        t_start, t_end, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)
        time = round(t_start + ((t_end - t_start) / 2), 2)

    #print("Shotnumber: ", shotnumber)

    # Create a subplot grid for the current shot number
    fig, ax = plt.subplots(figsize=(6, 5 ))
    
    if sfEQH.status:
        # EQH plotting
        idx_EQH = find_nearest(EQH.time, time)
        pres = EQH.pres[idx_EQH,:]
        rhop = np.sqrt(EQH.psiN[idx_EQH,:])
        ax.plot(rhop, pres/1000 , label=f'EQH @ {EQH.time[idx_EQH]:.3f}s')
        ax.axhline(0, c = 'k', linestyle = 'dotted')

    if sfIDE.status:
    # IDE plotting
        idx_IDE = find_nearest(IDE.time, time)
        pres = IDE.pres[idx_IDE,:]
        rhop = np.sqrt(IDE.psiN[idx_IDE,:])
        ax.plot(rhop, pres/1000, label=f'IDE @ {IDE.time[idx_IDE]:.3f}s')

    ax.set_xlabel(r'$\rho_{pol}$', fontsize = 14)
    ax.set_ylabel('Pressure [kPa]', fontsize = 14)
    ax.legend(fontsize = 12)     
    plt.title(f'Discharge {shotnumber} @ {time:.2f}s', fontsize = 14)
    if print:
        plt.savefig(f'figs/pPlot/pres_{shotnumber}_{time}s.png')
    plt.show()



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot data for a specific shot number and time.')
    parser.add_argument('shotnumber', type=int, help='The shot number to plot data for')
    parser.add_argument('time', type=float, nargs='?', default=None, help='The time (in seconds) to plot data for (optional)')
    #parser.add_argument('--print', dest='print_plot', action='store_true', help='Print the plot')

    
    args = parser.parse_args()
    main(args.shotnumber, args.time)