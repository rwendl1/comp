import aug_sfutils as sf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button
import numpy as np
import os, sys
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

import matplotlib.patches as mpatches
import matplotlib.lines as mlines

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils
import pickleboi
import argparse

def filter_and_trim_current(time, current, cutoff_percentage=5, trim_percentage=10, smoothing = False):
    current = np.array(current)
    time = np.array(time)
    
    # Handle negative currents by inverting the array if the mean is negative
    if np.mean(current) < 0:
        current = -current

    if smoothing:
        # Smooth the current signal using a Savitzky-Golay filter
        current = savgol_filter(current, 101, 3)
    
    while True:
        avg_current = np.mean(current)
        cutoff_value = avg_current * cutoff_percentage / 100
        
        # Calculate the lower and upper bounds
        lower_bound = avg_current - cutoff_value
        
        # Filter the current array based on the bound
        filtered_indices = (current >= lower_bound)
        filtered_current = current[filtered_indices]
        filtered_time = time[filtered_indices]
        
        # If the filtered array is the same as the current array, we are done
        if np.array_equal(filtered_current, current):
            break
        
        # Update current and time arrays for the next iteration
        current = filtered_current
        time = filtered_time
    
    # Perform trimming at the end
    trim_count = int(len(current) * trim_percentage / 100)
    trimmed_time = time[trim_count:-trim_count]
    
    # Return the start and end time of the trimmed array
    return trimmed_time[0], trimmed_time[-1], avg_current



def custom_legend(ax, loc='center left', bbox_to_anchor=(1.05, 0.5)):

    # Create some lines and patches to use in the legend
    line_lp = mlines.Line2D([], [], color='blue', label='lp')
    line_avg_current = mlines.Line2D([], [], color='black', linestyle='--', label='avg_current')
    line_start_trim = mlines.Line2D([], [], color='red', linestyle='--', label='Start Trim')
    line_end_trim = mlines.Line2D([], [], color='red', linestyle='--', label='End Trim')
    line_start_final = mlines.Line2D([], [], color='blue', linestyle='--', label='Start final')
    line_end_final = mlines.Line2D([], [], color='blue', linestyle='--', label='End final')

    patch_excluded1 = mpatches.Patch(color='gray', label='Excluded')
    patch_safety_margin1 = mpatches.Patch(color='lightcoral', label='safety margin')

    # Create the legend
    ax.legend(handles=[line_lp, line_avg_current, line_start_trim, line_end_trim, 
                        line_start_final, line_end_final, patch_excluded1, 
                        patch_safety_margin1], loc=loc, bbox_to_anchor=bbox_to_anchor)

def filter_and_trim_current_plot(time, current, cutoff_percentage=5, trim_percentage=10, smoothing=False,):
    # Handle negative currents by inverting the array if the mean is negative
    if np.mean(current) < 0:
        current = -current

    start_current = current
    start_time = time
    if smoothing:
        # Smooth the current signal using a Savitzky-Golay filter
        current = savgol_filter(current, 101, 3)

    unit = 1/100000
    figsize=(12, 5)
    title ='Establishing a Stable Plasma Current Regime through Averaging and Thresholding'

    # Plot current iteration of plasma current
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    
    ax.plot(start_time, unit *start_current, label=f'Ip')
    ax.set_xlabel('Time [s]', fontsize=14)
    ax.set_ylabel('Plasma current [MA]', fontsize=14)
    ax.set_title(title, fontsize=14)
    custom_legend(ax, loc='center left', bbox_to_anchor=(1.05, 0.5))
    #ax.legend(loc='center left', bbox_to_anchor=(1.05, 0.5))
    ax.set_xlim(start_time[0], start_time[-1])
    plt.tight_layout()
    plt.savefig(f'figs/gif/Ip_0.png')
    plt.close()
    
    iteration = 1
    while True:
        avg_current = np.mean(current)
        cutoff_value = avg_current * cutoff_percentage / 100
        
        # Calculate the lower and upper bounds
        lower_bound = avg_current - cutoff_value
        
        # Filter the current array based on the bound
        filtered_indices = (current >= lower_bound)
        filtered_current = current[filtered_indices]
        filtered_time = time[filtered_indices]

        rect = plt.Rectangle((filtered_time[0], unit *lower_bound), filtered_time[-1]- filtered_time[0],  2 * unit *cutoff_value, color='green', alpha=0.5)
        
        # Plot current iteration of plasma current
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        
        ax.plot(start_time, unit * start_current, label=f'Ip')
        #ax.plot(time, current, label=f'Ip')
        ax.axhline(y=unit *avg_current, color='k', linestyle='--', label='avg_current')
        ax.add_patch(rect)
        #ax.axhspan(avg_current - cutoff_value, avg_current + cutoff_value, color='gray', alpha=0.5, label='Cutoff')

        ax.axvline(x=filtered_time[0], color='r', linestyle='--', label='Start Trim')
        ax.axvline(x=filtered_time[-1], color='r', linestyle='--', label='End Trim')

        ax.axvspan(xmin = 0, xmax=filtered_time[0], color='k', alpha = 0.5, label='Excluded')
        ax.axvspan(xmin = filtered_time[-1], xmax=20, color='k', alpha = 0.5, label='Excluded')

        plt.text(filtered_time[0]-0.1, 0.2, f't_Start = {filtered_time[0]:.2f}s', color = 'r', rotation=90, verticalalignment='bottom', horizontalalignment='right')
        plt.text(filtered_time[-1]+0.1, 0.2, f't_End = {filtered_time[-1]:.2f}s', color = 'r', rotation=90, verticalalignment='bottom', horizontalalignment='left')
        
        ax.set_xlabel('Time [s]', fontsize=14)
        ax.set_ylabel('Plasma current [MA]', fontsize=14)
        ax.set_title(title, fontsize=14)
        custom_legend(ax, loc='center left', bbox_to_anchor=(1.05, 0.5))
        #ax.legend(loc='center left', bbox_to_anchor=(1.05, 0.5))
        ax.set_xlim(start_time[0], start_time[-1])
        plt.tight_layout()
        plt.savefig(f'figs/gif/Ip_{iteration}.png')
        plt.close()
        
        iteration += 1
        
        # If the filtered array is the same as the current array, we are done
        if np.array_equal(filtered_current, current):
            break
        
        # Update current and time arrays for the next iteration
        current = filtered_current
        time = filtered_time
    
    # Perform trimming at the end
    trim_count = int(len(current) * trim_percentage / 100)
    trimmed_time = time[trim_count:-trim_count]

    rect = plt.Rectangle((trimmed_time[0], unit *lower_bound), trimmed_time[-1]- trimmed_time[0],  2 * unit *cutoff_value, color='green', alpha=0.5)

    # Plot current iteration of plasma current
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    
    ax.plot(start_time, unit * start_current, label=f'Ip')
    #ax.plot(time, current, label=f'Ip')
    ax.axhline(y=unit *avg_current, color='k', linestyle='--', label='avg_current')
    ax.add_patch(rect)
    #ax.axhspan(avg_current - cutoff_value, avg_current + cutoff_value, color='gray', alpha=0.5, label='Cutoff')

    ax.axvline(x=filtered_time[0], color='r', linestyle='--', label='Start Trim')
    ax.axvline(x=filtered_time[-1], color='r', linestyle='--', label='End Trim')

    ax.axvline(x=trimmed_time[0], color='b', linestyle='--', label=r'Start final')
    ax.axvline(x=trimmed_time[-1], color='b', linestyle='--', label=r'End final')

    ax.axvspan(xmin = 0, xmax=filtered_time[0], color='k', alpha = 0.5, label='Excluded')
    ax.axvspan(xmin = filtered_time[-1], xmax=14, color='k', alpha = 0.5, label='Excluded')

    ax.axvspan(xmin = filtered_time[0], xmax=trimmed_time[0], color='r', alpha = 0.4, label=r'safety margin')
    ax.axvspan(xmin = trimmed_time[-1], xmax=filtered_time[-1], color='r', alpha = 0.4, label=r'safety margin')

    ax.text(trimmed_time[0]-0.1, 0.2, f't_Start = {trimmed_time[0]:.2f}s', color = 'b', rotation=90, verticalalignment='bottom', horizontalalignment='right')
    ax.text(trimmed_time[-1]+0.1, 0.2, f't_End = {trimmed_time[-1]:.2f}s', color = 'b', rotation=90, verticalalignment='bottom', horizontalalignment='left')
    
    ax.set_xlabel('Time [s]', fontsize=14)
    ax.set_ylabel('Plasma current [MA]', fontsize=14)
    ax.set_title(title, fontsize=14)
    #ax.legend(loc='center left', bbox_to_anchor=(1.05, 0.5))
    ax.set_xlim(start_time[0], start_time[-1])
    custom_legend(ax, loc='center left', bbox_to_anchor=(1.05, 0.5))
    plt.tight_layout()
    plt.savefig(f'figs/gif/Ip_{iteration}.png')
    plt.close()
    
    """ # Plot the final trimmed times
    plt.figure(figsize=(10, 6))
    plt.plot(time, current, label='Final Plasma Current')
    plt.axvline(x=trimmed_time[0], color='r', linestyle='--', label='Start Trim')
    plt.axvline(x=trimmed_time[-1], color='r', linestyle='--', label='End Trim')
    plt.xlabel('Time')
    plt.ylabel('Current')
    plt.title('Final Plasma Current with Trimmed Times')
    plt.legend()
    plt.savefig(f'figs/gif/Ip_final.png')
    plt.close() """
    
    # Return the start and end time of the trimmed array
    return trimmed_time[0], trimmed_time[-1], avg_current

def main():
    pickle_file = 'shot_data.pickle'
    shot_data = utils.load_shot_data(pickle_file)

    shotnumber = 40148;    time = 3.01
    #shotnumber = 40263;    time = 3.96

    #create start adn end times of constant plasma current
    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
    start_time, end_time, avg = filter_and_trim_current_plot(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 1, 10, True)

    """ plt.plot(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], label='IpiFP')
    plt.axvline(start_time, color='r', linestyle='--', label='Start')
    plt.axvline(end_time, color='r', linestyle='--', label='End')
    plt.legend()
    plt.savefig(f'figs/gif/Ip_{shotnumber}.png') """





if __name__ == "__main__":
    main()