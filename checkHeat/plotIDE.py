import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'x-large',
          'figure.figsize': (6, 9),
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)


# Load the dataset
data = pd.read_csv('checkIDE.csv', delimiter=',')

# Convert numerical columns to int (if not already done)
data[['ShotNumber', 'EQH', 'IDE']] = data[['ShotNumber', 'EQH', 'IDE']].astype(int)

# Calculate the counts
total = 10000
eqh_count = data['EQH'].sum()
ide_count = data['IDE'].sum()

# Calculate the percentages
eqh_percent = (eqh_count / total) * 100
ide_percent = (ide_count / total) * 100

print('EQH:', eqh_percent)
print('IDE:', ide_percent)

# Labels and percentages
labels = ['EQH', 'IDE']
counts = [eqh_percent, ide_percent]

# Plotting the counts
#plt.figure(figsize=(5, 8))
#ipp color #006C66
#tuwien color 
plt.bar(labels, counts, color=['#006AAC', 'orange'], width=0.3)

# Customizing the plot
#plt.xlabel('Equilibria')
plt.ylabel(r' % of total last 10k discharges', fontsize = 16)
plt.title(r'Availability in % of last 10k discharges', fontsize = 16)

# Removing the borders
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.gca().spines['left'].set_visible(False)
plt.gca().spines['bottom'].set_visible(True)

# Displaying only x and y axis lines
plt.axhline(y=0, color='k', linewidth=0.8)
plt.axvline(x=-0.5, color='k', linewidth=0.8)

# Save and show the plot
plt.savefig('plotIDE.jpg')
plt.show()
