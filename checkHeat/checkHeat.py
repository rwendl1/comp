import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import pickleboi
import utils
import privmapeq
import csv
import math
from sql import sqlFuncs

def split_times(time, start_time, end_time, delta_t = 1.0, full_secs = False):
    times = []
    idxs = [] 

    if full_secs:
        current_time = math.ceil(start_time)
    else:
        current_time = start_time

    while current_time <= end_time:
        idx = utils.find_idx_of_nearest(time, current_time)
        times.append(time[idx])
        idxs.append(idx)
        current_time += delta_t
    return times, idxs

def check_power(shotnumber, diag):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        return 1
    else:
        return 0
    
def get_power(shotnumber, diag, signal_name):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        signal = sfr.getobject(signal_name)
        timebase = sfr.gettimebase(signal_name)
        return signal, timebase
    
def find_zero_index(arr):
    """
    This function takes a numpy array as input and returns the index where the array first gets to zero.
    If zero is not found, it returns -1.
    """
    zero_indices = np.where(arr == 0)[0]
    if zero_indices.size > 0:
        return zero_indices[0]
    else:
        return -1


def process_shot_data(latest_shotnumber, until_shotnumber, output_csv):
    results = []

    # Write results to a CSV file
    with open(output_csv, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        
        # Write the header
        header = ['ShotNumber', 'StartTime', 'EndTime', 'PressureProfile', 'NBI', 'ECRH', 'ICRH']
        for i in np.arange(1, 11, 1):
            header.append(f"t{i}")
            header.append(f"rho{i}")
            header.append(f"NBI{i}")
            header.append(f"ECRH{i}")
            header.append(f"ICRH{i}")
            
        det = sqlFuncs.fetch_shot_details(latest_shotnumber)
        if det:
            for k in det.keys():
                header.append(k)
        else:
            print("No data found or error occurred.")

        csvwriter.writerow(header)

        for shotnumber in range(until_shotnumber, latest_shotnumber + 1):
            
            #check if EQh shotfile for shotnumber exists
            sfr = sf.SFREAD(shotnumber, 'EQH')  # Check if EQI shotfile exists
            if sfr.status:
                #get pressure data
                try:
                    equ = sf.EQU(shotnumber, diag='EQH')
                    time = equ.time
                    pres = equ.pres
                    #rhop = np.sqrt(equ.psiN)
                    rhop = sf.mapeq.rho2rho(equ, equ.rho_tor_n, coord_in = "rho_tor", coord_out = "rho_pol")

                    #create start adn end times of constant plasma current
                    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
                    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)

                    #split times of constant plasma current up into timepoints
                    times, idxs = split_times(time, start_time, end_time, delta_t= 1.0, full_secs = False)

                    #checks wich powers are included in this shot
                    NBI_status = check_power(shotnumber, 'NIS')
                    ECRH_status = check_power(shotnumber, 'ECS')
                    ICRH_status = check_power(shotnumber, 'ICP')

                    #gets the powers signal if existing
                    if NBI_status == 1:
                        NBI, NBI_tb = get_power(shotnumber, 'NIS', 'PNI')
                    if ECRH_status == 1:
                        ECRH, ECRH_tb = get_power(shotnumber, 'ECS', 'PECRH')
                    if ICRH_status == 1:
                        ICRH, ICRH_tb = get_power(shotnumber, 'ICP', 'PICRH')

                    zero_at_rhop_list = []
                    for signs in range(0,5):
                        zero_at_rhop_list = zero_at_rhop_list + [0, 0, 0, 0, 0]

                    pres_profile = 1
                    # iterate over all timepoints
                    for i, idx in enumerate(idxs):
                        #check pressure profile of shotnumber
                        pres_at_t_idx = pres.transpose()[:, idx]
                        zero_idx = find_zero_index(pres_at_t_idx)
                        zero_at_rhop = rhop.transpose()[zero_idx, idx]
                        print(f" shot: {shotnumber} at time {time[idx]:.2f} zero at:   {zero_at_rhop}")

                        zero_at_rhop_list[5*i] = (np.round(time[idx],2))
                        zero_at_rhop_list[5*i + 1] = (np.round(zero_at_rhop,2))

                        #writes the power at each timestep that is analyzed
                        if NBI_status == 1:
                            zero_at_rhop_list[5*i + 2] = (np.round(NBI[utils.find_idx_of_nearest(NBI_tb, times[i])], 2))
                        if ECRH_status == 1:
                            zero_at_rhop_list[5*i + 3] = (np.round(ECRH[utils.find_idx_of_nearest(ECRH_tb, times[i])], 2))
                        if ICRH_status == 1:
                            zero_at_rhop_list[5*i + 4] = (np.round(ICRH[utils.find_idx_of_nearest(ICRH_tb, times[i])], 2))

                        if zero_at_rhop <= 0.95:
                            pres_profile = 0

                    #gets the details from the journal for each shot
                    det = sqlFuncs.fetch_shot_details(shotnumber)
                    if det:
                        details = []
                        for k in det.keys():
                            details.append(det[k])
                    else:
                        print("No data found or error occurred.")
                    

                    # Append the result for the current shotnumber
                    row = [shotnumber, start_time, end_time, pres_profile, NBI_status, ECRH_status, ICRH_status] + zero_at_rhop_list + details

                    # Write the data
                    csvwriter.writerow(row)

                except Exception as e:
                    print(f"Error: {e}")

def main_time():
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    shot_data = utils.load_shot_data(pickle_file)
    shot_numbers, tcIps_list, tcIpe_list = utils.readShotlistData(shotlist_file)

    #check if dict already contains poloDiff data for that shot
    for x, shot_number in enumerate(shot_numbers):
        print(shot_data[shot_number].keys())
        time = shot_data[shot_number]['EQH']['IpiFP_TB']
        Ip = shot_data[shot_number]['EQH']['IpiFP']
        func_start, func_end, avg = utils.filter_and_trim_current(time, Ip, 3, 10, True)
        av_Ip = np.average(Ip)
        print(f"start: {func_start:.6f} end: {func_end:.6f}")

        plt.plot(time, Ip)
        plt.axvline(func_start, c = 'r')
        plt.axvline(func_end, c = 'r')
        plt.axvline(tcIps_list[x], c = 'b')
        plt.axvline(tcIpe_list[x], c = 'b')
        plt.axhline(avg, c = 'k')
        plt.axvspan(func_start, func_end ,avg*0.9, avg*1.1, color='orange', alpha = 0.3)
        plt.savefig(f'get_all/{shot_number}')
        plt.show()
        plt.clf()
    return

def main():

    latest_shot = 41545
    diff = 10000
    #latest_shot = 36544
    start_shot = latest_shot - diff
    #process_shot_data(start_shot+10, start_shot, 'checkHeat.csv')
    #process_shot_data(latest_shot, start_shot, 'checkHeat.csv')
    process_shot_data(41500, 41495, 'checkHeat.csv')

    return


if __name__ == "__main__":
    main()
