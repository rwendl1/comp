import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import pickleboi
import utils
import privmapeq
import csv
import math
from sql import sqlFuncs


def process_shot_data(latest_shotnumber, until_shotnumber, output_csv):
    results = []

    # Write results to a CSV file
    with open(output_csv, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        
        # Write the header
        header = ['ShotNumber', 'EQH', 'IDE', ]
        csvwriter.writerow(header)

        for shotnumber in range(until_shotnumber, latest_shotnumber + 1):

            eqh = 0
            ide = 0

            try:
                #check if EQh shotfile for shotnumber exists
                sfr = sf.SFREAD(shotnumber, 'EQH')  # Check if EQI shotfile exists
                if sfr.status:
                    eqh = 1

                    
                    sfide = sf.SFREAD(shotnumber, 'IDE')  # Check if EQI shotfile exists
                    if sfide.status:
                        ide = 1

                    # Append the result for the current shotnumber
                    row = [shotnumber, eqh, ide]

                    # Write the data
                    csvwriter.writerow(row)

            except Exception as e:
                print(f"Error: {e}")


def main():

    latest_shot = 41545
    diff = 10000
    #latest_shot = 36544
    start_shot = latest_shot - diff
    #process_shot_data(start_shot+10, start_shot, 'checkIDE.csv')
    process_shot_data(latest_shot, start_shot, 'checkIDE.csv')
    #process_shot_data(41500, 41495, 'checkIDE.csv')

    return


if __name__ == "__main__":
    main()
