import os, sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))  # Add two levels up to sys.path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))        #so it gets files and functions from parent directory

import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import pickleboi
import utils
import privmapeq
import csv
import math
import mysql.connector
import numpy as np
import pickle
import pandas as pd

def split_times(time, start_time, end_time, delta_t = 1.0, full_secs = False):
    times = []
    idxs = [] 

    if full_secs:
        current_time = math.ceil(start_time)
    else:
        current_time = start_time

    while current_time <= end_time:
        idx = utils.find_idx_of_nearest(time, current_time)
        times.append(time[idx])
        idxs.append(idx)
        current_time += delta_t
    return times, idxs

def check_diag(shotnumber, diag):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        return 1
    else:
        return 0
    
def fetchSignalAndTimebase(shotnumber, diag, signal_name):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        signal = sfr.getobject(signal_name)
        timebase = sfr.gettimebase(signal_name)
        return signal, timebase
    
def find_zero_index(arr):
    """
    This function takes a numpy array as input and returns the index where the array first gets to zero.
    If zero is not found, it returns -1.
    """
    zero_indices = np.where(arr == 0)[0]
    if zero_indices.size > 0:
        return zero_indices[0]
    else:
        return -1
    
def fetch_sql_details(shotno):
    try:
        # Establishing the connection to the database
        connection = mysql.connector.connect(
            host="srv-mariadb-1.ipp.mpg.de",
            user="augxro",
            password="augxro",
            database="aug_operation",
            ssl_disabled=False,  # SSL is enabled
            ssl_verify_cert=False  # Disable server certificate verification if needed
        )

        # Creating a cursor object using the cursor() method
        cursor = connection.cursor()

        # Preparing SQL query to fetch data from the database
        query = f"SELECT * FROM augjournal WHERE shotno = {shotno}"

        # Executing the SQL query
        cursor.execute(query)

        # Fetching the first row from the executed query
        result = cursor.fetchone()

        # Closing the cursor and connection
        cursor.close()
        connection.close()

        if result:
            # Debugging: print the entire result to find correct indices
            #print("Fetched result:", result)

            time = result[4]
            date = result[3]
            #print(date)

            data = {
                'date': date,
                'time': time,
            }

            return data
        else:
            return None

    except mysql.connector.Error as error:
        print(f"Error: {error}")
        return None


def ana_data(file_path):

    
    data = {}

    # Load data from file to reset it
    with open(file_path, 'rb') as f:
        data = pickle.load(f)  

    print(data.keys())
    #print(len(data.keys()))
    
    # Convert the loaded data into a DataFrame for easier manipulation
    shots_data = []
    for shot_number, shot_info in data.items():
        shots_data.append({
            'ShotNumber': shot_number,
            **shot_info  # This unpacks the nested dictionary into the new dictionary
        })
    df = pd.DataFrame(shots_data)
    
    # Assuming 'rho' needs to be processed similarly to the previous example
    # Bin the shots into groups of 100
    df['bin'] = df['ShotNumber'].apply(lambda x: (x - 1) // 100)
    
    # Calculate the percentage of shots in each bin with `rho` below 0.95
    bin_counts = df.groupby('bin')['rho'].apply(lambda x: (x < 0.95).mean() * 100)
    
    # Get the shot number range for each bin
    bin_ranges = df.groupby('bin')['ShotNumber'].agg(['min', 'max']).apply(lambda x: f"{x['min']}-{x['max']}", axis=1)
    
    # Plot the histogram
    plt.figure(figsize=(12, 6))
    bin_counts.plot(kind='bar')
    plt.xlabel('Shot Number Bins (Groups of 100)', fontsize=14)
    plt.ylabel('Percentage of bad p-profiles per bin', fontsize=14)
    plt.title('Timeline of bad profiles with ' + r'$p(\rho_{pol}<0.95) = 0$' + ' for discharges #31545 to #41545', fontsize=14)

    # Generate all tick positions
    all_tick_positions = np.arange(len(bin_ranges))

    # Create labels where only every 5th label is visible
    all_tick_labels = ['' if i % 5 != 0 else label for i, label in enumerate(bin_ranges)]

    # Apply the ticks and labels to the plot
    plt.xticks(ticks=all_tick_positions, labels=all_tick_labels, rotation=45, ha='right', fontsize=8)


    #plt.xticks(ticks=np.arange(len(bin_ranges)), labels=bin_ranges, rotation=45, ha='right', fontsize=5.5)
    plt.tight_layout()
    plt.savefig('figs/timeline/shot_bins_pickle.png')
    plt.show()

def main():

    latest_shot = 41545
    diff = 10000
    start_shot = latest_shot - diff

    ana_data('TimeLine_profiles/data/data_10k.pickle')

    return


if __name__ == "__main__":
    main()
