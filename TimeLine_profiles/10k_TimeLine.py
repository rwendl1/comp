import pandas as pd
import matplotlib.pyplot as plt

# Function to process the data
def process_data(file_path):
    # Read the CSV file
    data = pd.read_csv(file_path)
    
    # Extract necessary columns
    relevant_data = data.loc[:, ['ShotNumber', 'PressureProfile', 'rho3']].copy()
    
    # Bin the shots into groups of 100
    num_shots = relevant_data.shape[0]
    bins = (num_shots + 99) // 100  # Calculate the number of bins
    relevant_data['bin'] = relevant_data.index // 100  # Create a bin column
    
    # Calculate the percentage of shots in each bin with `rho` below 0.95
    bin_counts = relevant_data.groupby('bin')['rho3'].apply(lambda x: (x < 0.95).mean() * 100)

    # Get the shot number range for each bin
    bin_labels = relevant_data.groupby('bin')['ShotNumber'].agg(['min', 'max']).apply(lambda x: f"{int(x['min'])}-{int(x['max'])}", axis=1)
    
    # Plot the histogram
    plt.figure(figsize=(12, 6))
    bin_counts.plot(kind='bar')
    plt.xlabel('Discharge bins (Groups of 100)', fontsize=14)
    plt.ylabel(r'Percentage of Shots with $p(\rho_{pol} < 0.95)=0$', fontsize=14)
    plt.title('Percentage of bad EQH p-profiles per Bin of 100 discharges', fontsize=14)
    plt.ylim(0, 100)  # Ensure the y-axis ranges from 0 to 100
    plt.xticks(ticks=range(len(bin_labels)), labels=bin_labels, rotation=45, ha='right', fontsize=8)
    plt.tight_layout()
    plt.savefig('figs/timeline/shot_bins.png')
    plt.show()

# Example usage
file_path = 'checkHeat/checkHeat10k.csv'
process_data(file_path)
