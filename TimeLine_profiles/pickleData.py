import os, sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))  # Add two levels up to sys.path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))        #so it gets files and functions from parent directory

import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import pickleboi
import utils
import privmapeq
import csv
import math
import mysql.connector
import numpy as np
import pickle

def split_times(time, start_time, end_time, delta_t = 1.0, full_secs = False):
    times = []
    idxs = [] 

    if full_secs:
        current_time = math.ceil(start_time)
    else:
        current_time = start_time

    while current_time <= end_time:
        idx = utils.find_idx_of_nearest(time, current_time)
        times.append(time[idx])
        idxs.append(idx)
        current_time += delta_t
    return times, idxs

def check_diag(shotnumber, diag):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        return 1
    else:
        return 0
    
def fetchSignalAndTimebase(shotnumber, diag, signal_name):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        signal = sfr.getobject(signal_name)
        timebase = sfr.gettimebase(signal_name)
        return signal, timebase
    
def find_zero_index(arr):
    """
    This function takes a numpy array as input and returns the index where the array first gets to zero.
    If zero is not found, it returns -1.
    """
    zero_indices = np.where(arr == 0)[0]
    if zero_indices.size > 0:
        return zero_indices[0]
    else:
        return -1
    
def fetch_sql_details(shotno):
    try:
        # Establishing the connection to the database
        connection = mysql.connector.connect(
            host="srv-mariadb-1.ipp.mpg.de",
            user="augxro",
            password="augxro",
            database="aug_operation",
            ssl_disabled=False,  # SSL is enabled
            ssl_verify_cert=False  # Disable server certificate verification if needed
        )

        # Creating a cursor object using the cursor() method
        cursor = connection.cursor()

        # Preparing SQL query to fetch data from the database
        query = f"SELECT * FROM augjournal WHERE shotno = {shotno}"

        # Executing the SQL query
        cursor.execute(query)

        # Fetching the first row from the executed query
        result = cursor.fetchone()

        # Closing the cursor and connection
        cursor.close()
        connection.close()

        if result:
            # Debugging: print the entire result to find correct indices
            #print("Fetched result:", result)

            time = result[4]
            date = result[3]
            #print(date)

            data = {
                'date': date,
                'time': time,
            }

            return data
        else:
            return None

    except mysql.connector.Error as error:
        print(f"Error: {error}")
        return None


def process_shot_data(latest_shotnumber, until_shotnumber, output_file, t_res = 1.0, single = False):

    det = fetch_sql_details(latest_shotnumber)
    data = {}

    for shotnumber in range(until_shotnumber, latest_shotnumber + 1):
        
        count = 0
        #check if EQh shotfile for shotnumber exists
        sfr = sf.SFREAD(shotnumber, 'EQH')  # Check if EQI shotfile exists
        if sfr.status:
            #get pressure data
            try:
                equ = sf.EQU(shotnumber, diag='EQH')
                time = equ.time
                pres = equ.pres
                #rhop = np.sqrt(equ.psiN)
                rhop = sf.mapeq.rho2rho(equ, equ.rho_tor_n, coord_in = "rho_tor", coord_out = "rho_pol")

                #create start adn end times of constant plasma current
                ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
                start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)

                if end_time - start_time <= 1.6:
                    print("shot duration to short, probably inconstant Ip")
                    continue

                #split times of constant plasma current up into timepoints
                times, idxs = split_times(time, start_time, end_time, delta_t = t_res, full_secs = False)

                #gets the details from the journal for each shot
                det = fetch_sql_details(shotnumber)
                if det:
                    details = []
                    for k in det.keys():
                        details.append(det[k])
                    print(details)
                else:
                    print("No data found or error occurred.")

                if single:
                    idxs = [idxs[int(len(idxs)/2)]]

                # iterate over all timepoints
                for i, idx in enumerate(idxs):
                    pres_profile = 1
                    zero_at_rhop_list = [0,0]
                    #check pressure profile of shotnumber
                    pres_at_t_idx = pres.transpose()[:, idx]
                    zero_idx = find_zero_index(pres_at_t_idx)
                    zero_at_rhop = rhop.transpose()[zero_idx, idx]
                    print(f" shot: {shotnumber} at time {time[idx]:.2f} zero at: {zero_at_rhop}")

                    zero_at_rhop_list[0] = (np.round(time[idx], 2))
                    rho = (np.round(zero_at_rhop, 2))


                    if zero_at_rhop <= 0.95:
                        pres_profile = 0

                    # Append the result for the current shotnumber and time
                    #row = [shotnumber, pres_profile, rho] + details

                    # Write the data
                    data[shotnumber]= {
                        'PressureProfile': pres_profile,
                        'rho': rho,
                        'date': details[0],
                        'time': details[1],
                    }

                    if count % 10 == 0:
                        # Save data to a file
                        with open(output_file, 'wb') as f:
                            pickle.dump(data, f)   

                        # Load data from file to reset it
                        with open(output_file, 'rb') as f:
                            data = pickle.load(f)  
                    count += 1

            except Exception as e:
                print(f"Error: {e}")

def main():

    latest_shot = 41545
    diff = 20000
    #latest_shot = 36544
    start_shot = latest_shot - diff
    #process_shot_data(start_shot+10, start_shot, 'NN/data/NN.csv', t_res = 0.4)
    process_shot_data(latest_shot, start_shot, 'TimeLine_profiles/data/data_20k.pickle', t_res = 0.4, single = True)
    #process_shot_data(41500, 41495, 'TimeLine_profiles/data_20k.csv', t_res = 0.4, single = True)

    return


if __name__ == "__main__":
    main()
