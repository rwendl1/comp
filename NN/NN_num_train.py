import pandas as pd
import numpy as np
import os, sys
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, Flatten, Concatenate, Dense, Input
from tensorflow.keras.models import Model
import tensorflow as tf
import shap
import NN_main

def file_path(shotlist_file):
    """ read shotlist from shotlist_file.txt """
    dir = str(os.path.dirname(__file__))            #gets directory of executed file
    filepath = dir + "/" + shotlist_file
    return filepath

def main():

    model, att, train_file, model_file, shap_file, exp_file, header_string, target_col = NN_main.init()

    #model, att+ana_att, train_file, model_file, shap_file, exp_file, header_string, target_col
    train(train_file, header_string, target_col, model+att, model_file)


def train(train_file, header_string, target_col, model_str, save_file):

    selected_numerical_cols = header_string.split(',')

    # Specify columns to use in the neural network
    #selected_numerical_cols = ['NBI_status', 'ECRH_status', 'ICRH_status', 'NBI', 'ECRH', 'ICRH', 'plasma_status', 'HMOD', 'LMOD', 'DISE', 'DISR', 'VDEo', 'VDEu', 'Ip', 'ne', 'BT', 'D', 'He', 'Ne']
    # Load the dataset
    data = pd.read_csv(train_file)

    # Replace commas with dots for numerical conversion if necessary
    data = data.replace({',': '.'}, regex=True)

    # Convert numerical columns to float
    data[selected_numerical_cols] = data[selected_numerical_cols].astype(float)

    # Extract numerical features and target
    X = data[selected_numerical_cols]
    y = data[target_col]

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    # Standardize the numerical data (specific value size shouldn matter now)
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    # Define the input layers
    numerical_input = Input(shape=(X_train.shape[1],), name='numerical_input')

    # Add hidden layers
    x = Dense(64, activation='relu')(numerical_input)
    x = Dense(64, activation='relu')(x)
    output = Dense(1, activation='sigmoid')(x)

    # Define the model
    model = Model(inputs=numerical_input, outputs=output)

    # Compile the model
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    # Train the model
    history = model.fit({'numerical_input': X_train,},
                        y_train,
                        epochs=50,
                        batch_size=32,
                        validation_split=0.2)

    # Save the entire model
    model.save(save_file)

    # Evaluate the model
    loss, accuracy = model.evaluate({'numerical_input': X_test}, y_test)
    print(f"Model: {model_str}, Test Loss: {loss}, Test Accuracy: {accuracy}")

if __name__ == "__main__":
    main()
