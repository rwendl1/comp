import os, sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))  # Add two levels up to sys.path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))        #so it gets files and functions from parent directory

import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import pickleboi
import utils
import privmapeq
import csv
import math
from sql import sqlFuncs

def split_times(time, start_time, end_time, delta_t = 1.0, full_secs = False):
    times = []
    idxs = [] 

    if full_secs:
        current_time = math.ceil(start_time)
    else:
        current_time = start_time

    while current_time <= end_time:
        idx = utils.find_idx_of_nearest(time, current_time)
        times.append(time[idx])
        idxs.append(idx)
        current_time += delta_t
    return times, idxs

def check_diag(shotnumber, diag):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        return 1
    else:
        return 0
    
def fetchSignalAndTimebase(shotnumber, diag, signal_name):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        signal = sfr.getobject(signal_name)
        timebase = sfr.gettimebase(signal_name)
        return signal, timebase
    
def find_zero_index(arr):
    """
    This function takes a numpy array as input and returns the index where the array first gets to zero.
    If zero is not found, it returns -1.
    """
    zero_indices = np.where(arr == 0)[0]
    if zero_indices.size > 0:
        return zero_indices[0]
    else:
        return -1


def process_shot_data(latest_shotnumber, until_shotnumber, output_csv, t_res = 1.0, single = False):

    # Write results to a CSV file
    with open(output_csv, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        
        # Write the header
        header = ['ShotNumber', 'StartTime', 'EndTime', 'PressureProfile',
                    'NBI_status', 'ECRH_status', 'ICRH_status',
                    't', 'rho', 'NBI', 'ECRH', 'ICRH', 'DCN', 'DCK',
                    'Wmhd', 'Wmhd_ide', 'Wmhd_ratio']
            
        det = sqlFuncs.fetch_shot_details(latest_shotnumber)
        if det:
            for k in det.keys():
                header.append(k)
        else:
            print("No data found or error occurred.")

        csvwriter.writerow(header)

        for shotnumber in range(until_shotnumber, latest_shotnumber + 1):
            
            #check if EQh shotfile for shotnumber exists
            sfr = sf.SFREAD(shotnumber, 'EQH')  # Check if EQI shotfile exists
            if sfr.status:
                #get pressure data
                try:
                    equ = sf.EQU(shotnumber, diag='EQH')
                    time = equ.time
                    pres = equ.pres
                    #rhop = np.sqrt(equ.psiN)
                    rhop = sf.mapeq.rho2rho(equ, equ.rho_tor_n, coord_in = "rho_tor", coord_out = "rho_pol")

                    #create start adn end times of constant plasma current
                    ipc_obj = utils.get_signal_objects(sf.SFREAD(shotnumber, 'FPC'), 'IpiFP', TB=['IpiFP'])
                    start_time, end_time, avg = utils.filter_and_trim_current(ipc_obj['IpiFP_TB'], ipc_obj['IpiFP'], 3, 10, True)

                    if end_time - start_time <= 1.6:
                        print("shot duration to short, probably inconstant Ip")
                        continue

                    #split times of constant plasma current up into timepoints
                    times, idxs = split_times(time, start_time, end_time, delta_t = t_res, full_secs = False)

                    #checks wich powers are included in this shot
                    NBI_status = check_diag(shotnumber, 'NIS')
                    ECRH_status = check_diag(shotnumber, 'ECS')
                    ICRH_status = check_diag(shotnumber, 'ICP')
                    DCN_status = check_diag(shotnumber, 'DCN')
                    DCK_status = check_diag(shotnumber, 'DCK')
                    GQH_status = check_diag(shotnumber, 'GQH')
                    IDG_status = check_diag(shotnumber, 'IDG')


                    #gets the powers signal if existing
                    if NBI_status == 1:
                        NBI, NBI_tb = fetchSignalAndTimebase(shotnumber, 'NIS', 'PNI')
                    if ECRH_status == 1:
                        ECRH, ECRH_tb = fetchSignalAndTimebase(shotnumber, 'ECS', 'PECRH')
                    if ICRH_status == 1:
                        ICRH, ICRH_tb = fetchSignalAndTimebase(shotnumber, 'ICP', 'PICRH')
                    if DCN_status == 1:
                        DCN_H_0, DCN_tb = fetchSignalAndTimebase(shotnumber, 'DCN', 'H-0')
                    if DCK_status == 1:
                        DCK_H_0, DCK_tb = fetchSignalAndTimebase(shotnumber, 'DCK', 'H-0')

                    if GQH_status == 1:
                        Wmhd, Wmhd_tb = fetchSignalAndTimebase(shotnumber, 'GQH', 'Wmhd')
                    if IDG_status == 1:
                        Wmhd_ide, Wmhd_ide_tb = fetchSignalAndTimebase(shotnumber, 'IDG', 'Wmhd')
                        Wmhd_ratio = Wmhd / Wmhd_ide
                    

                    #gets the details from the journal for each shot
                    det = sqlFuncs.fetch_shot_details(shotnumber)
                    if det:
                        details = []
                        for k in det.keys():
                            details.append(det[k])
                    else:
                        print("No data found or error occurred.")

                    if single:
                        idxs = [idxs[int(len(idxs)/2)]]

                    # iterate over all timepoints
                    for i, idx in enumerate(idxs):
                        pres_profile = 1
                        zero_at_rhop_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                        #check pressure profile of shotnumber
                        pres_at_t_idx = pres.transpose()[:, idx]
                        zero_idx = find_zero_index(pres_at_t_idx)
                        zero_at_rhop = rhop.transpose()[zero_idx, idx]
                        print(f" shot: {shotnumber} at time {time[idx]:.2f} zero at: {zero_at_rhop}")

                        zero_at_rhop_list[0] = (np.round(time[idx], 2))
                        zero_at_rhop_list[1] = (np.round(zero_at_rhop, 2))

                        #writes the power at each timestep that is analyzed
                        if NBI_status == 1:
                            zero_at_rhop_list[2] = (np.round(NBI[utils.find_idx_of_nearest(NBI_tb, times[i])], 2))
                        if ECRH_status == 1:
                            zero_at_rhop_list[3] = (np.round(ECRH[utils.find_idx_of_nearest(ECRH_tb, times[i])], 2))
                        if ICRH_status == 1:
                            zero_at_rhop_list[4] = (np.round(ICRH[utils.find_idx_of_nearest(ICRH_tb, times[i])], 2))
                        if DCN_status == 1:
                            zero_at_rhop_list[5] = (np.round(DCN_H_0[utils.find_idx_of_nearest(DCN_tb, times[i])], 2))
                        if DCK_status == 1:
                            zero_at_rhop_list[6] = (np.round(DCK_H_0[utils.find_idx_of_nearest(DCK_tb, times[i])], 2))
                        if GQH_status == 1:
                            zero_at_rhop_list[7] = (np.round(Wmhd[utils.find_idx_of_nearest(Wmhd_tb, times[i])], 2))
                        if IDG_status == 1:
                            zero_at_rhop_list[8] = (np.round(Wmhd_ide[utils.find_idx_of_nearest(Wmhd_ide_tb, times[i])], 2))
                            zero_at_rhop_list[9] = zero_at_rhop_list[7] / zero_at_rhop_list[8] 


                        if zero_at_rhop <= 0.95:
                            pres_profile = 0

                        # Append the result for the current shotnumber and time
                        row = [shotnumber, start_time, end_time, pres_profile, NBI_status, ECRH_status, ICRH_status] + zero_at_rhop_list + details

                        # Write the data
                        csvwriter.writerow(row)

                except Exception as e:
                    print(f"Error: {e}")

def main():

    latest_shot = 41545
    diff = 10000
    #latest_shot = 36544
    start_shot = latest_shot - diff
    #process_shot_data(start_shot+10, start_shot, 'NN/data/NN.csv', t_res = 0.4)
    process_shot_data(41545, 41500, 'NN/data/NN_new_test.csv', t_res = 0.4, single = True)
    #process_shot_data(41500, 41495, 'NN/data/NN_test.csv', t_res = 0.4)

    return


if __name__ == "__main__":
    main()
