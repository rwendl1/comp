import os, sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))  # Add two levels up to sys.path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))        #so it gets files and functions from parent directory

import aug_sfutils as sf
import pickle
    
def get_sig_from_dia(shotnumber, diag, signal_name):
    sfr = sf.SFREAD(shotnumber, diag)  # Check if EQI shotfile exists
    if sfr.status:
        signal = sfr.getobject(signal_name)
        timebase = sfr.gettimebase(signal_name)
    else:
        signal = None
        timebase = None
    return signal, timebase

def process_shot_data(latest_shotnumber, until_shotnumber, output_file):

    data = {}
    for shotnumber in range(until_shotnumber, latest_shotnumber + 1):
        
        #check if EQh shotfile for shotnumber exists
        sfr = sf.SFREAD(shotnumber, 'EQH')  # Check if EQI shotfile exists
        if sfr.status:
            
            equ = sf.EQU(shotnumber, diag='EQH')

            DCN_ne, DCN_ne_tb = get_sig_from_dia(shotnumber, 'DCN', 'H-1')
            DCK_ne, DCK_ne_tb = get_sig_from_dia(shotnumber, 'DCK', 'H-1')

            #rhop = np.sqrt(equ.psiN)
            rhop = sf.mapeq.rho2rho(equ, equ.rho_tor_n, coord_in = "rho_tor", coord_out = "rho_pol")
            
        else:
            DCN_ne, DCN_ne_tb, DCK_ne, DCK_ne_tb, rhop = None, None, None, None, None,
            
        data[shotnumber]={
            'DCN_ne': DCN_ne,
            'DCN_ne_tb': DCN_ne_tb,
            'DCK_ne': DCK_ne,
            'DCK_ne_tb': DCK_ne_tb,
            'rhop': rhop,
        }

        # Save data to a file
        with open(output_file, 'wb') as f:
            pickle.dump(data, f)   

        # Load data from file to reset it
        with open(output_file, 'rb') as f:
            data = pickle.load(f)             




def main():

    latest_shot = 41545
    diff = 10000
    #latest_shot = 36544
    start_shot = latest_shot - diff
    output_file = 'ne_NN_data.pkl'
    #process_shot_data(start_shot+10, start_shot, 'NN.csv')
    #process_shot_data(latest_shot, start_shot, output_file)
    #process_shot_data(41500, 41495, output_file)
    process_shot_data(start_shot+5, start_shot, output_file)

    with open(output_file, 'rb') as f:
        data = pickle.load(f)
        print(data.keys())
        #print(data[start_shot])

    return


if __name__ == "__main__":
    main()
