import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, Flatten, Concatenate, Dense, Input
from tensorflow.keras.models import Model
import tensorflow as tf
import shap

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))  # Add two levels up to sys.path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))        #so it gets files and functions from parent directory

import utils
import pickle

# Load the dataset
data = pd.read_csv('NN.csv')

#load ne dataset
with open('data/ne_NNdata.pkl', 'rb') as f:
    ne_data = pickle.load(f)

# Replace commas with dots for numerical conversion if necessary
data = data.replace({',': '.'}, regex=True)

#ShotNumber,StartTime,EndTime,PressureProfile,NBI_status,ECRH_status,ICRH_status,t,rho,NBI,ECRH,ICRH,comment,
# plasma_status,HMOD,LMOD,DISE,DISR,VDEo,VDEu,Ip,ne,BT,NBI_value,IC_value,EC_value,D,He,Ne

# Specify columns to use in the neural network
selected_numerical_cols = ['NBI_status', 'ECRH_status', 'ICRH_status', 'NBI', 'ECRH', 'ICRH', 'plasma_status', 'HMOD', 'LMOD', 'DISE', 'DISR',
                            'VDEo', 'VDEu', 'Ip', 'ne', 'BT', 'D', 'He', 'Ne']
target_col = 'PressureProfile'

# Convert numerical columns to float
data[selected_numerical_cols] = data[selected_numerical_cols].astype(float)
data['t'] = data['t'].astype(float)

# Extract numerical features and target
time = data['t']
X_numerical = data[selected_numerical_cols]
y = data[target_col]

for i, shotnumber in enumerate(data['ShotNumber']):
    print("hello")
    print(shotnumber)
    ne_time_array = ne_data[shotnumber]['ne_tb']
    ne_time = utils.find_idx_of_nearest(ne_time_array, time[i])
print('time ', time[i], 'ne_time ', ne_time)

# Standardize the numerical data
scaler = StandardScaler()
X_numerical = scaler.fit_transform(X_numerical)

# Combine the numerical and text data
X_combined = X_numerical

# Split the data into training and testing sets
X_train_num, X_test_num, y_train, y_test = train_test_split(X_combined, y, test_size=0.2, random_state=42)

# Define the input layers
numerical_input = Input(shape=(X_numerical.shape[1],), name='numerical_input')

# Add hidden layers
x = Dense(64, activation='relu')(numerical_input)
x = Dense(64, activation='relu')(x)
output = Dense(1, activation='sigmoid')(x)

# Define the model
model = Model(inputs=numerical_input, outputs=output)

# Compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Train the model
history = model.fit({'numerical_input': X_train_num,},
                    y_train,
                    epochs=50,
                    batch_size=32,
                    validation_split=0.2)

# Save the entire model
model.save('entire_num_no_t_model.keras')

# Evaluate the model
loss, accuracy = model.evaluate({'numerical_input': X_test_num}, y_test)
print(f"Test Loss: {loss}, Test Accuracy: {accuracy}")
