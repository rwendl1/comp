import mysql.connector
import numpy as np

def fetch_range(sql, s, e):
    range = np.arange(s,e,1)
    string  = ""
    for i in range:
        if sql[i] is not None:
            string = string + f" {sql[i]},"

    if string == "":        #check case for when no data was found
        string = "None found"
    return string

def fetch_shot_details(shotno):
    try:
        # Establishing the connection to the database
        connection = mysql.connector.connect(
            host="srv-mariadb-1.ipp.mpg.de",
            user="augxro",
            password="augxro",
            database="aug_operation",
            ssl_disabled=False,  # SSL is enabled
            ssl_verify_cert=False  # Disable server certificate verification if needed
        )

        # Creating a cursor object using the cursor() method
        cursor = connection.cursor()

        # Preparing SQL query to fetch data from the database
        query = f"SELECT * FROM augjournal WHERE shotno = {shotno}"

        # Executing the SQL query
        cursor.execute(query)

        # Fetching the first row from the executed query
        result = cursor.fetchone()

        # Closing the cursor and connection
        cursor.close()
        connection.close()

        if result:
            # Debugging: print the entire result to find correct indices
            #print("Fetched result:", result)
            
            # Extracting the desired fields
            comment = result[42].replace("\n", ", ") # Assuming this is the column for comments

            Ip = float(result[46]) / 1e6 if result[46] is not None else 0.0
            ne = float(result[47]) / 1e19 if result[47] is not None else 0.0
            BT = float(result[49]) / 1e19 if result[49] is not None else 0.0

            # Handle None values for ni_value, ic_value, and ec_value
            ni_value = float(result[199]) / 1e6 if result[199] is not None else 0.0
            ic_value = float(result[200]) / 1e6 if result[200] is not None else 0.0
            ec_value = float(result[201]) / 1e6 if result[201] is not None else 0.0
            #ni_ic_ec = f"NI {ni_value:.1f}\nIC {ic_value:.1f}\nEC {ec_value:.1f}"  # Formatting values
            ni = f"NI {ni_value:.1f}"  # Formatting values
            ic = f"IC {ic_value:.1f}"  # Formatting values
            ec = f"IC {ec_value:.1f}"  # Formatting values

            plasma_modes = fetch_range(result, 13, 30)
            modes = ['HMOD', 'LMOD', 'DISE', 'DISR', 'VDEo', 'VDEu' ]
            mode_dict = {}

            for mode in modes:
                if mode in plasma_modes:
                    mode_dict[mode] = 1
                else:
                    mode_dict[mode] = 0

            gas_range = fetch_range(result, 64, 72)
            gases = ['D', 'He', 'Ne' ]
            gas_dict = {}

            for gas in gases:
                if gas in gas_range:
                    gas_dict[gas] = 1
                else:
                    gas_dict[gas] = 0
            #gas = "H, " + fetch_range(result, 64, 72)

            plasma_status = result[12]  # yes/no for plasma
            if plasma_status == "yes":
                plasma_status = 1
            else:
                plasma_status = 0

            data = {
                'comment': comment,
                'plasma_status': plasma_status,
                **mode_dict,
                'Ip': Ip,
                'ne': ne,
                'BT': BT,
                'NBI_value': ni_value,
                'IC_value': ic_value,
                'EC_value': ec_value,
                **gas_dict,
            }

            return data
        else:
            return None

    except mysql.connector.Error as error:
        print(f"Error: {error}")
        return None

if __name__ == "__main__":
    # Example usage:
    shotno = 38896
    details = fetch_shot_details(shotno)
    if details:
        print("")
        print(details)
        """ comment, ni, ic, ec, plasma_mode, gas, plasma_status = details
        print("Comment:", comment)
        print("NI:", ni)
        print("IC:", ic)
        print("EC:", ec)
        print("Plasma Mode:", plasma_mode)
        print("Gas:", gas)
        print("Plasma Status:", plasma_status) """
    else:
        print("No data found or error occurred.")
