import mysql.connector

def fetch_data_from_db(shotno):
    try:
        # Establishing the connection to the database
        connection = mysql.connector.connect(
            host="srv-mariadb-1.ipp.mpg.de",
            user="augxro",
            password="augxro",
            database="aug_operation",
            ssl_disabled=False,  # SSL is enabled
            ssl_verify_cert=False  # Disable server certificate verification if needed
        )

        # Creating a cursor object using the cursor() method
        cursor = connection.cursor()

        # Preparing SQL query to fetch data from the database
        query = f"SELECT * FROM augjournal WHERE shotno = {shotno}"

        # Executing the SQL query
        cursor.execute(query)

        # Fetching all rows from the executed query
        result = cursor.fetchall()

        # Closing the cursor and connection
        cursor.close()
        connection.close()

        return result

    except mysql.connector.Error as error:
        print(f"Error: {error}")
        return None

# Example usage:
shotno = 41570
data = fetch_data_from_db(shotno)
if data:
    for row in data:
        print(row)
else:
    print("No data found or error occurred.")