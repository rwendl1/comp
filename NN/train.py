import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, Flatten, Concatenate, Dense, Input
from tensorflow.keras.models import Model

# Use SHAP for model interpretation
import shap

# Load the dataset
data = pd.read_csv('raw_nn_data.csv', delimiter=';')

# Replace commas with dots for numerical conversion if necessary
data = data.replace({',': '.'}, regex=True)

# Convert numerical columns to float
numerical_cols = ['NBI', 'ECRH', 'ICRH', 'HMOD', 'LMOD', 'DISE', 'DISR', 'DISB', 'DLIM', 'Ip', 'ne', 'BT', 'NBI_value', 'IC_value', 'EC_value', 'Deuterium', 'Helium', 'Natrium']
data[numerical_cols] = data[numerical_cols].astype(float)

# Extract numerical features and target
X_numerical = data[numerical_cols]
y = data['PressureProfile']

# Extract text data
text_data = data['comment']

# Tokenize the text data
tokenizer = Tokenizer(num_words=1000)
tokenizer.fit_on_texts(text_data)
sequences = tokenizer.texts_to_sequences(text_data)
word_index = tokenizer.word_index

# Pad the sequences to ensure uniform length
max_sequence_length = 100
padded_sequences = pad_sequences(sequences, maxlen=max_sequence_length)

# Standardize the numerical data
scaler = StandardScaler()
X_numerical = scaler.fit_transform(X_numerical)

# Combine the numerical and text data
X_combined = [X_numerical, padded_sequences]

# Split the data into training and testing sets
X_train_num, X_test_num, X_train_text, X_test_text, y_train, y_test = train_test_split(X_combined[0], X_combined[1], y, test_size=0.2, random_state=42)

print("i think its ok")

# Define the input layers
numerical_input = Input(shape=(X_numerical.shape[1],), name='numerical_input')
text_input = Input(shape=(max_sequence_length,), name='text_input')

# Embedding layer for text input
embedding_dim = 50
embedding_layer = Embedding(input_dim=len(word_index) + 1,
                            output_dim=embedding_dim,
                            input_length=max_sequence_length)(text_input)
flattened_text = Flatten()(embedding_layer)

# Combine the numerical and text data
combined = Concatenate()([numerical_input, flattened_text])

# Add hidden layers
x = Dense(32, activation='relu')(combined)
x = Dense(64, activation='relu')(x)
x = Dense(32, activation='relu')(x)
output = Dense(1, activation='sigmoid')(x)  # Assuming binary classification

# Define the model
model = Model(inputs=[numerical_input, text_input], outputs=output)

# Compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Train the model
history = model.fit({'numerical_input': X_train_num, 'text_input': X_train_text},
                    y_train,
                    epochs=50,
                    batch_size=32,
                    validation_split=0.2)

# Save model weights
model.save_weights('model.weights.h5')

# Save the entire model
model.save('entire_model.keras')

# Evaluate the model
loss, accuracy = model.evaluate({'numerical_input': X_test_num, 'text_input': X_test_text}, y_test)
print(f"Test Loss: {loss}, Test Accuracy: {accuracy}")