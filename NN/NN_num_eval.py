import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, Flatten, Concatenate, Dense, Input
from tensorflow.keras.models import Model, load_model
import tensorflow as tf
import os
import shap
import pickle
import matplotlib.pyplot as plt
import NN_main

def check_file(file_path):
    if os.path.exists(file_path):
        print("File exists.")
        return True
    else:
        print("File does not exist.")
        return False


def main():

    model, att, train_file, model_file, shap_file, exp_file, header_string, target_col = NN_main.init()

    if not check_file(shap_file):
        eval(model, train_file, header_string, target_col, model+att, model_file, shap_file, exp_file, gen = True)
    else:
        eval(model ,train_file, header_string, target_col, model+att, model_file, shap_file, exp_file, gen = False)



def eval(model, train_file, header_string, target_col, model_str, model_file, shap_file, exp_file, gen = False):

    selected_numerical_cols = header_string.split(',')

    #ShotNumber,StartTime,EndTime,PressureProfile,NBI_status,ECRH_status,ICRH_status,t,rho,NBI,ECRH,ICRH,comment,
    # plasma_status,HMOD,LMOD,DISE,DISR,VDEo,VDEu,Ip,ne,BT,NBI_value,IC_value,EC_value,D,He,Ne
    data = pd.read_csv(train_file)

    # Replace commas with dots for numerical conversion if necessary
    data = data.replace({',': '.'}, regex=True)

    # Convert numerical columns to float
    data[selected_numerical_cols] = data[selected_numerical_cols].astype(float)

    # Extract numerical features and target
    X = data[selected_numerical_cols]
    y = data[target_col]
    shotnumbers = data[['ShotNumber', 'PressureProfile', 'NBI']]

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test, shotnumbers_train, shotnumbers_test = train_test_split(X, y, shotnumbers, test_size=0.2, random_state=42)

    X_test_untransformed = X_test
    # Standardize the numerical data (specific value size shouldn matter now)
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
  
    # Define the input layers
    numerical_input = Input(shape=(X_train.shape[1],), name='numerical_input')

    # Add hidden layers
    x = Dense(64, activation='relu')(numerical_input)
    x = Dense(64, activation='relu')(x)
    output = Dense(1, activation='sigmoid')(x)

    # Load the entire model
    model = load_model(model_file)

    # Evaluate the model
    loss, accuracy = model.evaluate({'numerical_input': X_test}, y_test)
    print(f'model: {model_str}')
    print(f"Test Loss: {loss}, Test Accuracy: {accuracy}")

    if gen:
        # Use SHAP's KernelExplainer for model interpretation
        background = X_train[np.random.choice(X_train.shape[0], 100, replace=False)]  # Sample background for better performance
        #background = X_train_num[:100]
        #explainer = shap.Explainer(model, background)
        explainer = shap.KernelExplainer(model, background, feature_names = selected_numerical_cols)
        #explainer = shap.DeepExplainer(model, background)

        # Save SHAP values to a file
        with open(exp_file, 'wb') as f:
            pickle.dump(explainer, f)

        shap_values = explainer.shap_values(X_test)
        expl_values = shap.Explanation(values=shap_values[:, :, :],
                               base_values=explainer.expected_value[:],
                               data=X_test[:], # Replace X with the actual input features if available
                               feature_names=selected_numerical_cols) # Replace feature_names with your actual feature names

        # Save SHAP values to a file
        with open(shap_file, 'wb') as f:
            pickle.dump(shap_values, f)
        
        # Save SHAP values to a file
        with open(NN_main.file_path('explainer/expl_vals_'+ model_str +'.pkl'), 'wb') as f:
            pickle.dump(expl_values, f)

    

    # Load SHAP values from file
    with open(shap_file, 'rb') as f:
        shap_values = pickle.load(f)

    # Load explainer from file
    with open(exp_file, 'rb') as f:
        explainer = pickle.load(f)

    # Load expl_values from file
    with open(NN_main.file_path('explainer/expl_vals_'+ model_str +'.pkl'), 'rb') as f:
        expl_values = pickle.load(f)

    
    eval_idx = int(180)
    w = eval_idx + 20

    print(explainer.expected_value.shape)
    print(shap_values.shape)
    print(expl_values.shape)
    print(shotnumbers_test.shape)

    print('--')
    print(expl_values[:, :, 0].shape)

    count=0
    for idx, rho in enumerate(shotnumbers_test['PressureProfile']):
        count += 1
        if rho == 1 and count == 2:
            good_idx = idx
            good_shotnumber = shotnumbers_test['ShotNumber'].iloc[good_idx]
            good_vals = shap.Explanation(values=shap_values[good_idx, :, 0],
                               base_values=explainer.expected_value[:],
                               data=X_test[good_idx], # Replace X with the actual input features if available
                               feature_names=selected_numerical_cols) # Replace feature_names with your actual feature names
            print('good shot: ', good_shotnumber, 'idx: ', good_idx)
            break

    for idx, rho in enumerate(shotnumbers_test['PressureProfile']):
        if rho == 0:
            bad_idx = idx
            bad_shotnumber = shotnumbers_test['ShotNumber'].iloc[bad_idx]
            bad_vals = shap.Explanation(values=shap_values[bad_idx, :, 0],
                               base_values=explainer.expected_value[:],
                               data=X_test[bad_idx], # Replace X with the actual input features if available
                               feature_names=selected_numerical_cols) # Replace feature_names with your actual feature names
            print('bad shot: ', bad_shotnumber, 'idx: ', bad_idx)
            break
    

    #shap force plot
    shap.force_plot(explainer.expected_value[0], shap_values[eval_idx, :, 0], X_test[eval_idx], feature_names = selected_numerical_cols, matplotlib=True)
    displ_shotnumber = shotnumbers_test['ShotNumber'].iloc[eval_idx]
    plt.title(f'Force Plot of #{displ_shotnumber}')
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_force.png')
    plt.close()

    """ 
    #shap force plot
    shap.decision_plot(explainer.expected_value[0], shap_values[eval_idx:w, :, 0], X_test[eval_idx:w], feature_names = selected_numerical_cols)
    displ_shotnumber = shotnumbers_test['ShotNumber'].iloc[eval_idx]
    plt.title(f'Decision Plot of #{displ_shotnumber}-#{shotnumbers_test['ShotNumber'].iloc[w]}')
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_decision.png')
    plt.close() """

    # Plot SHAP summary plot
    shap.plots.beeswarm(expl_values[:, :, 0])
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_summary.png')
    plt.close()

    #Waterfall plot
    shap.waterfall_plot(bad_vals)
    plt.title(f'Waterfall Plot of bad p-profile of discharge #{bad_shotnumber}')
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_waterfall_bad.png')
    plt.close()

    #Waterfall plot
    shap.waterfall_plot(good_vals)
    plt.title(f'Waterfall Plot of good p-profile of discharge #{good_shotnumber}')
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_waterfall_good.png')
    plt.close()

    #shap plot bar
    shap.plots.bar(expl_values[:, :, 0], show_data = False)
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_bar.png')
    plt.close()

    """ shap.plots.heatmap(expl_values[:, :, 0])
    plt.tight_layout()
    plt.savefig('figs/NN/'+ model_str +'_heatmap.png')
    plt.close() """

    """ # Plot SHAP summary plot
    shap.summary_plot(shap_values, X_test_num, feature_names = selected_numerical_cols)
    plt.savefig('figs/NN/'+ model_str +'_summary.png')
    plt.close()

    #shap.plots.bar(shap_values)
    #Waterfall plot
    #shap.waterfall_plot(explainer)
    shap.plots._waterfall.waterfall_legacy(explainer.expected_value, shap_values[0])
    plt.savefig(f'figs/{model}_waterfall.png')
    plt.close() """

    """ #shap decision plot
    shap.decision_plot(explainer.expected_value, shap_values[0], X_test_num.iloc[0])
    plt.savefig(f'figs/NN/'+ model_str +'_decision.png')
    plt.close()

    #shap force plot
    shap.force_plot(explainer.expected_value, shap_values[0], X_test_num.iloc[0])
    plt.savefig(f'figs/NN/'+ model_str +'_force.png')
    plt.close() """


    """ # Plot showing the most important feature
    shap.summary_plot(shap_values, X_test_num, feature_names=selected_numerical_cols, plot_type='bar')
    plt.savefig(f'figs/NN/'+ model_str +'_feature.png')
    plt.close()
    #plt.show() """

    


if __name__ == "__main__":
    main()
