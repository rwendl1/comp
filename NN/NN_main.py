import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, Flatten, Concatenate, Dense, Input
from tensorflow.keras.models import Model
import tensorflow as tf
import os
import shap
import pickle
import matplotlib.pyplot as plt

def file_path(shotlist_file):
    """ read shotlist from shotlist_file.txt """
    dir = str(os.path.dirname(__file__))            #gets directory of executed file
    filepath = dir + "/" + shotlist_file
    return filepath

def init():

    model = 'tsingle'
    att = '_fin'
    ana_att = ''
    #att = ''

    train_file = file_path('data/NN_new_'+ model + '.csv')
    model_file = file_path('models/'+ model + att + '.keras')
    shap_file = file_path('shap_vals/'+ model + att + ana_att +'.pkl')
    exp_file = file_path('explainer/expl_'+ model + att + ana_att +'.pkl')
    
    #ShotNumber,StartTime,EndTime,PressureProfile,NBI_status,ECRH_status,ICRH_status,t,rho,NBI,ECRH,ICRH,comment,
    # plasma_status,HMOD,LMOD,DISE,DISR,VDEo,VDEu,Ip,ne,BT,NBI_value,IC_value,EC_value,D,He,Ne
    # ShotNumber,StartTime,EndTime,PressureProfile,NBI_status,ECRH_status,ICRH_status,t,rho,NBI,ECRH,ICRH,DCN,DCK,Wmhd,Wmhd_IDE,Wmhd_ratio,comment,plasma_status,HMOD,LMOD,Ip,ne,BT,D,He,Ne
    header_string = str('NBI,ECRH,ICRH,DCN,HMOD,Ip,BT,D,He,Ne')
    target_col = 'PressureProfile'

    atts = str(att+ana_att)

    return model, atts, train_file, model_file, shap_file, exp_file, header_string, target_col

    


if __name__ == "__main__":
    init()
