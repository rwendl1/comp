import aug_sfutils as sf
import matplotlib.pyplot as plt
import numpy as np
import os
import utils

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def plot_single_scalar(shot_data, shot_list, scalar):
    """
    Plots the scalar values for the specified shots on separate subplots.

    Args:
    - shot_data (dict): Dictionary containing shot data.
    - shot_list (list): List of shot information (shotnumber, tcIps, tcIpe).
    - scalar (str): The scalar name to plot.
    """

    shot_numbers = [shot_info[0] for shot_info in shot_list]  # Extract shot numbers
    num_shots = len(shot_numbers)
    spacing = 1  # Adjust this value to control the spacing between shot numbers

    for i, shot_number in enumerate(shot_numbers):
        try:
            shot = shot_data[shot_number]['IDE']
            time_index = len(shot['time']) // 2  # Index of the middle timepoint
            scalar_value_ide = shot[scalar][time_index]
            plt.plot(i * spacing, scalar_value_ide, 'bo', label=f"IDE")
        except KeyError:
            print(f"Shot data for shot {shot_number} IDE not found.")

        try:
            shot = shot_data[shot_number]['EQH']
            time_index = len(shot['time']) // 2  # Index of the middle timepoint
            scalar_value_eqh = shot[scalar][time_index]
            plt.plot(i * spacing, scalar_value_eqh, 'rx', label=f"EQH")
        except KeyError:
            print(f"Shot data for shot {shot_number} EQH not found.")

    plt.xlabel('Shot Number')
    plt.ylabel('Value')
    plt.title(f"{scalar} Values")
    plt.xticks(range(num_shots), shot_numbers)  # Set shot numbers as x-axis ticks
    
    # Create a custom legend with only two entries
    plt.legend(['IDE', 'EQH'], loc='upper right')

    # Adjust layout to prevent overlapping
    plt.tight_layout()
    plt.show()




def plot_scalar_values(shot_data, shot_list, scalars):
    """
    Plots the scalar values for the specified shots on separate subplots.

    Args:
    - shot_data (dict): Dictionary containing shot data.
    - shot_list (list): List of shot information (shotnumber, tcIps, tcIpe).
    - scalars (list): List of scalar names to plot.
    """

    num_scalars = len(scalars)
    num_shots = len(shot_list)
    num_subplots = num_scalars

    fig, axes = plt.subplots(num_subplots, 1, figsize=(10, 8))

    for i, scalar in enumerate(scalars):
        scalar_values = []
        shot_numbers = []

        for shot_info in shot_list:
            shotnumber, _, _ = shot_info
            try:
                shot = shot_data[shotnumber]['IDE']
                time_index = len(shot['time']) // 2  # Index of the middle timepoint
                scalar_value = shot[scalar][time_index]
                scalar_values.append(scalar_value)
                shot_numbers.append(shotnumber)
            except KeyError:
                print(f"Shot data for shot {shotnumber} not found.")

        # Plot scalar values for all shots on the same subplot
        ax = axes[i] if num_subplots > 1 else axes
        for j, values in enumerate(scalar_values):
            ax.plot(shot_numbers[j] , values, 'o-', label=f"Shot {shot_numbers[j]}")
        ax.set_xlabel('Shot Number')
        ax.set_ylabel('Value')
        ax.set_title(scalar)
        #ax.legend()

    # Adjust layout to prevent overlapping
    plt.tight_layout()
    plt.show()


def plot_scalar_values_own(shot_data, shot_list, scalars):
    """
    Plots the scalar values for the specified shot at the given timepoint.

    Args:
    - shot_data (dict): Dictionary containing shot data.
    - shotnumber (int): The shot number.
    - timepoint (float): The timepoint to plot.
    """

    # Populate shot data with chosen shots from the list
    for shot_info in shot_list:  
        shotnumber, tcIps, tcIpe = shot_info
        try:
            shot = shot_data[shotnumber]['IDE'] 
            time_index = int(len(shot['time']) / 2)  # Index of the middle timepoint
            time = shot['time'][time_index]
            print(shot['time'].shape)
            print(f"Selected timepoint: {time}s")

            num_subfigures = len(scalars)
            fig, axes = plt.subplots(num_subfigures)
            
            # Iterate over each element in the list and plot it in a separate subfigure
            for i, scalar in enumerate(scalars):
                """ print(shotnumber)
                print(scalar)
                print(shot[scalar])
                print(shot[scalar][time_index])
                print(shot[scalar].shape) """
                axes[i].plot(shotnumber, shot[scalar][time_index], 'o' )
                #axes[i].set_title(f"Subfigure {i+1}")

        except KeyError:
            print(f"Shot data for shot {shotnumber} not found.")

    # Adjust layout to prevent overlapping
    plt.xlabel('Shot Number')
    plt.ylabel('Value')
    plt.legend()
    plt.tight_layout()
    plt.show()



if __name__ == "__main__":
    pickle_file = 'shot_data.pickle'
    shotlist_file = 'shotlist.txt'
    #scalars = ('Zgeri', 'Zgera', 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    scalars = ( 'Rmag', 'Zmag', 'Raus', 'Rxpu', 'Zxpu', 'Wmhd', 'delRoben', 'delRuntn', 'koben', 'kuntn', 'q95')
    shot_data = utils.load_shot_data(pickle_file)
    shot_list = utils.readShotlist(shotlist_file)

    #plot_single_scalar(shot_data, shot_list, 'Raus')
    
    for scalar in scalars:
        plot_single_scalar(shot_data, shot_list, scalar)

    #plot_scalar_values(shot_data, shot_list, scalars)  # Provide the shotnumber and timepoint
